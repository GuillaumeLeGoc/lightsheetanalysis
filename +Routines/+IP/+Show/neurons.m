function out = neurons(F, varargin)
%Routines.IP.Show.neurons Show neuronal segmentation
%   ROUTINES.IP.SHOW.NEURONS(F) Show 2D segmentation for the current set of
%   the Focus object F.
%
%   ROUTINES.IP.SHOW.NEURONS(..., 'set', SET) uses a specific SET of the 
%   Focus object F.
%
%   See also: Routines.IP, Routines.IP.neurons.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('set', NaN, @isnumeric);
in.addParamValue('regions', NaN, @isstruct);
in.addParamValue('index', NaN, @isnumeric);
in.addParamValue('pixlist', {}, @iscell);
in.addParamValue('neurohexs', false, @islogical);
in.addParamValue('zoom', false, @islogical);
in = +in;

% === Parameters ==========================================================

mtag = 'IP/@Mean';
ntag = 'IP/@Neurons';
nhtag = 'IP/@Neurohexs';

% =========================================================================

% --- Focus set
if ~isnan(in.set)
    F.select(in.set);
end

% --- Preparation
Fig = ML.Figures;
Fig.select('IP');

% --- Set zoom
if in.zoom
    ZL = get(gca,{'xlim','ylim'});
end

% --- Input variables

% Positions
if isstruct(in.regions)
    pos = round(reshape([in.regions.Centroid], [2 numel(in.regions)])');
end

if ~exist('pos', 'var')
    Nmat = F.matfile(ntag);
    neurons = Nmat.load('pos');
    pos = neurons.pos;
end

if any(isnan(in.index)) 
    in.index = 1:size(pos,1);
end

if isempty(in.pixlist)
    
    if ~exist('Nmat', 'var')
        Nmat = F.matfile(ntag);
    end
    
    neurons = Nmat.load('ind');
    in.pixlist = neurons.ind;
    
end

% --- Display
Fig.clear('IP');

% Mean image with neurons
Mean = Image(F.fname(mtag, 'png'));

Img = Mean.pix;
Resc = (Img-min(Img(:)))/(max(Img(:))-min(Img(:)));

Grid = ones(size(Img)).*0.65;

for i = 1:numel(in.pixlist)
    Grid(in.pixlist{i}) = 1;
end

CD = cat(3, Resc, Resc.*Grid, Resc.*Grid);
CD = Image(CD);
CD.show; hold on

% Neurons' positions
s = scatter(pos(in.index,1), pos(in.index,2), 'r.');
s.MarkerEdgeAlpha = 0.4;
s.SizeData = 20;

I = setdiff(1:size(pos,1), in.index);
scatter(pos(I,1), pos(I,2), 1, 'y.');

% % % % Neurons' contours
% % % tmp = [cellfun(@(x) x(:,1), in.contours(in.index), 'UniformOutput', false) ...
% % %     cellfun(@(x) x(:,2), in.contours(in.index), 'UniformOutput', false)]';
% % % plot(tmp{:});

% Neurohexs
if in.neurohexs
    NHmat = F.matfile(nhtag);
    neurohexs = NHmat.load();
    for i = 1:numel(neurohexs.N)
        scatter(neurohexs.pos{i}(:,1), neurohexs.pos{i}(:,2), 15, 'c+');
    end
end

% --- Zoom level
if in.zoom
    zoom reset
    set(gca,{'xlim','ylim'}, ZL);
end

% --- Adjustment
F.title('Neurons');

% --- Output
if nargout
    out = struct('pos', pos, 'ind', in.index);
end