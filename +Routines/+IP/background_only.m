function [] = background_only(F)
% Routines.IP.BACKGROUND_ONLY(F)
% Computes the background for experiment specified in focus object F. Saves
% it under its usual location, ie. ../Files/IP/Background.mat.
%
% Based on Routines.IP.background.
%
% INPUTS :
% ------
% F : Focus object.

% === Parameters ==========================================================

btag = 'IP/@Background';

% =========================================================================

for layer = 1:numel(F.sets)
    
    F.select(layer);
    
    % --- Check file existence
    M = F.matfile(btag);
    
    if ~M.exist
        
        % --- Process
        Img = F.iload(1);
        
        % --- Get background noise
        [mean_first, std_first] = Img.background();
        
        % --- Output
        M.save(mean_first, 'Average noise level');
        M.save(std_first, 'Standard deviation of the noise level');
        
        fprintf('Background found : %2.2f\n', mean_first);
        
    end 
end