%Routines.IP.brain Get the brain contour and mask
%   ROUTINES.IP.BRAIN Get the brain contour and mask for the current Focus
%   set and save them in the variables 'contour' and 'ind'/'sub' of the 
%   corresponding Files directory with the tag 'IP/@Brain' .
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   See also: Routines.IP, Routines.IP.Show.brain

% === Parameters ==========================================================

ptag = 'IP/@Parameters';
ktag = 'IP/@Background';
btag = 'IP/@Brain';

tol = 5;
ero_coeff = 20;
dil_coeff = 10;

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Matfiles preparation

Pmat = F.matfile(ptag);
Bmat = F.matfile(btag);
Kmat = F.matfile(ktag);

% --- Processing

if ~Bmat.exist
    
    % Load background info
    Bkg = Kmat.load();
    
    % Load first image of the set
    Img = F.iload(1);
    Tmp1 = Img.copy;
    Tmp1.rm_infos('rep', Bkg.mean_first);
    Tmp1.filter('Gaussian', 'box', [5 5], 'sigma', 2);
        
    % Get default threshold value
    th = 8*graythresh(uint16(Tmp1.pix))*(max(Tmp1.pix(:)));
        
    mode = 'default';
    while true
              
        switch mode
        
            case 'manual'
        
                fprintf('Please validate the polygon to continue.\n');
                
                % Display polygon
                poly = impoly(gca, pos);
                pos = poly.wait;
                
                mode = 'default';
                commandwindow();
                
            case 'manual2'
        
                fprintf('Please validate the polygon to continue.\n');
                
                % Display polygon
                poly = drawpolygon(gca);
                pos = poly.Position;
                
                mode = 'default';
                commandwindow();
                
            otherwise
                
                % Filter image
                Tmp2 = Tmp1.copy;
                Tmp2.threshold(Bkg.mean_first+th*Bkg.std_first);
                Tmp2.erode('size', ero_coeff);
                Tmp2.dilate('size', dil_coeff);
                
                % Extract main region (filled mask)
                reg = Tmp2.regions('Area', 'PixelIdxList');
                [~, I] = max([reg.Area]);
                mask = zeros(Img.height, Img.width);
                if ~isempty(I)
                    mask(reg(I).PixelIdxList) = 1;
                    mask = imfill(mask);
                end
                
                % Get reduced contour
                tmp = bwboundaries(mask);
                if isempty(tmp)
                    pos = NaN(1,2);
                else
                    [x, y] = reducem(tmp{1}(:,2), tmp{1}(:,1), tol);
                    pos = [x y];
                end
                
        end
        
        % Display image
        Routines.IP.Show.brain('F', F, 'pos', pos);
        
        % --- CLI
        clc
        ML.CW.line([F.name ' | Brain contour']);
        
        fprintf('\nPlease choose an action:\n\n');
        fprintf('\t[t] Set sigma threshold (current %0.2f)\n', th);
        fprintf('\t[e] Set tolerance for simplification (current %0.2f)\n', tol);
        fprintf('\t[E] Set erosion coefficient (current %0.2f)\n', ero_coeff);
        fprintf('\t[d] Set dilation coefficient (current %0.2f)\n', dil_coeff);
        fprintf('\t[R] Define background region (current background %0.2f)\n', Bkg.mean_first);
        fprintf('\t[M] Define points manually\n');
        fprintf('\t[m] Move points manually\n\n');
        fprintf('\t[s] Save and continue\n\n');
        
        switch input('?> ', 's')
            
            case 't'
                fprintf('\nNew threshold value: [0 ; Inf[\n');
                th = input('?> ');
                
            case 'e'
                fprintf('\nNew tolerance value: [0 ; 180]\n');
                tol = input('?> ');
                
            case 'E'
                fprintf('\nNew erosion coefficient:\n');
                ero_coeff = input('?> ');
                
            case 'd'
                fprintf('\nNew dilation coefficient:\n');
                dil_coeff = input('?> ');
                
            case 'R'
                
                fprintf('Please validate the polygon to continue.\n');
                
                roi = impoly(gca);
                junk = roi.wait;
                mask = roi.createMask;
                subImg = Image(Img.pix(mask));
                [Bkg.mean_first, Bkg.std_first] = subImg.background();
                mean_first = Bkg.mean_first;
                std_first = Bkg.std_first;
                
            case 'm'
                mode = 'manual';
                
            case 'M'
                mode = 'manual2';
                
            case 's'
                
                % --- Recompute mask
                cld = ML.Time.Display;
                cld.start('Computing mask');
                mask = poly2mask(pos(:, 1), pos(:, 2), Img.height, Img.width);
                ind = find(mask);
                [I, J] = find(mask);
                sub = [I J];
                
                % --- Resave background
                mean_first = Bkg.mean_first;
                std_first = Bkg.std_first;
                Kmat.save(mean_first, 'Average noise level');
                Kmat.save(std_first, 'Standard deviation of the noise level');
                
                % --- Save Parameters
                cld.step('Saving parameters');
                Pmat.save('brain_sigma_th', th, 'Parameter for brain detection');
                Pmat.save('brain_tol', tol, 'Parameter for brain detection');
                
                % --- Save head infos
                cld.step('Saving brain infos');
                Bmat.save(ind, 'Linear indices of the brain mask ([i])');
                Bmat.save(sub, 'Subscripts indices of the brain mask ([i j])');
                Bmat.save('contours', pos, 'Contour of the brain ([x y])');
                bbox = [max(ceil(min(pos(:,1))),1) min(floor(max(pos(:,1))),Img.width) ...
                        max(ceil(min(pos(:,2))),1) min(floor(max(pos(:,2))),Img.height)];
                Bmat.save(bbox, 'Bounding box of the brain contour ([x1 x2 y1 y2])');
                
                cld.stop;
                
                break;
        end
    end
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Brain', ...
        ['<a href="matlab:Routines.IP.Show.brain(''set'', ' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end