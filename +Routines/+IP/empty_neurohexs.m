%Routines.IP.neurohexs Get the neuropils' hexagons
%   ROUTINES.IP.NEUROHEXS Get the neuropils hexagons for the current Focus'
%   set and save them in the Files directory under the tag 'IP/@Neurohexs'.
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   This version has been modified to not save anything, just empty files
%   with empty fields for compatibility.
%
%   See also: Routines.IP, Routines.IP.Show.neurohexs

% === Parameters ==========================================================

nhtag = 'IP/@Neurohexs';

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Matfiles preparation
NHmat = F.matfile(nhtag);

% --- Processing

if ~NHmat.exist
    
    % --- Save neurohexs
    NHmat.save('N', {}, 'Number of neurohexs, per neuropil {n}');
    NHmat.save('pos', {}, 'Positions of the neurohex centers, per neuropil {[x y]}');
    NHmat.save('ind', {},  'Linear indices of the pixels in each neurohex, per neuropil {{[i]}}');
    NHmat.save('sub', {}, 'Subscript indices of the pixels in each neurohex, per neuropil {{[i j]}}');
    NHmat.save('contours', {}, 'Contours of the neurohex, per neuropil {{[x y]}}');
    
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Neurohexs', ...
        ['<a href="matlab:Routines.IP.Show.neurohexs(F, ''set'', ' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end