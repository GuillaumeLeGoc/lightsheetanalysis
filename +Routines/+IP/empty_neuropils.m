function empty_neuropils()
%Routines.IP.neuropils Segmentation of the mean image
%   ROUTINES.IP.NEUROPILS() Finds the neuropils in the mean image
%   for the current set of the Focus object F present in the base
%   workspace. Neuropils contours and masks are saved in the matfile tagged
%   'IP/@Neuropils' in the current set's Files directory.
%
%   Note: If no Focus object is defined in the base workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the base workspace, it is
%   automatically updated.
%
%   This version has been modified to not save anything, just empty files
%   with empty fields for compatibility.
%
%   See also: Routines.IP, Routines.IP.Show.neuropils.

% === Parameters ==========================================================

nptag = 'IP/@Neuropils';

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Prepare matfiles

NPmat = F.matfile(nptag);

if ~NPmat.exist
    
    
    % Time display
    clc
    
    % --- Save
    NPmat.save('N', 0, 'Number of neuropils');
    NPmat.save('pos', [], 'Centers of mass of the neuropils [x y]');
    NPmat.save('ind', {}, 'Linear indices of the neuropils masks {[i]}.');
    NPmat.save('sub', {}, 'Subscript indices of the neuropils masks {[i j]}.');
    NPmat.save('contours', {}, 'Contour polygons {[x y]}');
    
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Neuropils', ...
        ['<a href="matlab:Routines.IP.Show.neuropils(F,''set'',' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end