%Routines.IP.mean_image Compute mean image
%   ROUTINES.IP.MEAN_IMAGE() Get the mean image for the current set of the
%   Focus object F. The mean image is saved twice in the Files directory:
%       - 16 bit version, with the tag 'IP/@Mean.png'.
%       - 8 bit rescaled version, with the tag 'IP/@mean.png'.
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   See also: Routines.IP, Routines.IP.Show.mean_image.

% === Parameters ==========================================================

ptag = 'IP/@Parameters';
ktag = 'IP/@Background';
btag = 'IP/@Brain';
dtag = 'IP/@Drifts';
mtag16 = 'IP/@Mean';
mtag8 = 'IP/@mean8';
ntag = 'IP/@Neurons';

ext = 'png';
freq = 10;

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Matfiles preparation

Pmat = F.matfile(ptag);
Kmat = F.matfile(ktag);
Bmat = F.matfile(btag);
dmat = F.matfile(dtag);

% --- Processing

% Load background info
Bkg = Kmat.load();

% Preparation
clear Mimg
ti = 1;
tf = numel(F.set.frames);

if dmat.exist
    d = dmat.load('dx', 'dy');
    dx = d.dx;
    dy = d.dy;
else
    dx = NaN(1, numel(F.set.frames));
    dy = NaN(1, numel(F.set.frames));
end

greystack_fname = fullfile(F.Data, 'grey_stack', ['Image_' sprintf('%02i', F.set.id) '.tif']);
if ~exist([F.Data filesep 'grey_stack'], 'dir')
    mkdir([F.Data filesep 'grey_stack']);
end

if ~exist(F.fname(mtag16, ext), 'file')
    
    Routines.IP.Show.brain('F', F);
    
    cld = ML.Time.Display;
    
    % --- Ask for number of images
    while true
        
        T = ti:freq:tf;
        
        clc
        ML.CW.line([F.name ' | Mean image']);
        
        fprintf('\nPlease choose the number of images for averaging\n\n');
        fprintf('\t[f] Set selection period (current: 1 frame out of %i)\n', freq);
        fprintf('\t[n] Set number of frames for averaging (%i)\n', numel(T));
        fprintf('\t[ti] Set first frame (%i)\n', ti);
        fprintf('\t[tf] Set last frame (%i)\n\n', tf);
        fprintf('\t[Enter] Compute mean image\n\n');
        if exist('Mimg', 'var')
            fprintf('\t[s] Save and continue\n\n');
        end
        
        switch input('?> ', 's')
            
            case 'f'
                fprintf('\nSet new Frequency:\n');
                freq = round(input('?> '));
                
            case 'n'
                fprintf('\nNumber of frames:\n');
                ntot = round(input('?> '));
                freq = round((tf-ti+1)/ntot);
                
            case 'ti'
                fprintf('\nFirst frame:\n');
                ti = round(input('?> '));
                
            case 'tf'
                fprintf('\nLast frame:\n');
                tf = round(input('?> '));
                
            case 's'
                
                % Check
                if ~exist('Mimg', 'var'), continue; end
                
                cld.start('Saving mean images');
                
                % 16-bit image
                F.isave(Mimg, '', 'bitdepth', 16, 'fname', F.fname(mtag16, ext));
                
                % 8-bit rescaled image
                F.isave(Mimg/max(Mimg(:))*256, '', 'bitdepth', 8, 'fname', F.fname(mtag8, ext));
                
                % Full frame 16-bit image
                imwrite(uint16(Mimg_full), greystack_fname, 'tif');

                % Saving Parameters
                cld.step('Saving parameters');
                Pmat.save('mean_frequency', freq, 'Averaging frequency');
                Pmat.save('mean_number', numel(T), 'Number of images for averaging');
                Pmat.save('mean_ti', ti, 'First frame for averaging');
                Pmat.save('mean_tf', tf, 'Last frame for averaging');
                
                % Saving available drifts
                dmat.save(dx, 'Drift in the x-direction, at each time step [pix]');
                dmat.save(dy, 'Drift in the y-direction, at each time step [pix]');
                
                cld.stop;
                
                break;
                
            otherwise
                
                % Get reference image
                brain = Bmat.load('bbox');
                Ref = F.iload(T(1));
                Ref.rm_infos('rep', Bkg.mean_first);
                
                % Preparation
                Ref_cropped = Ref.copy;
                Ref_cropped.region(brain.bbox);
                Mimg = zeros(Ref_cropped.height, Ref_cropped.width);
                Mimg_full = zeros(Ref.height, Ref.width);
                cld.period = numel(T)/50;
                
                for i = 2:numel(T)
                    
                    % Load image
                    Img = F.iload(T(i));
                    Img.rm_infos('rep', Bkg.mean_first);
                    
                    % Correct for drift
                    if isnan(dx(T(i))) || isnan(dx(T(i)))
                        [dx(T(i)), dy(T(i))] = Ref.fcorr(Img);
                    end
                    Img.translate(-dy(T(i)), -dx(T(i)), 'fill', Bkg.mean_first);
                    Mimg_full = Mimg_full + Img.pix;
                    Img.region(brain.bbox);
                    
                    % Store for averaging
                    Mimg = Mimg + Img.pix;
                    
                    cld.waitline('i');
                    
                end
                
                Mimg_full = Mimg_full/numel(T);
                Mimg = Mimg/numel(T);
                
                % --- Display
                Routines.IP.Show.mean_image(F, 'img', Image(Mimg));
                
        end
    end
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Mean image', ...
        ['<a href="matlab:Routines.IP.Show.mean_image(F,''set'',' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end
