function [] = mean_image_only(F, outdir, varargin)
% Routines.IP.COMPUTEMEANIMAGE(F)
% Computes the mean temporal image for each layers for the experiment
% defined in the focus object F. Uses 1 frame over freq frames. Images are
% drift corrected and the drifts are saved. Saves the images in outdir.
%
% WARNING : Images are not cropped !
%
% Based on Routines.IP.mean_image.
%
% INPUTS :
% ------
% F : Focus object
% freq : frequency for mean computation. Default is 10.
% outdir : absolute path to the folder where to save images or specify
% 'auto' to save images where they are usually saved (../Files/IP/Mean.png)

% === Check input =========================================================

p = inputParser;
p.addRequired('F', @(x) isa(x, 'Focus'));
p.addRequired('outdir', @(x) ischar(x)||isstring(x));
p.addOptional('freq', 10, @isnumeric);
p.parse(F, outdir, varargin{:});

F = p.Results.F;
outdir = p.Results.outdir;
freq = p.Results.freq;

if ~exist(outdir, 'dir')
    mkdir(outdir);
end

% === Parameters ==========================================================

ptag = 'IP/@Parameters';
ktag = 'IP/@Background';
dtag = 'IP/@Drifts';

ext = 'tif';

make_filename = @(l) fullfile(outdir, ['Image_' sprintf('%02i', l) '.' ext]);

% =========================================================================

for layer = 1:numel(F.sets)
    
    statusInfo(layer, numel(F.sets), numel(F.sets));
    
    F.select(layer);
    
    % --- Matfiles preparation
    
    Pmat = F.matfile(ptag);
    Kmat = F.matfile(ktag);
    dmat = F.matfile(dtag);
    
    % --- Processing
    
    % Load background info
    Bkg = Kmat.load();
    
    % Preparation
    ti = 1;
    tf = numel(F.set.frames);
    
    if dmat.exist
        d = dmat.load('dx', 'dy');
        dx = d.dx;
        dy = d.dy;
    else
        dx = NaN(1, numel(F.set.frames));
        dy = NaN(1, numel(F.set.frames));
    end
    
    if ~exist(make_filename(layer), 'file') || ~exist(make_filename(layer), 'file')
        
        T = ti:freq:tf;
        
        % Get reference image
        Ref = F.iload(T(1));
        Ref.rm_infos('rep', Bkg.mean_first);
        
        % Preparation
        Mimg = zeros(Ref.height, Ref.width);
        
        for i = 2:numel(T)
            
            % Load image
            Img = F.iload(T(i));
            Img.rm_infos('rep', Bkg.mean_first);
            
            % Correct for drift
            if isnan(dx(T(i))) || isnan(dx(T(i)))
                [dx(T(i)), dy(T(i))] = Ref.fcorr(Img);
            end
            Img.translate(-dy(T(i)), -dx(T(i)), 'fill', Bkg.mean_first);
            
            % Store for averaging
            Mimg = Mimg + Img.pix;
                        
        end
        
        Mimg = Mimg/numel(T);
        
        % 16-bit image
        F.isave(Mimg, '', 'bitdepth', 16, 'fname', make_filename(layer));
        
        % Saving Parameters
        Pmat.save('mean_frequency', freq, 'Averaging frequency');
        Pmat.save('mean_number', numel(T), 'Number of images for averaging');
        Pmat.save('mean_ti', ti, 'First frame for averaging');
        Pmat.save('mean_tf', tf, 'Last frame for averaging');
        
        % Saving available drifts
        dmat.save(dx, 'Drift in the x-direction, at each time step [pix]');
        dmat.save(dy, 'Drift in the y-direction, at each time step [pix]');
                
    end
end
end