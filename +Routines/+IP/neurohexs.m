%Routines.IP.neurohexs Get the neuropils' hexagons
%   ROUTINES.IP.NEUROHEXS Get the neuropils hexagons for the current Focus'
%   set and save them in the Files directory under the tag 'IP/@Neurohexs'.
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   See also: Routines.IP, Routines.IP.Show.neurohexs

% === Parameters ==========================================================

ptag = 'IP/@Parameters';
btag = 'IP/@Brain';
nptag = 'IP/@Neuropils';
nhtag = 'IP/@Neurohexs';

% --- Default values
a = 10;

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Matfiles preparation

Pmat = F.matfile(ptag);
Bmat = F.matfile(btag);
NPmat = F.matfile(nptag);
NHmat = F.matfile(nhtag);

% --- Processing

if ~NHmat.exist
    
    cld = ML.Time.Display;
    
    % --- Load brain contours
    cld.start('Load brain bounding box');
    brain = Bmat.load('bbox');
    
    % --- Load neuropils contours
    cld.start('Load neuropils'' contours');
    neuropils = NPmat.load();
    
    while true
        
        % --- Display
        Routines.IP.Show.mean_image(F);
        
        % --- Prepare
        cld.step('Prepare processing');
        N = cell(neuropils.N, 1);
        pos = cell(neuropils.N, 1);
        contours = cell(neuropils.N, 1);
        
        NC = cell(neuropils.N, 1);
        V = cell(neuropils.N, 1);
        C = cell(neuropils.N, 1);
        
        % --- Processing
        
        cld.step('Processing');
        
        for i = 1:neuropils.N
            
            % --- Get neuropil contour
            NC = neuropils.contours{i};
            
            % --- Get neurohex positions
            [X, Y] = meshgrid(min(NC(:,1)):a*sqrt(3)/2:max(NC(:,1)), ...
                min(NC(:,2)):a:max(NC(:,2)));
            Y(:,2:2:end) = Y(:,2:2:end) + a/2;
            x = X(:);
            y = Y(:);
            
            % --- Get neurohex vertices
            z = a/sqrt(3)*exp(sqrt(-1)*pi/3*(0:6));
            vx = bsxfun(@plus, x, real(z));
            vy = bsxfun(@plus, y, imag(z));
            
            % --- Check if cells are inside neuropil contour
            I = find(all(inpolygon(vx, vy, NC(:,1), NC(:,2)), 2));
            
            % --- Get informations
            N{i} = numel(I);
            pos{i} = [x(I) y(I)];
            
            % There is an error here. Call Raphy !
            contours{i} = mat2cell([vx(I,:) vy(I,:)], 7*ones(N{i},1));
            
            % --- Display
            plot(NC(:,1), NC(:,2), 'c-');
            scatter(pos{i}(:,1), pos{i}(:,2), 'm.')
            
        end
        
        cld.stop
        
        % --- CLI
        clc
        ML.CW.line([F.name ' | Neurohexs']);
        
        fprintf('\nPlease choose an action:\n\n');
        fprintf('\t[a] Set hexagonal edge length (current %.0f pix)\n', a);
        
        fprintf('\n\t[s] Save and continue\n\n');
        
        switch input('?> ', 's')
            
            case 'a'
                fprintf('\nNew hexagonal edge length (pix): ]0 ; Inf[\n');
                a = input('?> ');
                
            case 's'
                
                % --- Save parameters
                cld.step('Saving parameters');
                Pmat.save('neurohex_a', a, 'Length of the hexagonal edges = distance between two cell centers (pix)');
                cld.stop
                
                % --- Compute masks infos
                ind = cell(neuropils.N, 1);
                sub = cell(neuropils.N, 1);
        
                for i = 1:neuropils.N
                    
                    for j = 1:numel(contours{i})
                        
                        % Get mask infos
                        tmp = poly2mask(contours{i}{j}(:,1), contours{i}{j}(:,2), ...
                            brain.bbox(4)-brain.bbox(3)+1, brain.bbox(2)-brain.bbox(1)+1);
                        ind{i}{j} = find(tmp);
                        [I, J] = find(tmp);
                        sub{i}{j} = [I J];
                        
                        % Waitline
                        cld.waitline('i', 'j');
                        
                    end
                    
                end
                
                % Proper closure
                if neuropils.N
                    cld.waitline('i', 'j');
                end
                
                % --- Save neurohexs
                cld.step('Saving neurohexs');
                NHmat.save(N, 'Number of neurohexs, per neuropil {n}');
                NHmat.save(pos, 'Positions of the neurohex centers, per neuropil {[x y]}');
                NHmat.save(ind, 'Linear indices of the pixels in each neurohex, per neuropil {{[i]}}');
                NHmat.save(sub, 'Subscript indices of the pixels in each neurohex, per neuropil {{[i j]}}');
                NHmat.save(contours, 'Contours of the neurohex, per neuropil {{[x y]}}');
                
                cld.stop
                
                break;
        end
        
    end
    
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Neurohexs', ...
        ['<a href="matlab:Routines.IP.Show.neurohexs(F, ''set'', ' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end