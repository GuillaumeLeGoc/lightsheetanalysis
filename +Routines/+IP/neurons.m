
%Routines.IP.neurons Segmentation of the mean image
%   ROUTINES.IP.NEURONS() Perform segmentation of the mean image for the
%   set and save it in the Files directory with the tag 'IP/@Neurons'.
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   See also: Routines.IP, Routines.IP.Show.neurons.

% === Parameters ==========================================================

btag = 'IP/@Brain';
mtag = 'IP/@Mean';
nptag = 'IP/@Neuropils';
ntag = 'IP/@Neurons';

% % % smwindow = 5;
% % % pertile = 0.2;
% % % Alim = [50 500];
% % %
% % % margin = 50;
% % %
% % % x_step = 25;
% % % y_step = 25;
% % %
% % % show_segmented = true;

switch F.IP.camera
    
    case 'Andor_iXon'
        
        area_lth = 2.4;         % Neuron area lower threshold (µm²)
        area_uth = 150;         % Neuron area upper threshold (µm²)
        ecc_lth = 0.1;
        ecc_uth = 0.9;
        anc_size = 3;           % Number of neighbooring pixels to correlate
        anc_th = 0.05;
        
    case {'PCO.Edge', 'PCO.edge'}
        
        area_lth = 20;              % Neuron area lower threshold (µm²)
        area_uth = 500;             % Neuron area upper threshold (µm²)
        ecc_lth = 0;                % Eccentricity lower threshold
        ecc_uth = 0.9;              % Eccentricity upper threshold
        anc_size = 3;               % Neighbooring distance to correlate (µm)
        anc_th = 0.05;
end

% -------------------------------------------------------------------------

% Convert thresholds in pixels
spix = (F.dx + F.dy)/2;
area_lth = fix(area_lth/spix);
area_uth = fix(area_uth/spix);
anc_size = fix(anc_size/spix);

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Matfiles preparation

Pmat = F.matfile(ptag);
Bmat = F.matfile(btag);
NPmat = F.matfile(nptag);
Nmat = F.matfile(ntag);

% --- Processing

if ~Nmat.exist
    
    % Preparation
    clc
    cli_ht = [F.name ' | Neuronal segmentation'];
    fprintf('\n%s\n', ML.CW.line(cli_ht));
    cld = ML.Time.Display;
    Fig = ML.Figures;
    
    % --- Load masks
    cld.step('Loading masks');
    brain = Bmat.load();
    neuropils = NPmat.load();
    
    % --- Load mean image
    cld.step('Loading mean image');
    Mean = Image(F.fname(mtag, 'png'));
    
    % --- Image processing
    
    cld.step('Image procesing');
    Img = Mean.copy();
    
    % Increase contrast
    A = ordfilt2(Img.pix, 5, ones(round(8/spix)));
    B = ordfilt2(Img.pix, 95, ones(round(8/spix)));
    
    if strcmp(F.IP.line, 'Nuc')
        Pre = (B-Img.pix)./(B-A);
    elseif strcmp(F.IP.line, 'Cyt')
        Pre = (Img.pix-A)./(B-A);
    end
    
    % Convolution
    Wat = conv2(Pre, fspecial('disk', 3), 'same');
    
    % --- Apply masks
    
    cld.step('Apply masks');
    
    % Brain mask
    tmp = zeros(F.IP.height, F.IP.width);
    tmp(brain.ind) = 1;
    Mask = Image(tmp);
    Mask.region(brain.bbox);
    Mask = logical(Mask.pix);
    Pre(~Mask) = Inf;
    
    % Neuropils masks
    for i = 1:neuropils.N
        Pre(neuropils.ind{i}) = Inf;
    end
    
    % --- Watershed
    
    cld.step('Watershed');
    
    % Watershed
    Wat(isnan(Wat)) = Inf;
    L = watershed(Wat);
    R = regionprops(L, 'Centroid', 'Area', 'Eccentricity', 'PixelIdxList', 'PixelList');
    
    cld.stop;
    
    % --- Neuron filtering loop
    
    % Preparation
    Plist = {R(:).PixelIdxList};
    Pos = reshape([R(:).Centroid], [2 numel(R)])';
    Slist = {R(:).PixelList};
    Area = [R(:).Area];
    Ecc = [R(:).Eccentricity];
    
    mode = 'auto';
    
    while true
        
        switch mode
            
            case 'auto'
                
                plist = Plist;
                pos = Pos;
                slist = Slist;
                area = Area;
                ecc = Ecc;
                r = R;
                
                % --- Filter mask
                I = Mask(sub2ind(size(Img.pix), round(pos(:, 2)), round(pos(:, 1))));
                plist = plist(I);
                pos = pos(I, :);
                slist = slist(I);
                area = area(I);
                ecc = ecc(I);
                r = r(I);
                
                if ~exist('n_init', 'var')
                    n_init = numel(area);   % initial number of neurons
                end
                
                % --- Filter size
                I = area>=area_lth & area<=area_uth;
                plist = plist(I);
                pos = pos(I, :);
                slist = slist(I);
                area = area(I);
                ecc = ecc(I);
                r = r(I);
                
                % --- Filter eccentricity
                I = ecc>=ecc_lth & ecc<=ecc_uth;
                plist = plist(I);
                pos = pos(I, :);
                slist = slist(I);
                area = area(I);
                ecc = ecc(I);
                r = r(I);
                
                % --- Filter with correlations
                Raw = zeros(size(Img.pix));
                Raw(sub2ind(size(Img.pix), round(pos(:,2)), round(pos(:,1)))) = 1;
                
                if strcmp(F.IP.line, 'Nuc')
                    Res = -bwdist(Raw);
                elseif strcmp(F.IP.line, 'Cyt')
                    Res = bwdist(Raw);
                end
                
                coeff = NaN(size(pos,1), 1);
                
                for i = 1:size(pos,1)
                    
                    x = round(pos(i,1));
                    y = round(pos(i,2));
                    
                    Sub = Img.pix(max(y-anc_size,1):min(y+anc_size, size(Img.pix,1)), max(x-anc_size,1):min(x+anc_size, size(Img.pix,2)));
                    Sub2 = Res(max(y-anc_size,1):min(y+anc_size, size(Img.pix,1)), max(x-anc_size,1):min(x+anc_size, size(Img.pix,2)));
                    
                    coeff(i) = corr2(Sub, Sub2);
                    
                end
                
                I = coeff>=anc_th;
                plist = plist(I);
                pos = pos(I, :);
                slist = slist(I);
                area = area(I);
                ecc = ecc(I);
                r = r(I);
                idx = 1:numel(r);
                
        end
        
        % --- Display
        Routines.IP.Show.neurons(F, 'set', F.set.id, ...
            'regions', r, 'index', idx, 'pixlist', plist, 'zoom', true);
        
        % Update manually removed/added neurons
        area = area(idx);
        pos = pos(idx, :);
        plist = plist(idx);
        slist = slist(idx);
        ecc = ecc(idx);
        r = r(idx);
        
        % --- CLI
        clc
        fprintf('\n%s\n', ML.CW.line(cli_ht));
        
        fprintf('%i neurons kept over %i (%0.2f%%)\n', numel(I), n_init, numel(I)/n_init*100);
        
        fprintf('\nPlease choose an action:\n');
        
        fprintf('\n\t --- Shape filters\n');
        fprintf('\t[A] Display area pdf\n');
        fprintf('\t[E] Display eccentricity pdf\n');
        
        fprintf('\n\t[l] Set area lower threshold (current %2.2f µm²)\n', area_lth*spix);
        fprintf('\t[u] Set area upper threshold (current %2.2f µm²)\n', area_uth*spix);
        
        fprintf('\n\t[el] Set eccentricity lower threshold (current %0.2f)\n', ecc_lth);
        fprintf('\t[eu] Set eccentricity upper threshold (current %0.2f)\n', ecc_uth);
        
        fprintf('\n\t --- Average Neighborhood correlation\n');
        fprintf('\t[w] Set ANC window size (current %0.2f µm)\n', anc_size*spix);
        fprintf('\t[t] Set ANC threshold (current %0.2f)\n', anc_th);
        
        fprintf('\n\t --- Line\n');
        fprintf('\t[L] Change line (current %s)\n', F.IP.line);
        
        fprintf('\n\t[m] Go manual\n');
        
        fprintf('\n\t --- Continue\n');
        fprintf('\t[s] Save and continue\n\n');
        
        switch input('?> ', 's')
            
            case 'A'
                
                % --- Display area pdf
                p = ML.Stat.pdf(0:5:500, area(:));
                
                Fig.new('Area PDF');
                
                hold on
                box on
                plot(p.bin, p.pdf, 'b+-');
                ML.Visu.line(area_lth, 'x', 'linestyle', '--', 'color', 'k');
                ML.Visu.line(area_uth, 'x', 'linestyle', '--', 'color', 'k');
                xlabel('Area (pix)');
                ylabel('PDF');
                title('PDF of the neurons'' area');
                
            case 'E'
                
                % --- Display eccenticity pdf
                p = ML.Stat.pdf(linspace(0,1,round(numel(ecc)/50)), ecc(:));
                
                Fig.new('Eccenticity PDF');
                hold on
                box on
                plot(p.bin, p.pdf, 'b+-');
                ML.Visu.line(ecc_lth, 'x', 'linestyle', '--', 'color', 'k');
                ML.Visu.line(ecc_uth, 'x', 'linestyle', '--', 'color', 'k');
                
                xlabel('Eccentricity');
                ylabel('PDF');
                title('PDF of the neurons'' eccentricities');
                
            case 'l'
                fprintf('\nNew area lower threshold: [1 ; %i]\n', area_uth*spix);
                area_lth = input('?> ');
                area_lth = fix(area_lth/spix);
                mode = 'auto';
                
            case 'u'
                fprintf('\nNew area upper threshold: [%i ; Inf[\n', area_lth*spix);
                area_uth = input('?> ');
                area_uth = fix(area_uth/spix);
                mode = 'auto';
                
            case 'el'
                fprintf('\nNew eccentricity lower threshold: [0; %i]\n', ecc_uth);
                ecc_lth = input('?> ');
                mode = 'auto';
                
            case 'eu'
                fprintf('\nNew eccentricity upper threshold: [%i; 1]\n', ecc_lth);
                ecc_uth = input('?> ');
                mode = 'auto';
                
            case 'w'
                fprintf('\nNew size: [1 ; Inf[\n');
                anc_size = input('?> ');
                anc_size = fix(anc_size/spix);
                mode = 'auto';
                
            case 't'
                fprintf('\nNew threshold value: [0 ; Inf[\n');
                anc_th = input('?> ');
                mode = 'auto';
                
            case 'L'
                fprintf('\nNew line: Nuc, Cyt\n');
                F.IP.line = input('?> ', 's');      % added 2018-03-28 for new segmentation
                mode = 'auto';
                
            case 'm'
                
                % Update manually removed/added neurons
                area = area(idx);
                pos = pos(idx, :);
                plist = plist(idx);
                slist = slist(idx);
                ecc = ecc(idx);
                r = r(idx);
                
                % Display
                vars = Routines.IP.Show.neurons(F, 'set', F.set.id, ...
                    'regions', r, 'index', idx, 'pixlist', plist, 'zoom', true);
                
                idx_ = setdiff(1:size(pos,1), idx);
                
                while true
                    
                    clc
                    fprintf('\n%s\n', ML.CW.line(cli_ht));
                    fprintf('\t[Right click] Free mode (zoom, translation, etc.)\n');
                    fprintf('\t[Left click] Switch neuron state (ON/OFF)\n');
                    fprintf('\t[+] Group ON\n');
                    fprintf('\t[-] Group OFF\n');
                    fprintf('\t[s] Group switch (ON/OFF)\n');
                    fprintf('\n\t[Middle click] Back to general segmentation menu.\n');
                    
                    h(1) = scatter(vars.pos(idx,1), vars.pos(idx,2), 'r.');
                    h(1).MarkerEdgeAlpha = 0.6;
                    h(1).SizeData = 30;
                    h(2) = scatter(vars.pos(idx_,1), vars.pos(idx_,2), 1, 'y.');
                    h(2).MarkerEdgeAlpha = 0.6;
                    h(2).SizeData = 30;
                    
                    [x, y, button] = ML.ginput(1);
                    
                    if isempty(button), continue; end
                    
                    switch button
                        
                        case 1      % Individual switch
                            
                            [~, mi] = min((vars.pos(:,1)-x).^2 + (vars.pos(:,2)-y).^2);
                            if ismember(mi, idx)
                                idx = setdiff(idx, mi);
                                idx_ = union(idx_, mi);
                            else
                                idx = union(idx, mi);
                                idx_ = setdiff(idx_, mi);
                            end
                            
                        case 43    % Group ON
                            
                            % Get polygon
                            tmp = drawpolygon;
                            p = tmp.Position;
                            delete(tmp);
                            set(gcf,'Pointer','arrow');
                            
                            I = find(inpolygon(vars.pos(:,1), vars.pos(:,2), p(:,1), p(:,2)));
                            idx = union(idx, I);
                            idx_ = setdiff(idx_, I);
                            
                        case 45    % Group OFF
                            
                            % Get polygon
                            tmp = drawpolygon;
                            p = tmp.Position;
                            delete(tmp);
                            set(gcf,'Pointer','arrow');
                            
                            I = find(inpolygon(vars.pos(:,1), vars.pos(:,2), p(:,1), p(:,2)));
                            idx = setdiff(idx, I);
                            idx_ = union(idx_, I);
                            
                        case 115    % Group switch
                            
                            % Get polygon
                            tmp = drawpolygon;
                            p = tmp.Position;
                            delete(tmp);
                            set(gcf,'Pointer','arrow');
                            
                            I = find(inpolygon(vars.pos(:,1), vars.pos(:,2), p(:,1), p(:,2)));
                            tf = ismember(I, idx);
                            idx = union(setdiff(idx, I(tf)), I(~tf));
                            idx_ = union(setdiff(idx_, I(~tf)), I(tf));
                            
                        case 2
                            break;
                            
                        case 3
                            clc
                            fprintf('\n%s\n', ML.CW.line(cli_ht));
                            fprintf('Zoom mode. Press [Enter] to resume.\n');
                            pause
                    end
                    
                    delete(h)
                end
                set(gcf,'Pointer','arrow');
                mode = 'manual';
                
            case 's'
                
                % --- Save parameters
                cld.step('Saving parameters');
                Pmat.save('neurons_lower_area', area_lth, 'Area lower threshold');
                Pmat.save('neurons_upper_area', area_uth, 'Area upper threshold');
                Pmat.save('neurons_eccentricity', ecc_lth, 'Eccentricity threshold');
                
                % Save in Neurons
                cld.step('Saving neurons');
                Nmat.save('N', numel(r), 'Number of neurons');
                Nmat.save('pos', pos, 'Positions of the neurons'' centroids [x, y]');
                Nmat.save('ind', plist, 'Linear indices of the pixels belonging to each neuron in the mean image {[i]}.');
                Nmat.save('sub', slist, 'Subscript indices of the pixels belonging to each neuron in the mean image {[x y]}.');
                
                cld.stop;
                
                break;
                
        end
    end
    
else
    
    %Routines.IP.Show.neurons(F);
    
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Neurons', ...
        ['<a href="matlab:Routines.IP.Show.neurons(F, ''set'', ' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end