function neuropils()
%Routines.IP.neuropils Segmentation of the mean image
%   ROUTINES.IP.NEUROPILS() Finds the neuropils in the mean image
%   for the current set of the Focus object F present in the base
%   workspace. Neuropils contours and masks are saved in the matfile tagged
%   'IP/@Neuropils' in the current set's Files directory.
%
%   Note: If no Focus object is defined in the base workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the base workspace, it is
%   automatically updated.
%
%   See also: Routines.IP, Routines.IP.Show.neuropils.

% === Parameters ==========================================================

btag = 'IP/@Brain';
mtag = 'IP/@Mean';
nptag = 'IP/@Neuropils';

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Prepare matfiles

Bmat = F.matfile(btag);
NPmat = F.matfile(nptag);

if ~NPmat.exist
    
    % --- Preparation
    Fig = ML.Figures;
    Fig.clear('IP');
    
    % Time display
    clc
    cld = ML.Time.Display;
    
    % --- Loads
    cld.start('Loading mean image and brain infos');
    Mean = Image(F.fname(mtag, 'png'));
    brain = Bmat.load();
    
    % Create brain mask
    Mask = zeros(F.IP.height, F.IP.width);
    Mask(brain.ind) = 1;
    Mask = Image(Mask);
    Mask.region(brain.bbox);
    
    cld.stop
    
    % --- Main loop
    B = {};
    
    while true
        
        % --- Display
        Routines.IP.Show.neuropils(F, 'contours', B);
        
        % --- CLI
        
        clc
        ML.CW.line([F.name ' | Neuropils']);
        
        fprintf('\nPlease choose an action:\n\n');
        
        fprintf('\t[+] Add a neuropil contour manually \n');
        fprintf('\t[-] Delete a neuropil contour manually \n');
        
        fprintf('\n\t[s] Save and continue\n\n');
        
        switch input('?> ', 's')
            
            case '+'
                
                clc
                fprintf('Please validate the polygon to continue.\n');
                
                % Display polygon
                poly = impoly(gca);
                tmp = poly.wait;
                B{end+1} = [tmp ; tmp(1,:)];
                
                delete(poly);
                pause(0.3);
                commandwindow();
                
            case '-'
                
                % Graphical selection
                [x, y] = ML.ginput(1);
                
                % Get distances
                d = NaN(numel(B),1);
                for i = 1:numel(B)
                    d(i) = min((B{i}(:,1)-x).^2 + (B{i}(:,2)-y).^2);
                end
                
                % Remove selected boundary
                [~, mi] = min(d);
                B(mi) = [];
                
            case 's'
                
                % --- Compute indices
                cld.start('Computing indices');
                pos = NaN(numel(B),2);
                ind = cell(numel(B),1);
                sub = cell(numel(B),1);
                for i = 1:numel(B)
                    tmp = poly2mask(B{i}(:,1), B{i}(:,2), Mean.height, Mean.width);
                    ind{i} = find(tmp);
                    [I, J] = find(tmp);
                    sub{i} = [I J];
                    pos(i,:) = [mean(J) mean(I)];
                end
                
                % --- Save
                cld.step('Saving neuropils');
                NPmat.save('N', numel(B), 'Number of neuropils');
                NPmat.save(pos, 'Centers of mass of the neuropils [x y]');
                NPmat.save(ind, 'Linear indices of the neuropils masks {[i]}.');
                NPmat.save(sub, 'Subscript indices of the neuropils masks {[i j]}.');
                NPmat.save('contours', B, 'Contour polygons {[x y]}');
                
                cld.stop;
                break;
                
        end
    end
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Neuropils', ...
        ['<a href="matlab:Routines.IP.Show.neuropils(F,''set'',' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end

