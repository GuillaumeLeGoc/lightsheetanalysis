function click_signal(varargin)
%Routines.Show.click_signal Get signals, with graphical interface
%   ROUTINES.SHOW.CLICK_SIGNAL Get the Focus object defined in the base
%   workspace and starts the graphical interface. Click on the neurones to
%   get the temporal trace.
%
%   ROUTINES.SHOW.CLICK_SIGNAL(F) Specifies a Focus object to work with.
%
%   See also .

% === Input ===============================================================

in = ML.Input(varargin{:});
in.addOptional('F', NaN, @(x) isa(x, 'Focus'));
in = +in;

% === Parameters ==========================================================

mtag = 'IP/@Mean';
Ntag = 'IP/@Neurons';

% =========================================================================

cld = ML.Time.Display;

% --- Get Focus (if not input)
if ~isa(in.F, 'Focus')
    tmp = ML.WS.get_by_class('Focus');
    if ~isempty(tmp)
        in.F = evalin('base', tmp{1});
    else
        in.F = getFocus;
    end
end

% --- Loads
cld.start('Loading mean image');
Img = Image(in.F.fname(mtag, 'png'));

cld.step('Loading Neurons');
Nmat = in.F.matfile(Ntag);
Neurons = Nmat.load('pos', 'sub', 'sfluo');

cld.stop

% --- Display
Fig = ML.Figures;
Fig.clear('Click signals');

% Mean image
subplot(3,1,1:2)
Img.show();
in.F.title('Select a neuron');

while true
    
    clc
    fprintf('\n%s\n', ML.CW.line('Signals'));
    fprintf('\t[Right click] Free mode (zoom, translation, etc.)\n');
    fprintf('\t[Left click] Display neuron signal\n');
    fprintf('\n\t[Middle click] Exit.\n');
    
    % Get neuron position
    [x, y, button] = ML.ginput(1);
    
    switch button
        
        case 1      % Display signal
            
            subplot(3,1,1:2);
            [~, i] = min((Neurons.pos(:,1)-x).^2 + (Neurons.pos(:,2)-y).^2);
            scatter(Neurons.sub{i}(:,1), Neurons.sub{i}(:,2), '.');
            
            % Signals
            subplot(3,1,3);
            cla
            plot(Neurons.sfluo(i,:), 'k-');
            title(['Neuron ' num2str(i)]);
            
        case 2
            break;
            
        case 3
            clc
            fprintf('\n%s\n', ML.CW.line('Signals'));
            fprintf('Zoom mode. Pres [Enter] to resume.\n');
            pause
    end
    
end

