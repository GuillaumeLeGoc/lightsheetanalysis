% Routines.Traduction.LOB.rename
%
% Rename Files from the LOB

% === Parameters ==========================================================

format = '%06i';
ext = '.tif';

% =========================================================================

% --- Get the focus
F = getFocus;

% --- Rename images
D = dir([F.Images '*' ext]);

cld = ML.Time.Display;
cld.period = numel(D)/100;

for i = 1:numel(D)
    
    tk = regexp(D(i).name, ['(\d*)\.T' ext], 'tokens');
    if isempty(tk), continue; end
    
    n = str2double(tk{1});
    movefile([F.Images D(i).name], [F.Images 'Img_' num2str(n, format) ext]);
   
    cld.waitline('i');
end

% --- Rename times
D = dir([F.Images '*Times.txt']);
if ~isempty(D)
    movefile([F.Images D(1).name], [F.Images 'Times.txt']);
end