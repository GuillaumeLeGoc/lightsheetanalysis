%Routines.IP Image processing routine
%
%   See also Focus.

clc

% === Parameters ==========================================================

% --- Define Focii

% F = Focus('2013-10-29', 14);
% Fc = {}; Set to an empty cell for interactive Focus selection

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Check if sets are defined 
if isa(F.sets, 'numeric')
    return
end

% --- Define THF
thf = ML.THF;
thf.add('Image Processing', 'h2');

% Define steps
S = ML.Steps;
S.set_steps({'Background', 'Brain', 'Mean image', 'Neuropils', 'Neurohexs', 'Neurons', 'Signals'});
elms = cell(numel(F.sets),1);
for i = 1:numel(F.sets)
    elms{i} = ['<a href="matlab:Routines.IP.Show.middle_image(F,''set'',' num2str(i) ');">Set ' num2str(i) '</a>']; 
end
S.set_elms(elms);

thf.add(F.name, 'h3');
uid = thf.add(S.display('empty', '-'));
refresh = @(S) thf.replace(uid, S.display('empty', '-'));

for i = 1:numel(F.sets)
    
    F.select(F.sets(i).id);

    % --- Get background noise --------------------------------------------
    Routines.IP.background;
    refresh(S);
    
    % --- Get Brain contour -----------------------------------------------
    Routines.IP.brain;
    refresh(S);
    
    % --- Get Mean Images -------------------------------------------------
    Routines.IP.mean_image;
    refresh(S);
    
    % --- Neuropils -------------------------------------------------------
    Routines.IP.empty_neuropils;
    refresh(S);
    
    % --- Neurohexs -------------------------------------------------------
    Routines.IP.empty_neurohexs;
    refresh(S);
    
    % --- Neurons ---------------------------------------------------------
    Routines.IP.neurons;
    refresh(S);
    
end

close;

timing = tic;
for i = 1:numel(F.sets)
    %1:numel(F.sets);
    
    F.select(F.sets(i).id);

    % --- Signals ---------------------------------------------------------
    % Compute timestamps, drifts and DFF at once
    
    Routines.IP.signals;
    refresh(S);
    
end

% --- End message
disp(['Finished in ' sprintf('%2.2f', toc(timing)) 's']);
thf.add('msg', 'h4');