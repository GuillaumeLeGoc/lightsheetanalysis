% Routines.export_segmentation Takes segmentation from an experiment and
% convert it to another experiment using CMTK rigid registration.

clear

% Get Focus from CLI
fprintf('Select <strong>source</strong> experiment\n');
in.F = getFocus;
fprintf('Select <strong>destination</strong> experiment\n');
out.F = getFocus;

clc;

% Definitions
study = 'Thermotaxis';

% CMTK settings
param.exp_type = '';
param.binning = [1 1];

% Get parameters
n_layers = numel(in.F.sets);

% Prepare output folders
in.meanstackdir = fullfile(in.F.Data, 'grey_stack');
in.dir = fullfile(in.F.Data, 'Registration');
out.meanstackdir = fullfile(out.F.Data, 'grey_stack');
out.dir = fullfile(out.F.Data, 'Registration');
out.affine1 = fullfile(out.dir, 'affine_1.xform');
out.affine2 = fullfile(out.dir, 'affine_2.xform');
if ~exist(in.dir, 'dir')
    mkdir(in.dir);
end
if ~exist(out.dir, 'dir')
    mkdir(out.dir);
end

% Get Background for destination experiment
% -----------------------------------------
fprintf('Getting backgrounds...\n');
Routines.IP.background_only(out.F);
fprintf('Done.\n');

% Get mean image for both experiment
% ----------------------------------
fprintf('Getting temporal mean image...\n');
tic
Routines.IP.mean_image_only(in.F, fullfile(in.F.Data, 'grey_stack'));
fprintf('\t%2.2fs\n', toc);
tic
Routines.IP.mean_image_only(out.F, fullfile(out.F.Data, 'grey_stack'));
fprintf('\t%2.2fs\n', toc);

% Create NRRD, register OUT on IN and save transformations
% --------------------------------------------------------
fprintf('Creating & registering NRRD...'); tic

focii = {in.F, out.F};
[parts1, parts2] = create2Nrrd(focii, 'grey_stack', 'Registration', param);
[fp, fn] = fileparts(parts1{2});
reformat_1 = [fp, filesep, fn, '_reformatted.nrrd'];
[fp, fn] = fileparts(parts2{2});
reformat_2 = [fp, filesep, fn, '_reformatted.nrrd'];
registerNrrd(parts1{1}, parts1{2}, out.affine1);
registerNrrd(parts2{1}, parts2{2}, out.affine2);
transformNrrd(reformat_1, parts1{1}, parts1{2}, out.affine1);
transformNrrd(reformat_2, parts2{1}, parts2{2}, out.affine2);
fprintf('\tDone (%2.2fs).\n', toc);

% Convert neurons indices
% -------------------------------------------------------------------------
fprintf('Converting coordinates...\n'); tic
for l = 1:n_layers
    
    statusInfo(l, n_layers, n_layers);
    
    in.F.select(l);
    out.F.select(l);
    
    % Get xformlist
    if l <= n_layers/2
        xform = out.affine1;
    else
        xform = out.affine2;
    end
    
    % In data
    in.neurons = load(in.F.fname('IP/@Neurons'), 'N', 'ind', 'pos', 'sub');
    in.brain = load(in.F.fname('IP/@Brain'));
    in.param = load(in.F.fname('IP/@Parameters'));
    
    % Out data
    out.Nmat = out.F.matfile('IP/@Neurons');
    out.Pmat = out.F.matfile('IP/@Parameters');
    out.Bmat = out.F.matfile('IP/@Brain');
    
    % --- Backup
    if exist(out.F.fname('IP/@Neurons'), 'file')
        movefile(out.F.fname('IP/@Neurons'), [out.F.fname('IP/@Neurons') '.bak']);
    end
    if exist(out.F.fname('IP/@Brain'), 'file')
        movefile(out.F.fname('IP/@Brain'), [out.F.fname('IP/@Brain') '.bak']);
    end
    if exist(out.F.fname('IP/@Mean', 'png'), 'file')
        movefile(out.F.fname('IP/@Mean', 'png'), [out.F.fname('IP/@Mean', 'png') '.bak']);
    end
    if exist(out.F.fname('IP/@mean8', 'png'), 'file')
        movefile(out.F.fname('IP/@mean8', 'png'), [out.F.fname('IP/@mean8', 'png') '.bak']);
    end
    if exist(out.F.fname('@DFF'), 'file')
        movefile(out.F.fname('@DFF'), [out.F.fname('@DFF') '.bak']);
    end
    % ---
    
    % --- Brain mask
    % --------------
    sub = in.brain.sub;
    pos = in.brain.contours;
    
    % Transform coordinates of brain mask
    pos = [pos.*in.F.dx, in.F.set.z.*ones(size(pos, 1), 1)];
    pos = Store.convertCoordinates(pos, xform);
    pos = round(pos(:, 1:2)./in.F.dx);
    sub = [sub(:, 2).*in.F.dx, sub(:, 1).*in.F.dx, in.F.set.z.*ones(size(sub, 1), 1)];
    sub = Store.convertCoordinates(sub, xform);
    sub = round([sub(:, 2), sub(:, 1)]./in.F.dx);
    sub = min(sub, [in.F.IP.height, in.F.IP.width]);
    sub = max(sub, [1, 1]);
    ind = NaN(size(sub, 1), 1);
    for j = 1:size(sub, 1)
        r = sub(j, 1);
        c = sub(j, 2);
        ind(j) = sub2ind([in.F.IP.height, in.F.IP.width], r, c);
    end
    out.Bmat.save(ind, 'Linear indices of the brain mask ([i])');
    out.Bmat.save(sub, 'Subscripts indices of the brain mask ([i j])');
    out.Bmat.save('contours', pos, 'Contour of the brain ([x y])');
    out.bbox = [max(ceil(min(pos(:,1))),1) min(floor(max(pos(:,1))),out.F.IP.width) ...
        max(ceil(min(pos(:,2))),1) min(floor(max(pos(:,2))),out.F.IP.height)];
    out.Bmat.save('bbox', out.bbox, 'Bounding box of the brain contour ([x1 x2 y1 y2])');
    % ---
    
    % --- Move mean image
    meanimage = imread(fullfile(out.meanstackdir, ['Image_' sprintf('%02i', l) '.tif']));
    meanimage = Image(meanimage);
    meanimage.region(out.bbox);
    out.F.isave(meanimage.pix, '', 'bitdepth', 16, 'fname', out.F.fname('IP/@Mean', 'png'));
    % ---
    
    out.meansize = size(meanimage.pix);
    
    
    % --- Neurons
    % -----------
    % --- Convert neurons' coordinates in full frame
    pos = in.neurons.pos;
    pos = pos + [in.brain.bbox(1), in.brain.bbox(3)];
    sub = in.neurons.sub;
    sub = cellfun(@(x) x + [in.brain.bbox(1), in.brain.bbox(3)], sub, 'UniformOutput', false);
    % ---
    
    % --- Transform neurons' coordinates with CMTK
    pos = [pos.*in.F.dx, in.F.set.z.*ones(size(pos, 1), 1)];
    pos = Store.convertCoordinates(pos, xform);
    pos = pos(:, 1:2)./in.F.dx;
    sub = cellfun(@(x) [x.*in.F.dx, in.F.set.z.*ones(size(x, 1), 1)], sub, 'UniformOutput', false);
    sub = cellfun(@(x) Store.convertCoordinates(x, xform), sub, 'UniformOutput', false);
    sub = cellfun(@(x) round(x(:, 1:2)./in.F.dx), sub, 'UniformOutput', false);
    % ---
    
    % --- Convert neurons' coordinates back in the brain box
    pos = pos - [out.bbox(1), out.bbox(3)];
    sub = cellfun(@(x) x - [out.bbox(1), out.bbox(3)], sub, 'UniformOutput', false);
    sub = cellfun(@(x) min(x, flip(out.meansize)), sub, 'UniformOutput', false);
    sub = cellfun(@(x) max(x, [1, 1]), sub, 'UniformOutput', false);
    % Get linear indices, can't use cellfun
    ind = cell(size(sub));
    for i = 1:numel(sub)
        subs = sub{i};
        linind = NaN(size(subs, 1), 1);
        for j = 1:size(subs, 1)
            r = subs(j, 2);
            c = subs(j, 1);
            linind(j) = sub2ind(out.meansize, r, c);
        end
        ind{i} = linind;
    end
    % ---
    
    % --- Save new neurons
    out.Nmat.save('N', in.neurons.N, 'Number of neurons');
    out.Nmat.save('pos', pos, 'Positions of the neurons'' centroids [x, y]');
    out.Nmat.save('ind', ind, 'Linear indices of the pixels belonging to each neuron in the mean image {[i]}.');
    out.Nmat.save('sub', sub, 'Subscript indices of the pixels belonging to each neuron in the mean image {[x y]}.');
    % ---
    
    % --- Copy segmentation parameters
    area_lth = in.param.neurons_lower_area;
    area_uth = in.param.neurons_upper_area;
    ecc_lth = in.param.neurons_eccentricity;
    out.Pmat.save('neurons_lower_area', area_lth, 'Area lower threshold');
    out.Pmat.save('neurons_upper_area', area_uth, 'Area upper threshold');
    out.Pmat.save('neurons_eccentricity', ecc_lth, 'Eccentricity threshold');
    % ---
    
end

fprintf('\t%2.2fs\n', toc);
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------

function [nrrd1, nrrd2] = create2Nrrd(focii, tiffdirname, regdirname, param)

if ~iscell(focii)
    focii{1} = focii;
end

% Handles
grey_source = @(F, n) fullfile(F.Data, tiffdirname, ['Image_' sprintf('%02i', n) '.tif']);
grey_target = @(F, m, n) fullfile(F.Data, regdirname, [tiffdirname '_' num2str(m)], ['Image_' sprintf('%02i', n) '.tif']);

% Init
nrrd1 = cell(numel(focii), 1);
nrrd2 = cell(numel(focii), 1);
for idxf = 1:numel(focii)
    
    n_layers = numel(focii{idxf}.sets);
    
    % --- Outbound
    if ~exist(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1']), 'dir')
        mkdir(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1']));
    end
    % Create substack
    layers = 1:ceil(n_layers/2);
    for l = layers
        cmd = ['cp "' grey_source(focii{idxf}, l) '" "' grey_target(focii{idxf}, 1, l) '"'];
        unix(cmd);
    end
    % Convert to NRRD
    dz = mean(abs(diff([focii{idxf}.sets(layers).z])));
    param.pix_size = [focii{idxf}.dx focii{idxf}.dy dz];
    param.origin = [0 0 focii{idxf}.sets(layers(1)).z];
    param.space_type = 'pls';
    makeNrrdStack(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1']), ...
        fullfile(focii{idxf}.Data, regdirname), [tiffdirname '_1'], param);
    nrrd1{idxf} = fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1.nrrd']);
    
    % --- Return
    if ~exist(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2']), 'dir')
        mkdir(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2']));
    end
    % Create substack
    layers = (n_layers/2 + 1):n_layers;
    for l = layers
        cmd = ['cp "' grey_source(focii{idxf}, l) '" "' grey_target(focii{idxf}, 2, l) '"'];
        unix(cmd);
    end
    % Convert to NRRD
    dz = mean(diff([focii{idxf}.sets(layers).z]));
    param.pix_size = [focii{idxf}.dx focii{idxf}.dy dz];
    param.origin = [0 0 focii{idxf}.sets(layers(1)).z];
    param.space_type = 'pla';
    makeNrrdStack(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2']), ...
        fullfile(focii{idxf}.Data, regdirname), [tiffdirname '_2'], param);
    nrrd2{idxf} = fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2.nrrd']);
end
end

% -------------------------------------------------------------------------

function [] = registerNrrd(refnrrd, floatnrrd, output)

cmtk_dir = '/usr/local/lib/cmtk/bin/';      % where are CMTK binaries
stacks = ['-o ' '"' output '"'  ' ' '"' refnrrd '"' ' ' '"' floatnrrd '"' ];
% Bertoni
% options = 'registration -i --coarsest 25.6 --sampling 3.2 --omit-original-data --exploration 25.6 --dofs 6 --dofs 9 --accuracy 3.2 ';
% Burgess
% options = 'registrationx --dofs 12 --min-stepsize 1 ';
% CMTK User guide (auto)
% options = 'registration --initxlate --dofs 6,9 --auto-multi-levels 4 ';
% Guillaume
% options = 'registrationx --max-stepsize 30 --min-stepsize 1 --sampling 1 --coarsest 25 --dofs 6,12 --symmetric --cubic ';
% Hugo
options = 'registration --initxlate --dofs 6,9,12 --sampling 3 --coarsest 25 --omit-original-data --accuracy 3 --exploration 25.6 ';
command = [cmtk_dir options stacks];
unix(command);
end

function [] = transformNrrd(output, refnrrd, floatnrrd, xform)

cmtk_dir = '/usr/local/lib/cmtk/bin/';      % where are CMTK binaries

command = ['"' cmtk_dir 'reformatx" -o ' '"' output '"' ' --floating ' '"' floatnrrd '"' ' ' '"' refnrrd '"' ' ' '"' xform '"'];
unix(command);
end

% -------------------------------------------------------------------------