function new_coord = convertCoordinates(coord, xformlist, param, source, target)
% Transform given coordinates according to CMTK transformation specified by xformlist,
% using CMTK streamxform function.
% 
% INPUTS :
% ------
% coord : n x 3 array, containing neurons x,y,z coordinates.
% xformlist : full path to transformation handled by CMTK (either affine or warp).
% param : 'inverse' or nothing. Specify how to to tranform :
%       xformlist depicts transformation from A to B. 'inverse' converts coordinates
%       in A into coordinates in B. Without inverse, it's the other way around.
% source : additional option : 
%
% From Hugo Trentesaux 2018-06-07

% CMTK binaries directory
cmtkbindir = "/usr/lib/cmtk/bin/";

% Check input
% -----------
if ~isstring(xformlist)
    xformlist = string(xformlist);
end

% testing if 'inverse' keyword is present
if ~exist('param', 'var')
    param = "";
end
if strcmp(param, 'inverse')
    param = "--inverse";
else
    param = "";
end

% Read coordinates
% ----------------
%returns a string with triplets separated by \n
ascii_coord = join(join(string(coord)),"\n");
tmpfile = ['/tmp/asciicoordinates' num2str(randi([1e10 1e11])) '.txt'];
fid = fopen(tmpfile, 'w');
fprintf(fid, ascii_coord);
fclose(fid);

% Create command lines
% --------------------
echo = join(["cat " tmpfile], "");
tool = join([cmtkbindir "streamxform"], '');
if exist('source', 'var') && exist('target', 'var')
    options = join(join([
        "--source-image" source
        "--target-image" target
        ]));
else
    options = join([
        ""
        ]);
end
verbose = "";

% concantenate args
args = [echo "|" tool options verbose "-- " param join(['"' xformlist '"'], '')];

% make the command
command = join(args, " ");

% Call system command
% -------------------
[status, cmdout] = unix(command');

% Check if any errors
if status ~= 0
    error('Error in unix command');
end

% Read output
C = textscan(cmdout, '%f %f %f');
new_coord = cell2mat(C);

% Check if some coordinates have been lost
fprintf('%i neurons not transformed. ', size(coord, 1) - size(new_coord, 1));

delete(tmpfile);        % delete temporary file
end