function labels = findLabels(a_coordinates)
% Store.findLabels. This function find the closest point in ZBrain Atlas 
% masks database to label each neurons in coordinates with a brain region.
%
% INPUTS :
% ------
% coordinates : n_neurons x 3 array, contains x,y,z coordinates in microns.
%
% OUTPUTS :
% -------
% labels : n_neurons x n_regions logical array.
%
% From Thijs 2018-05-31

% Parameters
% ----------
masks_dir = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/Atlas_Masks/MaskDatabase.mat';
resolution = [0.798, 0.798, 2];    % resolution specified in documentation (µm)

% Load and convert data
% ---------------------
M = load(masks_dir);
H = M.height;
W = M.width;
Z = M.Zs;

% Convert to pixels
grid_coordinates = round(a_coordinates./resolution);

% Flip coordinates to get same orientation
grid_coordinates = [grid_coordinates(:, 2), grid_coordinates(:, 1), grid_coordinates(:, 3)];

% Find closest points
% -------------------
labels = zeros(size(a_coordinates, 1), size(M.MaskDatabase, 2)); % must be full matrix instead of sparse matrix, because of transfer to python
unlabelled = 0;

for neuron = 1:size(a_coordinates, 1)
    % Check if within atlas-space, fetch label
    if (grid_coordinates(neuron, 3) <= Z )&& (grid_coordinates(neuron, 3) > 0) ...
            && (grid_coordinates(neuron, 2) <= W) && (grid_coordinates(neuron, 2) > 0) ...
            && (grid_coordinates(neuron, 1)) <= H && (grid_coordinates(neuron, 1) > 0)
        
        labels(neuron, :) = M.MaskDatabase(sub2ind([H W Z], ...
            grid_coordinates(neuron, 1), ...
            grid_coordinates(neuron, 2), ...
            grid_coordinates(neuron, 3)), :);
    else
        unlabelled = unlabelled + 1;
    end
end
fprintf('Not labelled : %i (%2.2f%%).', unlabelled, unlabelled/size(a_coordinates, 1));