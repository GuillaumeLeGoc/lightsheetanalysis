function drifts = getDrifts(F)
% Store.getDrifts. Get the x-y array of drift over time, in microns. The
% drift is defined as the mean across each layer.
%
% INPUTS :
% ------
% F : Focus object, focused on an experiment.
%
% OUTPUTS :
% -------
% drift : 2 x n_times array with x ; y drifts in microns for each timestep.

n_layers = length(F.sets);
drifts = zeros(2, numel(F.set.frames));

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load drift
    D = load(F.fname('IP/@Drifts'), 'dx', 'dy');
    
    % Store drift
    drifts = drifts + [D.dx ; D.dy];
    
end

drifts = (drifts./n_layers).*F.dx;