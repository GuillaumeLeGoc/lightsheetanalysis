function baseline = getNeuronsBaseline(F)
% Store.getNeuronsFluo. Gather baseline from all neurons in experiment
%  F in a single matrix, along with time vector.
%
% INPUTS :
% ------
% F : Focus object, focused on an experiment.
%
% OUTPUTS :
% -------
% baseline : (n_layers*n_neurons) x n_times array.
% time : 1 x (n_layers*n_times) array.
% delays : delays for each neurons with respect to the first layer
% (seconds).

n_layers = length(F.sets);      % number of layers

baseline = cell(n_layers, 1);

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    signal = load(F.fname('IP/@Neurons'), 'baseline');
    baseline{layer} = signal.baseline;
    
end

baseline = vertcat(baseline{:});
end