function [coord_xyz_set1, coord_xyz_set2] = getNeuronsCoordinates(F, layers)
% Store.getNeuronsCoordinates. Gather x, y and z coordinates from all
% neurons in the experiment in F Focus object, in interleave mode or not.
% If there is two output, the first half of layers and the second half of
% layers are split into two sets of coordinates.
%
% INPUTS :
% ------
% F : Focus Object, focused on an experiment.
% layers : optional, specify layers as a vector. If none specified, all
% coordinates from all layers are returned.
% 
% OUTPUTS :
% -------
% If only one output if specified, all coordinates are returned
% in a single array.
% coord_xyz_set1 : x-y-z coordinates of the outbound set (pix)
% coord_xyz_set1 : x-y-z coordinates of the return set   (pix)

n_layers = length(F.sets);

if ~exist('layers', 'var')
    layers = 1:n_layers;
    n_layers = numel(layers);
end

z = [F.sets(1:n_layers).z];

if nargout == 1
    % ----------------------------------
    % Return a single set of coordinates
    % ----------------------------------
    coord_xyz_set1 = [];
    
    for layer = layers
        
        F.select(layer);
        
        B = load(F.fname('IP/@Brain'), 'bbox');     % Brain box
        N = load(F.fname('IP/@Neurons'), 'pos');    % Neurons coordinates
        
        x0 = B.bbox(1);     % to replace the brain coordinates in the full frame
        y0 = B.bbox(3);
        n_neurons = size(N.pos,1);
        
        coord_xy = N.pos;
        coord_xy(:,1) = coord_xy(:, 1) + x0;
        coord_xy(:,2) = coord_xy(:, 2) + y0;
        
        coord_z = z(layer).*ones(n_neurons, 1);
        
        coord_xyz_set1 = vertcat(coord_xyz_set1, horzcat(coord_xy, coord_z));
    end
    
    
elseif nargout == 2
    % ------------------------------
    % Return two sets of coordinates
    % ------------------------------
    coord_xyz_set1 = [];
    coord_xyz_set2 = [];
    
    % Set 1 (outbound /)
    % ------------------
    for layer = 1:fix(n_layers/2)
        
        F.select(layer);
        
        B = load(F.fname('IP/@Brain'), 'bbox');     % Brain box
        N = load(F.fname('IP/@Neurons'), 'pos');    % Neurons coordinates
        
        x0 = B.bbox(1);
        y0 = B.bbox(3);
        n_neurons = size(N.pos,1);
        
        coord_xy = N.pos;
        coord_xy(:,1) = coord_xy(:, 1) + x0;
        coord_xy(:,2) = coord_xy(:, 2) + y0;
        
        coord_z = z(layer).*ones(n_neurons, 1);
        
        coord_xyz_set1 = vertcat(coord_xyz_set1, horzcat(coord_xy, coord_z));
    end
    
    % Set 2 (return \)
    % ----------------
    for layer = fix(n_layers/2)+1:n_layers
        
        F.select(layer);
        
        B = load(F.fname('IP/@Brain'), 'bbox');     % Brain box
        N = load(F.fname('IP/@Neurons'), 'pos');    % Neurons coordinates
        
        x0 = B.bbox(1);
        y0 = B.bbox(3);
        n_neurons = size(N.pos,1);
        
        coord_xy = N.pos;
        coord_xy(:,1) = coord_xy(:, 1) + x0;
        coord_xy(:,2) = coord_xy(:, 2) + y0;
        
        coord_z = z(layer).*ones(n_neurons, 1);
        
        coord_xyz_set2 = vertcat(coord_xyz_set2, horzcat(coord_xy, coord_z));
    end
end