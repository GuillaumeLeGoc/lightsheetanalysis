function [dff, time, delays] = getNeuronsDFF(F)
% Store.getNeuronsDFF. Gather DF/F from all neurons in experiment
%  F in a single matrix, along with time vector.
%
% INPUTS :
% ------
% F : Focus object, focused on an experiment.
%
% OUTPUTS :
% -------
% dff : (n_layers*n_neurons) x n_times array.
% time : 1 x (n_layers*n_times) array.
% delays : delays for each neurons with respect to the first layer
% (seconds).

n_layers = length(F.sets);      % number of layers

time = cell(n_layers, 1);
dff = cell(n_layers, 1);
delays = cell(n_layers, 1);

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    signal = load(F.fname('@DFF'), 'neurons');
    dff{layer} = signal.neurons;
    N = size(signal.neurons, 1);
    
    if nargout > 1
        % Load time vector
        t = load(F.fname('@Times'), 't');
    
        if mean(t.t) == 0 || isnan(t.t(1))
            time{layer} = F.set.t*1e-3;
        else
            time{layer} = t.t*1e-3;
        end
    
        delays{layer} = time{layer}(1).*ones(N, 1);
    end
end

dff = vertcat(dff{:});
if nargout > 1
    time = horzcat(time{:});
    delays = vertcat(delays{:});
end
end