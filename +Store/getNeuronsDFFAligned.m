function dff = getNeuronsDFFAligned(F)
% Store.getNeuronsDFF. Gather aligned DF/F from all neurons in experiment
%  F in a single matrix.
%
% INPUTS :
% ------
% F : Focus object, focused on an experiment.
%
% OUTPUTS :
% -------
% dff : (n_layers*n_neurons) x n_times array.

n_layers = length(F.sets);      % number of layers

dff = cell(n_layers, 1);

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    signal = load(F.fname('@DFF_Aligned'), 'dff_aligned');
    dff{layer} = signal.dff_aligned;
    
end

dff = vertcat(dff{:});
end