function [fluo, time, delays] = getNeuronsFluo(F)
% Store.getNeuronsFluo. Gather fluo from all neurons in experiment
%  F in a single matrix, along with time vector.
%
% INPUTS :
% ------
% F : Focus object, focused on an experiment.
%
% OUTPUTS :
% -------
% fluo : (n_layers*n_neurons) x n_times array.
% time : 1 x (n_layers*n_times) array.
% delays : delays for each neurons with respect to the first layer
% (seconds).

n_layers = length(F.sets);      % number of layers

time = cell(n_layers, 1);
fluo = cell(n_layers, 1);
delays = cell(n_layers, 1);

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    signal = load(F.fname('IP/@Neurons'), 'fluo');
    fluo{layer} = signal.fluo;
    N = size(signal.fluo, 1);
    
    % Load time vector
    if nargout > 1
        t = load(F.fname('@Times'), 't');
    
        if ~any(t.t) || any(isnan(t.t)) || any(t.t < 0) || any(diff(t.t) > 2*F.dt*n_layers)
            time{layer} = F.set.t/1000;
        else
            time{layer} = t.t/1000;
        end
    
        delays{layer} = time{layer}(1).*ones(N, 1);
    end
end

fluo = vertcat(fluo{:});
if nargout > 1
    time = horzcat(time{:});
    delays = vertcat(delays{:});
end
end