function [spikes, spikes_bin, tau_rise, tau_decay, ifr] = getNeuronsSpikes(F)
% Store.getNeuronsSpikes. Gather spikes from all neurons 
% from experiment F into a single array.
%
% INPUTS :
% ------
% F : focus object, focused on an experiment
%
% OUTPUTS :
% -------
% spikes : n_neurons x n_times array with inferred spikes,
% spikes_bin : n_neurons x n_times array with binarized inferred spikes.
% tau_rise : average rise time of calcium kernel.
% tau_decay : average decay time of calcium kernel.
% ifr : struct containing algorithm parameters to reconstruct the DF/F
% trace from spikes.

n_layers = length(F.sets);      % number of layers

spikes = cell(n_layers, 1);
spikes_bin = cell(n_layers, 1);
tau_rise = cell(n_layers, 1);
tau_decay = cell(n_layers, 1);
eta = cell(n_layers, 1);
gamma = cell(n_layers, 1);
delta = cell(n_layers, 1);
for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    signal = load(F.fname('@Spikes'), 'spikes', 'spikes_bin', 'pPhys', 'pAlg');
    
    spikes{layer} = signal.spikes;
    spikes_bin{layer} = signal.spikes_bin;
    tau_rise{layer} = signal.pPhys.tauRise;
    tau_decay{layer} = signal.pPhys.tauDecay;
    eta{layer} = signal.pAlg.eta;
    gamma{layer} = signal.pAlg.gamma;
    delta{layer} = signal.pAlg.delta;
end

spikes = vertcat(spikes{:});
spikes_bin = vertcat(spikes_bin{:});

tau_rise = mean([tau_rise{:}]);
tau_decay = mean([tau_decay{:}]);
ifr.eta = mean([eta{:}]);
ifr.gamma = mean([gamma{:}]);
ifr.delta = mean([delta{:}]);
end