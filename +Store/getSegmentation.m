function segmentation_mask = getSegmentation(F)
% Store.getSegmentation. Returns a binary mask corresponding to segmented
% neurons in the grey stack (temporal mean).
%
% INPUTS :
% ------
% F : Focus object.
%
% OUTPUT :
% ------
% segmentation_mask : H x W x n_layers 3D binary mask.

n_layers = length(F.sets);

segmentation_mask = cell(n_layers, 1);
prevneurons = 1;
for layer = 1:n_layers
    
    F.select(layer);
    
    mean_image = imread(fullfile(F.Data, 'grey_stack', ['Image_' sprintf('%02i', layer) '.tif']));
    brain = matfile(F.fname('IP/@Brain'));
    bbox = brain.bbox;
    neurons = matfile(F.fname('IP/@Neurons'));
    neurons_sub = neurons.sub;
    n_neurons = neurons.N;
    neuron_ids = prevneurons:(prevneurons + n_neurons);
    
    segmentation_mask{layer} = zeros(size(mean_image));
    for n = 1:n_neurons
        
        for i = 1:size(neurons_sub{n}, 1)
            
            indrow = min(neurons_sub{n}(i, 2) + bbox(3), F.IP.height);
            indcol = min(neurons_sub{n}(i, 1) + bbox(1), F.IP.width);
            segmentation_mask{layer}(indrow, indcol) = neuron_ids(n);
        end
        
    end
    
    prevneurons = prevneurons + n_neurons;
    
end

segmentation_mask = cat(3, segmentation_mask{:});

end