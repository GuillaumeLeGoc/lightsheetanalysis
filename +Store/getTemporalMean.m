function greystack = getTemporalMean(F)
% Store.getTemporalMean. Gather baseline from all neurons in experiment
%  F in a single matrix, along with time vector.
%
% INPUTS :
% ------
% F : Focus object, focused on an experiment.
%
% OUTPUTS :
% -------
% greystack : H x W x n_layers 3D image.

n_layers = length(F.sets);      % number of layers

greystack = cell(n_layers, 1);

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    greystack{layer} = imread([F.Data 'grey_stack' filesep 'Image_' sprintf('%02i', layer) '.tif']);
    
end

greystack = cat(3, greystack{:});
end
