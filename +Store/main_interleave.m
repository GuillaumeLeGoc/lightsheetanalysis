% Create H5 file from list.

clear
close all
clc

% Path parameters
% ---------------
origin = pwd;

experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/RandomPulses_list.txt';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list_all.txt';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Spontaneous/Spontaneous_list.txt';
study = 'Thermotaxis';
% study = 'Spontaneous';
root = [pwd filesep 'Data' filesep study filesep];

% CMTK transformations for ZBrainAtlas (cytoplasmic)
cyt_atlas_xform = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/ZBrainAtlas_on_LJP_Cyt/Warp';
cyt_source_stack = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/LJP_Cytoplasmic.nrrd';
cyt_target_stack = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/ZBrainAtlas_Cytoplasmic.nrrd';

% CMTK transformations for ZBrainAtlas (nuclear)
nuc_atlas_xform = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/ZBrainAtlas_on_LJP_Nuc/Warp';
nuc_source_stack = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/LJP_Nuclear.nrrd';
nuc_target_stack = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/ZBrainAtlas_Nuclear.nrrd';

% File tags
% ---------
fileversion = '1.0';
% tag = 'spontaneous';      % add tag to output filename
% stimtag = 'Spontaneous';
% stimtype = 'spontaneous at given temperature';
tag = 'rp';
stimtag = 'RandomPulses';
stimtype = 'random pulses with given gradient';
% tag = 'rbm';
% stimtag = 'Spontaneous';
% stimtype = 'spontaneous activity';
skip_existing = 'n';    % if 'n', overwrite existing file

% Settings
% --------
% Mirror y coordinates
mirror = 'y';           % set to 'n' to not mirror
orientation = 'RPS';    % Orientation of the fish (R : >Right, P : >Posterior, S : >Superior)

% --- What to save
save_coordinates = 'y';     % raw, registered on LJP reference brain and registered on ZBrain Atlas
save_labels = 'y';          % regions names from Zbrain Atlas
save_fluo = 'y';            % save raw fluo
save_drifts = 'y';          % save x-y drifts
save_baseline = 'y';        % save baseline
save_dff = 'y';             % DF/F of all neurons
save_spikes = 'y';          % infered spikes with Tubiana's BSD
save_greystack = 'y';       % save temporal mean
save_segmentation = 'y';    % save segmentation mask
save_stim = 'y';            % stimulus trace

% Load list of experiments
list = readtable(experiment_list, 'DatetimeType', 'text');
n_exp = size(list, 1);

for idx_dates = 1:n_exp
    
    % Read date and runs list
    dat = list.Date{idx_dates};
    if strcmp(dat(1), '#')
        continue;
    end
    
    fishline = list.Line(idx_dates);
    fishline = fishline{1};
    age = list.Age(idx_dates);
    fishid = list.FishID(idx_dates);
    
    if isa(list.RunNumber, 'double')
        % Handles when only one day in a day and runs list is read as an
        % array directly
        runs = list.RunNumber(idx_dates);
        
        if strcmp(study, 'Thermotaxis')
            Tsets = list.Tset(idx_dates);
        else
            Tsets = [];
        end
        
    else
        runs = eval(['[' list.RunNumber{idx_dates} ']']);
        if strcmp(study, 'Thermotaxis')
            Tsets = eval(['[' list.Tset{idx_dates} ']']);
        else
            Tsets = [];
        end
    end
    
    % Specify manually dates and run
%     dat = '2017-11-30';
%     runs = 4;
%     Tsets = 30;
    
    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        if ~isempty(Tsets)
            Tset = Tsets(idx_run);  % temperature setpoint in the experiment
        else
            Tset = [];
        end
        
        % Build path name where is the registration files
        reg_dir = [root 'Registration' filesep dat '_' sprintf('%02i', run) ...
            filesep];
        xform_dir = [reg_dir 'Transformations' filesep];
        if contains(fishline, 'nuc', 'IgnoreCase', true)
            xform_name_set1 = 'affine_grey_stack_1_LJP_Nuclear';
            xform_name_set2 = 'affine_grey_stack_2_LJP_Nuclear';
        elseif contains(fishline, 'cyt', 'IgnoreCase', true)
            xform_name_set1 = 'affine_grey_stack_1_LJP_Cytoplasmic';
            xform_name_set2 = 'affine_grey_stack_2_LJP_Cytoplasmic';
        else
            error('Fish line not recognized.');
        end
        
        % Build path name where is the calibration file
        stim_file = [root dat filesep 'Calibration' filesep 'calib_Tset=' num2str(Tset) '.mat'];
        
        % Build output HDF5 file
        d = dat;
        d(strfind(d, '-')) = [];    % remove hyphen
        output_dir = [root 'Datasets' filesep];
        output_file = [output_dir d '_Run' sprintf('%02i', run) '_' tag num2str(fishid) '_Tset=' num2str(Tset) '.h5'];
        
        % Create directory and delete existing file if it exists
        if ~exist(output_dir, 'dir')
            mkdir(output_dir);
        end
        if exist(output_file, 'file')
            switch skip_existing
                case 'y'
                    continue
                case 'n'
                    delete(output_file);
                otherwise
                    fprintf('File existing, choose to overwrite or not (skip_existing)\n');
                    break
            end
        end
        
        % Get focus to access files
        F = getFocus([study filesep dat], run);
        n_layers = length(F.sets);      % number of layers
        
        fprintf('%s : creating HDF5 file\n', F.name);
                
        % Get neurons' coordinates
        % ------------------------
        switch save_coordinates
            case 'y'
                fprintf('Getting neurons'' coordinates...');
                tic
                
                % Split each set (outbound and return) into two arrays
                [c_xyz_set1, c_xyz_set2] = Store.getNeuronsCoordinates(F);
                
                switch mirror
                    case 'n'
                        % Mirror the coordinates in the y direction to
                        % compensate the mirror made with the rotation.
                        c_xyz_set1(:, 2) = F.IP.height - c_xyz_set1(:, 2);
                        c_xyz_set2(:, 2) = F.IP.height - c_xyz_set2(:, 2);
                end
                
                % Rotate coordinates, warning, rotation is also a mirror in
                % counterclockwise direction
                c_xyz_set1 = [c_xyz_set1(:, 2), c_xyz_set1(:, 1), c_xyz_set1(:, 3)];
                c_xyz_set2 = [c_xyz_set2(:, 2), c_xyz_set2(:, 1), c_xyz_set2(:, 3)];
                
                % Convert in microns
                c_xyz_set1(:, 1:2) = c_xyz_set1(:, 1:2).*F.dx;  % pix to µm
                c_xyz_set2(:, 1:2) = c_xyz_set2(:, 1:2).*F.dx;  % pix to µm
                
                % Concatenate all coordinates
                coords = vertcat(c_xyz_set1, c_xyz_set2);
                increment = mean(diff(sort([F.sets.z])));
                coords = coords*1e-3;                           % µm to mm
                n_neurons = size(coords, 1);
                fprintf(' Done (%2.2fs).\n', toc);
                
                % Transform neuron's coordinates given CMTK registration
                % ------------------------------------------------------
                fprintf('Transforming neurons'' coordinates in LJP reference brain... ');
                tic
                
                xformlist_set1 = [xform_dir xform_name_set1];
                reg_coord_set1 = Store.convertCoordinates(c_xyz_set1, xformlist_set1, 'inverse');
                
                xformlist_set2 = [xform_dir xform_name_set2];
                reg_coord_set2 = Store.convertCoordinates(c_xyz_set2, xformlist_set2, 'inverse');
                
                ref_coords = vertcat(reg_coord_set1, reg_coord_set2);
                ref_coords = ref_coords*1e-3;        % µm to mm
                fprintf(' Done (%2.2fs).\n', toc);
                
                % Transform neuron's registered coordinates on the Atlas Zbrain
                % -------------------------------------------------------------
                fprintf('Transforming neurons'' coordinates in ZBrain Atlas... ');
                tic
                
                if contains(fishline, 'nuc', 'IgnoreCase', true)
                    zbrain_coords = Store.convertCoordinates(ref_coords*1e3, nuc_atlas_xform);
                elseif contains(fishline, 'cyt', 'IgnoreCase', true)
                    zbrain_coords = Store.convertCoordinates(ref_coords*1e3, cyt_atlas_xform);
                else
                    error('Fish line not recognized.');
                end
                
                zbrain_coords = zbrain_coords.*1e-3;    % µm to mm
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Find in which brain regions are the neurons
        % -------------------------------------------
        switch save_labels
            case 'y'
                fprintf('Finding neurons'' labels... ');
                tic
                labels = Store.findLabels(zbrain_coords.*1e3);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get fluo
        % --------
        switch save_fluo
            case 'y'
                fprintf('Getting neurons'' fluo...');
                tic
                [fluo, time, delays] = Store.getNeuronsFluo(F);
                n_times = size(fluo, 2);
                acq_rate = 1./mean(diff(time(1:n_times)));
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get drifts
        % ----------
        switch save_drifts
            case 'y'
                fprintf('Getting drifts...');
                tic
                drifts = Store.getDrifts(F);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get baseline
        % ------------
        switch save_baseline
            case 'y'
                fprintf('Getting neurons'' baseline...');
                tic
                baseline = Store.getNeuronsBaseline(F);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get DF/F
        % --------
        switch save_dff
            case 'y'
                fprintf('Getting neurons'' DF/F...');
                tic
                dff = Store.getNeuronsDFF(F);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get spikes
        % ----------
        switch save_spikes
            case 'y'
                fprintf('Getting neurons'' infered spikes...');
                tic
                [spikes, spikes_bin, tau_rise, tau_decay, ifr] = Store.getNeuronsSpikes(F);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get 'lags'
        % ----------
        % Check if it worked with timestamps
        if ~any(delays)
            layerlags = NaN(n_neurons, 1);
            altitudes = unique(coords(:, 3));
            lags = linspace(0, (n_layers - 1)*F.dt/1000, n_layers);
            c_neurons = 0;
        
            for layer = 1:n_layers
                start = c_neurons + 1;
                c_neurons = c_neurons + sum(coords(:, 3) == altitudes(layer));
                layerlags(start:c_neurons) = lags(layer);
            end
        else
            layerlags = delays;
        end
        
        % Get temporal mean
        % -----------------
        switch save_greystack
            case 'y'
                fprintf('Getting grey stack...');
                tic
                greystack = Store.getTemporalMean(F);
                switch mirror
                    case 'y'
                        greystack = flip(greystack, 1);
                end
                
                greystack_r = NaN(size(greystack, 2), size(greystack, 1), size(greystack, 3));
                for l = 1:size(greystack, 3)
                    greystack_r(:, :, l) = imrotate(greystack(:, :, l), -90);
                end
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get segmentation mask
        % ---------------------
        switch save_segmentation
            case 'y'
                fprintf('Getting segmentation mask...');
                tic
                seg_mask = Store.getSegmentation(F);
                switch mirror
                    case 'y'
                        seg_mask = flip(seg_mask, 1);
                end
                
                seg_mask_r = NaN(size(seg_mask, 2), size(seg_mask, 1), size(seg_mask, 3));
                for l = 1:size(seg_mask, 3)
                    seg_mask_r(:, :, l) = imrotate(seg_mask(:, :, l), -90);
                end
                fprintf(' Done (%2.2fs.)\n', toc);
        end
        
        % Get stimulation trace
        % ---------------------
        switch save_stim
            case 'y'
                
                fprintf('Getting stimulation calibration...');
                tic;
                if contains(stimtag, 'pulse', 'IgnoreCase', true)
                    S = load(stim_file, 'param', 'trace', 'peak');
                    % Interpolate to match time vector of DF/F
                    temperature = interp1(S.trace.time, S.trace.temperature, time(1:n_times));
%                     temperature = temperature';
                    
                    temperature_bl = temperature;
                    temperature_bl(isnan(temperature)) = [];
                    % Fill values outside the stimulation period with a
                    % baseline
                    temperature(isnan(temperature)) = mean(temperature_bl(end-10:end));
                elseif contains(stimtag, 'spont', 'IgnoreCase', true)
                    
                    files = dir(F.Data);
                    fileslow = cellfun(@lower, {files(:).name}, 'UniformOutput', false);
                    stimfile = fullfile(F.Data, files(contains(fileslow, 'thermostat')).name);
                    if ~isempty(files(contains(fileslow, 'thermostat')))
                        
                        S = readtable(stimfile);
                        temperature = interp1(S.Time/1000, S.Temperature, time(1:n_times), 'pchip', 'extrap');
                    else
                        warning('Stimulation file not found.');
                        temperature = NaN(1, n_times);
                    end
                    
                end
                
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Write data in HDF5 file
        % -----------------------------------------------------------------
        fprintf('Writing HDF5 file...');
        tic
        
        % --- Brain coordinates
        switch save_coordinates
            case 'y'
                h5create(output_file, '/Data/Brain/Coordinates', [n_neurons 3], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/Coordinates', single(coords));
                h5writeatt(output_file, '/Data/Brain/Coordinates', 'unit', 'mm');
                h5writeatt(output_file, '/Data/Brain/Coordinates', 'space', orientation);
                
                h5create(output_file, '/Data/Brain/LJPCoordinates', [n_neurons 3], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/LJPCoordinates', single(ref_coords));
                h5writeatt(output_file, '/Data/Brain/LJPCoordinates', 'unit', 'mm');
                h5writeatt(output_file, '/Data/Brain/LJPCoordinates', 'space', orientation);
                h5writeatt(output_file, '/Data/Brain/LJPCoordinates', 'reference brain', 'LJP_mean_refbrain');
                
                h5create(output_file, '/Data/Brain/ZBrainCoordinates', [size(zbrain_coords, 1) 3], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/ZBrainCoordinates', single(zbrain_coords));
                h5writeatt(output_file, '/Data/Brain/ZBrainCoordinates', 'unit', 'mm');
                h5writeatt(output_file, '/Data/Brain/ZBrainCoordinates', 'space', orientation);
                h5writeatt(output_file, '/Data/Brain/ZBrainCoordinates', 'reference brain', 'zbrain atlas');
                
        end
        
        % --- Brain labels
        switch save_labels
            case 'y'
                h5create(output_file, '/Data/Brain/Labels', size(labels), 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/Labels', single(labels));
                h5writeatt(output_file, '/Data/Brain/Labels', 'origin', 'zbrain atlas');
        end
        
        % --- Brain raw signal
        switch save_fluo
            case 'y'
                h5create(output_file, '/Data/Brain/RawSignal', [n_neurons n_times], 'Datatype', 'uint16');
                h5write(output_file, '/Data/Brain/RawSignal', uint16(fluo));
                
                h5create(output_file, '/Data/Brain/Times', [1 n_times], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/Times', single(time(1:n_times)));
                h5writeatt(output_file, '/Data/Brain/Times', 'unit', 's');
                
                h5create(output_file, '/Data/Brain/TimeDelays', [n_neurons 1], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/TimeDelays', single(layerlags));
                h5writeatt(output_file, '/Data/Brain/TimeDelays', 'units', 's');
                
        end
        
        % --- Brain Analysis Drifts
        switch save_drifts
            case 'y'
                h5create(output_file, '/Data/Brain/Analysis/Drifts', [2 n_times], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/Analysis/Drifts', single(drifts));
                h5writeatt(output_file, '/Data/Brain/Analysis/Drifts', 'unit', 'µm');
                h5writeatt(output_file, '/Data/Brain/Analysis/Drifts', 'dimensions', 'x ; y');
        end
        
        % --- Brain Analysis baseline
        switch save_baseline
            case 'y'
                h5create(output_file, '/Data/Brain/Analysis/Baseline', [n_neurons n_times], 'Datatype', 'uint16');
                h5write(output_file, '/Data/Brain/Analysis/Baseline', uint16(baseline));
        end
        
        % --- Brain Analysis DF/F
        switch save_dff
            case 'y'
                h5create(output_file, '/Data/Brain/Analysis/DFF', [n_neurons n_times], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/Analysis/DFF', single(dff));
                
        end
        
        % --- Brain Analysis spikes
        switch save_spikes
            case 'y'
                h5create(output_file, '/Data/Brain/Analysis/InferredSpikes', [n_neurons n_times], 'Datatype', 'single');
                h5write(output_file, '/Data/Brain/Analysis/InferredSpikes', single(spikes));
                h5writeatt(output_file, '/Data/Brain/Analysis/InferredSpikes', 'tauRise', tau_rise);
                h5writeatt(output_file, '/Data/Brain/Analysis/InferredSpikes', 'tauDecay', tau_decay);
                h5writeatt(output_file, '/Data/Brain/Analysis/InferredSpikes', 'eta', ifr.eta);
                h5writeatt(output_file, '/Data/Brain/Analysis/InferredSpikes', 'gamma', ifr.gamma);
                h5writeatt(output_file, '/Data/Brain/Analysis/InferredSpikes', 'delta', ifr.delta);
                
                h5writeatt(output_file, '/Data/Brain/Analysis/InferredSpikes', 'Recover DF/F with', 'filter(1, [eta, -eta*(gamma+delta),eta*delta],spikes)');
                
                h5create(output_file, '/Data/Brain/Analysis/ThresholdedSpikes', [n_neurons n_times], 'Datatype', 'uint8');
                h5write(output_file, '/Data/Brain/Analysis/ThresholdedSpikes', uint8(spikes_bin));
        end
        
        % --- Brain Pixels temporal mean
        switch save_greystack
            case 'y'
                h5create(output_file, '/Data/Brain/Pixels/TemporalMean', [F.IP.width F.IP.height n_layers], 'Datatype', 'uint16');
                h5write(output_file, '/Data/Brain/Pixels/TemporalMean', uint16(greystack_r));
                h5writeatt(output_file, '/Data/Brain/Pixels/TemporalMean', 'space', orientation);
        end
        
        % --- Brain Pixels segmentation mask
        switch save_segmentation
            case 'y'
                h5create(output_file, '/Data/Brain/Pixels/Segmentation', [F.IP.width F.IP.height n_layers], 'Datatype', 'uint32');
                h5write(output_file, '/Data/Brain/Pixels/Segmentation', uint32(seg_mask_r));
        end
        
        % --- Stimulus stim
        switch save_stim
            case 'y'
                h5create(output_file, ['/Data/Stimulus/' stimtag '/Trace'], [1 n_times], 'Datatype', 'single');
                h5write(output_file, ['/Data/Stimulus/' stimtag '/Trace'], single(temperature));
                h5writeatt(output_file, ['/Data/Stimulus/' stimtag '/Trace'], 'units', 'degrees celcius');
                h5writeatt(output_file, ['/Data/Stimulus/' stimtag], 'Temperature', Tset);
                
                if contains(stimtag, 'pulses', 'IgnoreCase', true)
                    h5create(output_file, ['/Data/Stimulus/' stimtag '/Parameters'], [size(S.param.onset, 1) 2], 'Datatype', 'single');
                    h5write(output_file, ['/Data/Stimulus/' stimtag '/Parameters'], single([S.param.onset , S.param.duration]));
                    h5writeatt(output_file, ['/Data/Stimulus/' stimtag '/Parameters'], 'Description', 'Onsets / Durations');
                    h5writeatt(output_file, ['/Data/Stimulus/' stimtag '/Parameters'], 'units', 's');
                    h5writeatt(output_file, ['/Data/Stimulus/' stimtag '/Parameters'], 'Temperature setpoint', Tset);
                end
                
            case 'n'
                h5create(output_file, ['/Data/Stimulus/' stimtag], [1 1]);
                if contains(lower(study), 'thermo')
                    h5writeatt(output_file, ['/Data/Stimulus/' stimtag], 'Temperature', Tset);
                end
        end
        
        % --- Metadata
        try
            h5create(output_file, '/Metadata/', [1 1]);
        catch
        end
        h5writeatt(output_file, '/Metadata', 'Experiment date (yyyy-mm-dd)', dat);
        h5writeatt(output_file, '/Metadata', 'Experiment run', run);
        h5writeatt(output_file, '/Metadata', 'Fish line', fishline);
        h5writeatt(output_file, '/Metadata', 'Fish age', age);
        h5writeatt(output_file, '/Metadata', 'Fish ID', [tag num2str(fishid)]);
        h5writeatt(output_file, '/Metadata', 'Acquisition rate (Hz)', acq_rate);
        h5writeatt(output_file, '/Metadata', 'Acquisition number of layers', n_layers);
        h5writeatt(output_file, '/Metadata', 'Acquisition increment (µm)', increment);
        h5writeatt(output_file, '/Metadata', 'Pixel width (µm)', F.dx);
        h5writeatt(output_file, '/Metadata', ['Stimulus --> ' stimtag ' sensory type'], lower(study));
        h5writeatt(output_file, '/Metadata', ['Stimulus --> ' stimtag ' stimulus type'], lower(stimtype));
        h5writeatt(output_file, '/Metadata', 'File creation', string(datetime('now')))
        h5writeatt(output_file, '/Metadata', 'File version', fileversion);
        

        fprintf(' Done (%2.2fs).\n', toc);
        ML.CW.line;
    end
end