function R = computeCorrelationMatrix(X)
% Computes the correlation matrix of X, removing first component of PCA.
% Warning : X must have time series on columns !

R = corrcoef(X);
R(isnan(R)) = 0;
[v, lambda] = eig(R, 'vector');
[maxlambda, argmax] = max(real(lambda));
R = R - v(:, argmax)*v(:, argmax)'*maxlambda;