% HBO.display Plot HBO activity from the given experiment.
clear
clc

root = '/home/ljp/Science/Projects/Neurofish/Data/';

% Experiment
% ----------
study = 'Phototaxis';
date = '2019-10-22';
run = 1;
% To define stimulation file
% Tset = 6;

% Define filenames
% ----------------
calib_filename = 'calib';

% Options
% -------
discard_motions = 'n';
motion_thresh = 5;
time_after_motion = 3;

t_before = 1;   % For mean response (seconds)
t_after = 30;

% Define paths
% ------------
hbo_dir = [root study filesep 'HBO' filesep];

% Get Focus and parameters
F = getFocus([study filesep date], run);
n_layers = length(F.sets);
dt = F.dt*1e-3;
dt_brain = n_layers.*dt;
time_full = 0:dt:(numel(F.set.t)*dt_brain-dt);

% Load data
% ---------
% Stimulation
[onsets, durations] = getStimTimes(F.Data);
if exist('Tset', 'var')
    stim = load([root study filesep date filesep 'Calibration' filesep calib_filename '_Tset=' num2str(Tset)], 'trace');
    stim = stim.trace;
    stim.temperature = stim.temperature(1:round(onsets(end)./mean(diff(stim.time))) + 70);
    stim.time = stim.time(1:round(onsets(end)./mean(diff(stim.time))) + 70);
end

% HbO DF/F
hbo_filename = [hbo_dir date '_Run' sprintf('%02i', run) filesep 'Matfiles' filesep 'hbo_dff.mat'];
hbo_dff = load(hbo_filename, 'hbo_dff');
hbo_dff = hbo_dff.hbo_dff;
n_groups = size(hbo_dff, 2);

% Oversample HBO DF/F
hbo_dff_interp = cell(n_layers, n_groups);
for layer = 1:n_layers
    F.select(layer);
    
    for group = 1:n_groups
        data = hbo_dff{layer, group};
        if isempty(data)
            continue
        end
        
        time_layer = F.set.t*1e-3;
        %         data = gpuArray(data);
        data_interp = interp1(time_layer, data', time_full, 'linear');
        hbo_dff_interp{layer, group} = gather(data_interp');
    end
end

% Gather left/right
HbO_right = mean(vertcat(hbo_dff_interp{:, [2 3 4]}));
HbO_left = mean(vertcat(hbo_dff_interp{:, [1 5 6]}));

% Oversample stimulation trace to match DF/F sampling
if exist('stim', 'var')
    T = interp1(stim.time, stim.temperature, time_full);
    % Replace outside values by mean of the end
    T_baseline = T;
    T_baseline(isnan(T)) = [];
    % Fill values outside the stimulation period with a
    % baseline
    T(isnan(T)) = mean(T_baseline(end-10:end));
end

switch discard_motions
    case 'y'
        motion_times = getMotionTimes(F, 5, motion_thresh, time_after_motion, 'uni');
        HbO_left(motion_times) = 0;
        HbO_right(motion_times) = 0;
end

% Trial-averaged response
% -----------------------
if exist('T', 'var')
    [avg.time, avg.signal] = periStimMean(time_full, [HbO_left', HbO_right', T'], ...
        onsets, n_layers, t_before, t_after);
    avg.temperature = avg.signal(:, 3);
else
    [avg.time, avg.signal] = periStimMean(time_full, [HbO_left', HbO_right'], ...
        onsets, n_layers, t_before, t_after);
end
avg.left = avg.signal(:, 1);
avg.right = avg.signal(:, 2);

% Plot
% ----
% Get colors
f = figure;
ax = gca;
c1 = ax.ColorOrder(1, :);
c2 = ax.ColorOrder(2, :);
close(f);

% Figure 1 : Trace
fig = figure;
left_color = c1;
right_color = [0 0 0];
set(fig,'defaultAxesColorOrder',[left_color; right_color]);

hold on;

if exist('T', 'var')
    yyaxis right
    p1 = plot(time, T, 'k', 'LineWidth', 1.25);
    p1.Color(4) = 0.25;
end
yyaxis left
plot(time_full, HbO_right, '-', 'Color', c1, 'LineWidth', 2);
yyaxis left
plot(time_full, HbO_left, '-', 'Color', c2, 'LineWidth', 2);
L = legend('Right HbO', 'Left HbO');
L.AutoUpdate = 'off';
xlabel('Time [s]');
ylabel('\Delta{F}/F');
infe = min([HbO_left HbO_right]);
supe = max([HbO_left HbO_right]);

for n = 1:length(onsets)
    p = patch([onsets(n) onsets(n) onsets(n)+durations(n) onsets(n)+durations(n)], ...
        [infe supe supe infe], [0 0 0]);
    set(p,'FaceAlpha', 0.5);
    set(p,'EdgeColor', 'none')
end

ax = gca;
ax.FontSize = 20;

% Figure 2 : Trial-averaged response
fig2 = figure;
left_color = [0, 0, 0];
right_color = c1;
set(fig2,'defaultAxesColorOrder', [left_color; right_color]);
hold on;
p1 = plot(avg.time, avg.right, 'LineWidth', 3);
p1.Color = c1;
p2 = plot(avg.time, avg.left, 'LineWidth', 3);
p2.Color = c2;
ylabel('\Delta{F}/F');

if isfield(avg, 'temperature')
    yyaxis right
    p3 = plot(avg.time, avg.temperature, 'k', 'LineWidth', 2.5);
    p3.Color(4) = 0.65;
    ylabel('Temperature [°C]');
end

ax = gca;
ax.FontSize = 21;
xlabel('Time [s]');