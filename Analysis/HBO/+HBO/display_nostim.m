% HBO.display Plot HBO activity from the given experiment.
% - Raw traces

clear
clc

root = '/home/ljp/Science/Projects/Neurofish/Data/';

% Experiment
% ----------
study = 'Thermotaxis';
dat = '2019-11-26';
run = 1;

% Options
% -------
discard_motions = 'n';
motion_thresh = 10;
time_after_motion = 10;
start_time = 1;             % start plot after start_time seconds

% Frequency PSD
startf = 0.5*10^-2;
stopf = .8;
npoints = 150;          % number of frequency points

freq = logspace(log10(startf), log10(stopf), npoints)';

% Define paths
% ------------
hbo_dir = [root study filesep 'HBO' filesep];

% Get Focus and parameters
F = getFocus([study filesep dat], run);
n_layers = length(F.sets);
dt = F.dt*1e-3;
dt_brain = n_layers.*dt;
time_full = linspace(0, numel(F.set.t)*dt_brain, numel(F.set.t));
fs = 1/dt_brain;

% HbO DF/F
hbo_filename = [hbo_dir dat '_Run' sprintf('%02i', run) filesep 'Matfiles' filesep 'hbo_dff.mat'];
hbo_dff = load(hbo_filename, 'hbo_dff');
hbo_dff = hbo_dff.hbo_dff;
n_groups = size(hbo_dff, 2);

% Oversample HBO DF/F
hbo_dff_interp = cell(n_layers, n_groups);
for layer = 1:n_layers
    F.select(layer);
    
    for group = 1:n_groups
        data = hbo_dff{layer, group};
        if isempty(data)
            continue
        end
        
        time_layer = F.set.t*1e-3;
        %         data = gpuArray(data);
        data_interp = interp1(time_layer, data', time_full, 'linear');
        
        hbo_dff_interp{layer, group} = gather(data_interp');
        
        % Fix size
        if size(data, 1) == 1
            hbo_dff_interp{layer, group} = hbo_dff_interp{layer, group}';
        end
    end
end

% Gather left/right
HbO_right = mean(vertcat(hbo_dff_interp{:, [3 4]}));
HbO_left = mean(vertcat(hbo_dff_interp{:, [5 6]}));

switch discard_motions
    case 'y'
        motion_times = getMotionTimes(F, 5, motion_thresh, time_after_motion, 'sym');
        HbO_left(motion_times) = 0;
        HbO_right(motion_times) = 0;
end

% Start from specified point
start_frame = round(start_time/mean(diff(time_full)));
HbO_right = HbO_right(start_frame:end);
HbO_left = HbO_left(start_frame:end);
time_full = time_full(start_frame:end);

% Frequency analysis
hbo = HbO_right - HbO_left;
hbo(~isfinite(hbo)) = 0;
hbo = hbo - mean(hbo);
hbo = hbo./max(hbo);
ifq = instfreq(hbo, 1/mean(diff(time_full)), 'Method', 'hilbert');

% PSD
% pxx = periodogram(hbo, hamming(length(hbo)), freq, fs);
% pxx = pwelch(hbo, 2, 1, freq, fs);
pxx = pmtm(hbo, 4, freq, fs);

% Plot
% ----
% Get colors
c = getColors(2);
c1 = c(1, :);
c2 = c(2, :);

% Figure 1 : Trace
fig = figure;
left_color = c1;
right_color = [0 0 0];
set(fig,'defaultAxesColorOrder',[left_color; right_color]);

hold on;

plot(time_full, smooth(HbO_right), '-', 'Color', c1, 'LineWidth', 2);
plot(time_full, smooth(HbO_left), '-', 'Color', c2, 'LineWidth', 2);
L = legend('Right HbO', 'Left HbO');
L.AutoUpdate = 'off';
xlabel('Time [s]');
ylabel('\Delta{F}/F');

% Figure 2 : HbO difference
figure; 
plot(time_full, hbo);
title([num2str(mean(ifq)) ' Hz']);
xlabel('Time [s]');
ylabel('\Delta{F}/F_{right} - \Delta{F}/F_{left}');

% Figure 3 : PSD
figure;
plot(freq, 10*log10(pxx));
ax = gca;
ax.XScale = 'log';
xlabel('Frequency [Hz]');
ylabel('Power [dB/Hz]');