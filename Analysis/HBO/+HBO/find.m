function [] = find(study, dat, run, neuron, layer_neuron, start_time, end_time)
% HBO.FIND(study, dat, run, neuron, layer_neuron(, start_time, end_time))
% HBO.find. Finds hindbrain oscillator (HBO) computing the correlation matrix. Finds
% neurons correlated with the neuron number NEURON on the layer
% LAYER_NEURON. Shows correlations, then user selects manually HBO parts.
% Saves the output in a specific folder (see code for detail).

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
base = '/home/ljp/Science/Projects/Neurofish/Data/';
% dat = '2018-01-31';
% study = 'Thermotaxis';
% run = 7;

% Settings & default values
% -------------------------
% Found neurons that shows HBO structure with correlations
layer_found_neurons = layer_neuron;
found_neurons = neuron;

% Timings
if ~exist('start_time', 'var')
    start_time = 1;             % Begin the search at this time (seconds)
end
if ~exist('end_time', 'var')
    end_time = 'end';           % End the search at this time (seconds) or 'end'
end

% Layers
layers_to_look = 'all';      % Layers where to look (vector or 'all')

% Drift
motion_thresh = 5;        % DF/F will be set at 0 if drift speed > motion_thresh*std
after_motion_time = 2;    % DF/F will also be set at 0 after the motion (seconds)

% Correlations thresholds
corr_thresh = [-0.15 0.2];   % [anticorrelation correlation]
clustering_steps = 3;       % number of times to look for neurons correlated with the new ones

% Options
% -------
save_in_test = 'n';         % output data will be stored in a test folder

checkdate = datevec(dat);   % parse date

% Preparation
% -------------------------------------------------------------------------
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([study '_Old' filesep dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

% Complete parameters
n_layers = length(F.sets);
dt_brain = (F.dt*n_layers)*1e-3;

% Define output paths
str_run = sprintf('%02i', run);
str_expe = [dat '_Run' str_run];

switch save_in_test
    case 'y'
        path_to_save = [base study filesep 'HBO' filesep 'test_' str_expe filesep];
    otherwise
        path_to_save = [base study filesep 'HBO' filesep str_expe filesep];
end

path_to_figures = [path_to_save 'Figures' filesep];
path_to_index = [path_to_save 'Index' filesep];

if ~exist(path_to_figures, 'dir')
    mkdir(path_to_figures);
end
if ~exist(path_to_index, 'dir')
    mkdir(path_to_index);
end

% Convert timings in frames
after_motion_frame = round(after_motion_time./dt_brain);
start_frame = round((start_time - 1)./dt_brain) + 1;

switch num2str(end_time)
    case 'end'
        end_frame = numel(F.set.t);
    otherwise
        end_frame = round(end_time./dt_brain);
end
selected_frames = start_frame:end_frame;

% Parse layers where to look
if ischar(layers_to_look) || isstring(layers_to_look)
    switch layers_to_look
        case 'all'
            layers = 1:n_layers;
    end
else
    layers = layers_to_look;
end

% Define color maps for each HBO groups
color_matrix = zeros(10, 3);
color_matrix(1,:) = [1 0 0];
color_matrix(2,:) = [0 1 0];
color_matrix(3,:) = [0 0 1];
color_matrix(4,:) = [1 1 0];
color_matrix(5,:) = [1 0 1];
color_matrix(6,:) = [0 1 1];
color_matrix(7,:) = [.5 1 0.5];
color_matrix(8,:) = [.1 .7 1];
color_matrix(9,:) = [0 .3 1];
color_matrix(10,:) = [1 .4 0.1];

fprintf('%s - HBO finder\n', F.name);
ML.CW.line;

% Load DF/F from found neuron
% -------------------------------------------------------------------------
F.select(layer_found_neurons)

DFF = F.matfile('@DFF');
DFF = DFF.load('neurons');
DFF = DFF.neurons;

% Discard motion times
% --------------------
motion_times = getMotionTimes(F, layer_neuron, motion_thresh, after_motion_frame, 'uni');
DFF(:, motion_times) = 0;
fprintf('%i/%i frames discarded due to motion.\n', numel(motion_times), size(DFF, 2));

% Limit time range
DFF = DFF(:, selected_frames);

% Merge DF/F of found neurons
DFF_found_neurons = mean(DFF(found_neurons, :), 1);

% Loop over each layer
% -------------------------------------------------------------------------

for layer = layers
    
    fprintf('Layer %i\n', layer);
    
    % Prepare output file name
    % -------------------------
    make_index_filename = @(n) [path_to_index 'index_layer_' num2str(layer) '_group' num2str(n) ...
        '_source_' num2str(layer_found_neurons) '_' num2str(found_neurons) '.mat'];
    
    F.select(layer);
    
    % Load data
    % ---------
    % DF/F
    DFF = F.matfile('@DFF');
    DFF = DFF.load('neurons');
    DFF = DFF.neurons;

    % Neurons position
    Neurons = F.matfile('IP/@Neurons');
    idx_neurons = Neurons.load('ind');
    idx_neurons = idx_neurons.ind;
    n_neurons = size(DFF, 1);        % number of neurons
    
    % Brain box
    Brain = F.matfile('IP/@Brain');
    bbox = Brain.load('bbox');      % brain box
    bbox = bbox.bbox;
    L = zeros(bbox(2)-bbox(1)+1,bbox(4)-bbox(3)+1)';
    
    % Make the neurons list
    for idx_group = 1:n_neurons
        L(idx_neurons{idx_group}) = idx_group;
    end
    
    % Mean image
    mean_img = imread(F.fname('IP/@Mean', 'png'));
    mean_img = double(mat2gray(mean_img));
    
    % Discard motion times
    motion_times = getMotionTimes(F, layer, motion_thresh, after_motion_frame, 'uni');
    DFF(:, motion_times) = 0;
    
    % Limit time range
    DFF = DFF(:, selected_frames);
    
    % Add the found neuron to the data
    DFF_added = cat(1, DFF, DFF_found_neurons);
    
    % Find correlated and anticorrelated neurons
    % ------------------------------------------
    correlation_matrix = computeCorrelationMatrix(DFF_added');
    
    correl_with_found = correlation_matrix(end, :);     % Take correlations with the found neuron
    correl_with_found = correl_with_found(1:end - 1);   % Remove itself
    
    index_corr_pos = cell(clustering_steps + 1, 1);
    index_corr_neg = cell(clustering_steps + 1, 1);
    
    index_corr_pos{1} = find(correl_with_found > corr_thresh(2));    % Neurons' index correlated with found neuron
    index_corr_neg{1} = find(correl_with_found < corr_thresh(1));    % Neurons' index anticorrelated with found neuron
    
    % Find neurons correlated with neurons correlated with found neurons
    for clu = 2:clustering_steps + 1
        new_correl = mean(correlation_matrix([index_corr_pos{1:clu - 1}], 1:end - 1), 1);
        new_neurons = find(new_correl > corr_thresh(2));
        index_corr_pos{clu} = new_neurons;
        
        new_correl = mean(correlation_matrix([index_corr_neg{1:clu - 1}], 1:end - 1), 1);
        new_neurons = find(new_correl > corr_thresh(2));
        index_corr_neg{clu} = new_neurons;
    end
    
    index_corr_pos = unique(cat(2, index_corr_pos{:}));    % remove duplicates
    index_corr_neg = unique(cat(2, index_corr_neg{:}));
    %     icp = index_corr_pos;
    %     icn = index_corr_neg;
    %     index_corr_pos(ismember(icp, icn)) = [];  % remove neurons both correlated and anticorrelated
    %     index_corr_neg(ismember(icn, icp)) = [];
    index_corr_all = [index_corr_pos, index_corr_neg];
    
    % Display found correlations
    % ---------------------------------------------------------------------
    % Prepare canvas
    % --------------
    img_r = zeros(size(mean_img));      % red channel for correlations
    img_b = zeros(size(mean_img));      % blue channel for anticorrelations
    
    % Fill canvas with found correlations
    % -----------------------------------
    for idx_corr_pos = 1:length(index_corr_pos)
        idx_neuron = idx_neurons{index_corr_pos(idx_corr_pos)};
        img_r(idx_neuron) = 1;
    end
    
    for idx_corr_neg = 1:length(index_corr_neg)
        idx_neuron = idx_neurons{index_corr_neg(idx_corr_neg)};
        img_b(idx_neuron) = 1;
    end
    
    img_rgb = zeros(size(mean_img, 1), size(mean_img, 2), 3);
    img_rgb(:, :, 1) = mean_img + img_r;
    img_rgb(:, :, 2) = mean_img;
    img_rgb(:, :, 3) = mean_img + img_b;
    
    % Display
    % -------
    figure(layer);
    imshow(img_rgb);
    
    % Prepare new canvas for selected neurons
    % ---------------------------------------
    img_r_selected = zeros(size(mean_img));
    img_g_selected = zeros(size(mean_img));
    img_b_selected = zeros(size(mean_img));
    img_rgb_selected = cat(3, mean_img, mean_img, mean_img);
    
    % Begin interactive tool to select HBO groups
    % ---------------------------------------------------------------------
    clc
    fprintf('%s - HBO finder\n', F.name);
    ML.CW.line;
    fprintf('Layer %i : HBO groups selector\n', layer);
    valid_input = 0;
    next = false;
    while ~valid_input || next == true
        
        input_groups_selection = input('Do you want to select groups on this layer ? [y/n]\n > ', 's');
        
        switch input_groups_selection
            case 'y'
                valid_input = 1;
                continue_selection = 'y';
                
                while continue_selection == 'y'
                    
                    group_number = input('Enter the group number (1-10) : \n > ');
                    
                    % User group selection
                    % --------------------
                    figure(layer);
                    fprintf('Draw a polygon around the group.\n');
                    poly = drawpolygon('FaceAlpha', 0, 'LineWidth', 1.5) ;   % Define polygon around the group
                    xpolygon = poly.Position(:, 1);
                    ypolygon = poly.Position(:, 2);
                    delete(poly);
                    
                    % Create the group of selected corrrelated neurons
                    % ------------------------------------------------
                    % Create query points
                    [Xq, Yq] = meshgrid(1:size(mean_img, 2), 1:size(mean_img, 1));
                    % Determine what points of the grid are inside the polygon
                    in_polygon = inpolygon(Xq, Yq, xpolygon, ypolygon);
                    x_in_polygon = Xq(in_polygon);
                    y_in_polygon = Yq(in_polygon);
                    % Convert to linear indices
                    linear_ind = sub2ind(size(mean_img), y_in_polygon, x_in_polygon);
                    % Get neurons' indices from pixel indices
                    index_neurons_inside = unique(L(linear_ind));
                    % Limit to the (anti)correlated neurons
                    neurons_in_both = ismember(index_neurons_inside, index_corr_all);
                    index_neurons_group = index_neurons_inside(neurons_in_both);
                    
                    % Display selected group
                    % ----------------------
                    % Define a color
                    color_vect = color_matrix(group_number, :);
                    % Set colors
                    for idx_group = 1:length(index_neurons_group)
                        ind = idx_neurons{index_neurons_group(idx_group)};
                        img_r_selected(ind) = color_vect(1);
                        img_g_selected(ind) = color_vect(2);
                        img_b_selected(ind) = color_vect(3);
                    end
                    img_rgb_selected(:, :, 1) = img_rgb_selected(:, :, 1) + img_r_selected;
                    img_rgb_selected(:, :, 2) = img_rgb_selected(:, :, 2) + img_g_selected;
                    img_rgb_selected(:, :, 3) = img_rgb_selected(:, :, 3) + img_b_selected;
                    
                    figure(n_layers + layer);
                    imshow(img_rgb_selected);
                    drawnow;
                    
                    % Save index file
                    % ---------------
                    index_filename = make_index_filename(group_number);
                    
                    if exist(index_filename, 'file')
                        input_overwrite = input('Group already saved with the same source, overwrite or rename ? [O/r]\n > ', 's');
                        switch input_overwrite
                            case 'o'
                                
                            case 'r'
                                movefile(index_filename, [index_filename '.bak']);
                        end
                    end
                    
                    save(index_filename, 'index_neurons_group', 'layer', ...
                        'group_number', 'found_neurons', 'layer_found_neurons', 'corr_thresh');
                    
                    fprintf('%i neurons indices saved (layer %i, group %i)\n', numel(index_neurons_group), layer, group_number);
                    
                    % Continue interactive tool
                    % -------------------------
                    close(n_layers + layer);
                    figure(layer);
                    vi = true;
                    while vi
                        continue_selection = input('Do you want to select another group ? [y/n]\n > ', 's');
                        switch continue_selection
                            case 'n'
                                vi = false;
                                continue;
                            case 'y'
                                vi = false;
                            otherwise
                                vi = true;
                        end
                    end
                    
                end
                
                % Groups selection is done, show and save final image
                % ---------------------------------------------------
                imshow(img_rgb_selected);
                
                figure_subdir = [path_to_figures 'Neurons_source_' num2str(layer_found_neurons) '_' num2str(found_neurons) filesep];
                if ~exist(figure_subdir, 'dir')
                    mkdir(figure_subdir);
                end
                figure_filename = ['neurons_layer_' num2str(layer) '.tif'];
                imwrite(img_rgb_selected, [figure_subdir figure_filename]);
                
            case 'n'
                valid_input = 1;
            otherwise
                valid_input = 0;
        end
    end
end

fprintf('All done.\n');
close all