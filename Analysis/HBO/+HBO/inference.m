function [] = inference(study, dat, run, par)
% HBO.INFERENCE. Uses blind sparse deconvolution (BSD) algorithm to get rise
% and decays times of the HBO neurons.
%
% INPUTS :
% ------
% study : char, study name
% dat : char, date with format YYYY-MM-DD
% run : float, number of run
% par : boolean, use parallel BSD or not (default = false)

% --- Check input
if ~exist('par', 'var')
    par = false;
end

% --- Definitions
F = getFocus([study '/' dat], run);
hbo_dir = fullfile(pwd, 'Data', study, 'HBO', [dat, '_Run' sprintf('%02i', run)]);
hbo_mat = fullfile(hbo_dir, 'Matfiles', 'hbo_dff.mat');
tau_mat = fullfile(hbo_dir, 'Matfiles', 'time_constants.mat');

ML.CW.line;
fprintf('%s - HBO estimator\n', F.name);

% if exist(tau_mat, 'file')
%     return;
% end

% --- Load
data = load(hbo_mat, 'hbo_dff');
data = data.hbo_dff;
n_layers = size(data, 1);
n_groups = size(data, 2);

% --- Initialize output
tau_rises = cell(size(data));
tau_decays = cell(size(data));

% --- Estimate time constants
tic
for layer = 1:n_layers
    
    statusInfo(layer, n_layers, n_layers);
    
    for group = 1:n_groups
        
        signal = data{layer, group}';
        signal(signal == 0) = 1e-6;
        signal(signal > 100*std(signal)) = 1e-6;
        signal(isnan(signal)) = 1e-6;
        
        if isempty(signal)
            continue;
        end
        
        % Algorithm parameters
        Oalg = struct;                      % Struct of experimental conditions & decoding options.
        Oalg.Time = size(signal, 1);        % Number of time frames.
        Oalg.dt = n_layers*F.dt/1000;       % interval duration. (s)
        Oalg.nNeurons = size(signal, 2);    % Number of neurons.
        Oalg.adaptive = 1;
        Oalg.iterations = 10;
        Palg.tauRise = .1;
        Palg.tauDecay = 3;
        
        if par
            [~, ~, ~, Pphys] = pBSD(signal, Oalg, Palg);
        else
            [~, ~, ~, Pphys] = BSD(signal, Oalg, Palg);
        end
        
        tau_rises{layer, group} = Pphys.tauRise;
        tau_decays{layer, group} = Pphys.tauDecay;
        
    end
end

% --- Save
save(tau_mat, 'tau_rises', 'tau_decays');

fprintf('\t Done in %2.2fs.\n', toc);
end