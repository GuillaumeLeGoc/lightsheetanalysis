function [] = merge(study, dat, run)
% HBO.MERGE(study, dat, run)
% Fetch all HBO neurons indices stored in study/HBO/dat/run. Removes
% duplicates and saves them as a single matfile in a n_layers x n_groups
% cell.

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
root = '/home/ljp/Science/Projects/Neurofish/Data/';
% study = 'Thermotaxis';
% dat = '2017-11-15';
% run = 8;

n_groups = 10;

% Define directories
% ------------------
hbo_dir = [root study filesep 'HBO' filesep];
hbo_exp = [hbo_dir dat '_Run' sprintf('%02i', run) filesep];
hbo_idx = [hbo_exp 'Index' filesep];

% Output
% ------
output_matfiles = [hbo_exp filesep 'Matfiles' filesep 'hbo_dff.mat'];

% Gather HBO index and DF/F
% -------------------------------------------------------------------------
% Get Focus
% ---------
F = getFocus([study filesep dat], run);

fprintf('%s - HBO merger\n', F.name);
ML.CW.line;

% Complete parameters
% -------------------
n_layers = length(F.sets);

% Get HBO index and corresponding DF/F
% ------------------------------------
list_index = dir([hbo_idx '*.mat']);       % list matfiles in directory
n_files = length(list_index);

if n_files == 0
    error('No index files found.');
end

if ~exist([hbo_exp filesep 'Matfiles' filesep], 'dir')
    mkdir([hbo_exp filesep 'Matfiles' filesep]);
end

make_index_filename = @(n) [list_index(n).folder filesep list_index(n).name];

hbo_index = cell(n_layers, n_groups);
hbo_dff = cell(n_layers, n_groups);

fprintf('Loading HBO index and DF/F... ');
tic;
for idx_file = 1:n_files
    
    filename = make_index_filename(idx_file);
    indices = load(filename);
    
    % HBO DF/F
    F.select(indices.layer);
    
    DFF = F.matfile('@DFF');
    DFF = DFF.load('neurons');
    DFF = DFF.neurons;
    
    % HBO index (handling different version of files)
    if isfield(indices, 'group_number')
        % New version
        old = hbo_index{indices.layer, indices.group_number};       % already stored
        new = indices.index_neurons_group;                          % new indices
        new(ismember(new, old)) = [];         
        % remove duplicates
        if ~isempty(new)
            hbo_index{indices.layer, indices.group_number} = cat(1, old, new);
        end
        hbo_dff{indices.layer, indices.group_number} = cat(1, hbo_dff{indices.layer, indices.group_number}, DFF(indices.index_neurons_group, :));
    elseif isfield(indices, 'num_group')
        % Old version
        old = hbo_index{indices.layer, indices.num_group};          % already stored
        new = indices.ind_group;                                    % new indices
        new(ismember(new, old)) = [];                               % remove duplicates
        hbo_index{indices.layer, indices.num_group} = cat(1, old, new);
        hbo_dff{indices.layer, indices.num_group} = cat(1, hbo_dff{indices.layer, indices.num_group}, DFF(indices.ind_group, :));
    end

end
save(output_matfiles, 'hbo_index', 'hbo_dff');

fprintf('Done and saved (%2.2fs).\n', toc);