% Convert HbO neuron indices to new indices in the whole brain reference frame.


% list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list_all.txt';
list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/RandomPulses_list.txt';
hbo_dir = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/HBO/';
study = 'Thermotaxis';

overwrite = 'y';

make_input_name = @(d, r) fullfile(hbo_dir, strcat(d, '_Run', sprintf('%02i', r)), 'Matfiles', 'hbo_dff.mat');
make_output_name = @(d, r) fullfile(hbo_dir, strcat(d, '_Run', sprintf('%02i', r)), 'Index', strcat(d, '_Run', sprintf('%02i', r), '_hbo_indices.txt'));
% make_output_name_2 = @(id) [pwd filesep 'Data' filesep 'Thermotaxis' filesep 'HBO' filesep 'Spontaneous' filesep 'Fish' id '.txt'];
make_output_name_2 = @(id,r) [pwd filesep 'Data' filesep 'Thermotaxis' filesep 'HBO' filesep 'RandomPulses' filesep 'Fish' id '_run' num2str(r) '.txt'];

exp = readtable(list, 'DatetimeType', 'text');

for idx_date = 1:size(exp, 1)
    
    dat = char(exp.Date(idx_date));
    
    if strcmp(dat(1), '#')
        continue;
    end
    
    runs = exp.RunNumber(idx_date);
    fishid = exp.FishID(idx_date);
    
    if iscell(runs)
        runs = eval(strcat('[', char(runs), ']'));
    end
    
    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        
        F = getFocus([study '/' dat], run);
        
        hbo_file = matfile(make_input_name(dat, run));
        
        indices = hbo_file.hbo_index;
        n_layers = size(indices, 1);
        n_groups = size(indices, 2);
        
        new_indices = NaN(0, n_groups);
        
        save_file = make_output_name(dat, run);
        save_file_2 = make_output_name_2(num2str(fishid), run);
        
        if exist(save_file, 'file')
            switch overwrite
                case 'y'
                    delete(save_file);
                case 'n'
                    continue;
            end
        end
        
        for layer = 1:n_layers
            
            inds = indices(layer, :);
            
            if all(cellfun(@isempty, inds))
                continue;
            end
            
            subindices = NaN(max(cellfun(@numel, inds)), n_groups);
            for group = 1:n_groups
                      
                subindices(1:numel(inds{group}), group) = convertIndices(F, inds{group}, layer);
                
            end
            
            new_indices = cat(1, new_indices, subindices);
            
        end
        
        dlmwrite(save_file, new_indices, 'delimiter', '\t');
        dlmwrite(save_file_2, new_indices, 'delimiter', '\t');
        
    end
end