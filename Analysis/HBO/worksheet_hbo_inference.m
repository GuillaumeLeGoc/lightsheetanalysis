% Perform adaptative BSD to get time constants on neurons HbO.

list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list.txt';
hbo_dir = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/HBO/';
study = 'Thermotaxis';

exp = readtable(list, 'DatetimeType', 'text');

for idx_date = 1:size(exp, 1)
    
    dat = char(exp.Date(idx_date));
    
    if strcmp(dat(1), '#')
        continue;
    end
    
    runs = exp.RunNumber(idx_date);
    
    if iscell(runs)
        runs = eval(strcat('[', char(runs), ']'));
    end
    
    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        
        HBO.inference(study, dat, run, true);
        
    end
end