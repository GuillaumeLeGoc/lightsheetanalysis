% Use Tubiana's BSD algorithm to estimate GCaMP time constants.

clear
clc

% Experiment
% ----------
study = 'Thermotaxis';
dat = '2019-05-03';
out_path = '/home/ljp/Science/Projects/Neurofish/Data/Time_Constants/';
run = 5;

% Parameters
% ----------
start = 60;                 % time points to take into account (seconds)
stop = 960;
guess_rise = .5;
guess_decay = 3;            % educated guess to find time constants (seconds)
par = 'y';                  % use parallel BSD

% Load data
% ---------
F = getFocus([study filesep dat], run);

% Complete parameters
P = NT.Parameters;
P.load(fullfile(F.Data, 'Parameters.txt'));
dt_scan = P.CycleTime/1000;        % acquisition time for one layer
n_layers = length(F.sets);

tau_rises = cell(n_layers, 1);
tau_decays = cell(n_layers, 1);

for layer = 1:n_layers
    
    F.select(layer);
    dff = load(F.fname('@DFF'), 'neurons');
    dff = dff.neurons(:, round(start/dt_scan):round(stop/dt_scan));
    dff = dff';                         % flip array to match BSD expectations
    
    dff(abs(dff) > 10*std(dff(:))) = 1e-3;
    dff(isnan(dff)) = 1e-3;
    dff(dff == 0) = 1e-3;
    
    % Inference algorithm parameters
    Oalg = struct;                      % Struct of experimental conditions & decoding options.
    Oalg.Time = size(dff, 1);           % Number of time frames.
    Oalg.dt = dt_scan;                  % interval duration. (s)
    Oalg.nNeurons = size(dff, 2);       % Number of neurons.
    Oalg.adaptive = 1;                  % Adaptative, refine given parameters
    Oalg.iterations = 5;                % Number of iterations for estimating parameters
    Oalg.thresholdBeforeKernelInference = 1;
    
    Palg = struct;
    Palg.tauRise = guess_rise;             % Fluorescence raise time (s)
    Palg.tauDecay = guess_decay;           % Fluorescence decay time (s)
    
    tic
    switch par
        case 'y'
            fprintf('Estimating time constants... ');
            [~, ~, ~, Pphys] = pBSD(dff, Oalg, Palg);
        case 'n'
            fprintf('Estimating time constants... ');
            [~, ~, ~, Pphys] = BSD(dff, Oalg, Palg);
    end
    
    fprintf('Done (%2.2fs).\n', toc)
    
    tau_rises{layer} = Pphys.tauRise;
    tau_decays{layer} = Pphys.tauDecay;
    
end

tau_rises = cat(2, tau_rises{:});
tau_decays = cat(2, tau_decays{:});

figure;
subplot(121);
histogram(tau_rises, 100);
xlabel('\tau_{Rise} [s]');
subplot(122);
histogram(tau_decays, 100);
xlabel('\tau_{Decay} [s]');

R.mean = mean(tau_rises);
R.median = median(tau_rises);
R.std = std(tau_rises);

D.mean = mean(tau_decays);
D.median = median(tau_decays);
D.std = std(tau_decays);

fprintf('Rising time : \n\t Mean = %12.8fs \n\t Median = %12.8fs \n\t Std = %12.8fs \n', ...
    R.mean, R.median, R.std);
fprintf('Decay time : \n\t Mean = %12.8fs \n\t Median = %12.8fs \n\t Std = %12.8fs \n', ...
    D.mean, D.median, D.std);

name = [study '_' dat '_' num2str(run) '_times.mat'];
save([out_path name], 'tau_rises', 'tau_decays');