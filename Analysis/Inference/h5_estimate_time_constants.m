% Infer GCaMP kernel time constants with BSD on specified dataset.
%
% Requirements :
% ------------
% BSD : https://github.com/jertubiana/BSD
% FisH5 : https://gitlab.com/GuillaumeLeGoc/fish5
%
% Notes :
% -----
% Rem 1 : you can avoid FisH5 by loading the dataset directly with h5read.
% Rem 2 : If the file doesn't have our "standard" metadata, fill the
% parameters (number of neurons...) manually.
% Rem 3 : BSD doesn't converge quite often and results in an error. To
% overcome this issue, here's what can help :
%   - Remove aberrant values from signal.
%   - Add the Oalg.thresholdBeforeKernelInference = 1; parameter.
%   - Use the modified BSD found in
%   https://gitlab.com/GuillaumeLeGoc/lightsheetanalysis that stick to the
%   previous estimated parameters if the algorithm has an error.
% Rem 4 : Here the signal is time aligned on the main time vector, but it
% probably doesn't change anything for time constants estimation.
% Rem 5 : On RAM limited device you might want to split the signal to
% process it.

% --- Parameters
h5fname = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Datasets/20190502_Run03_spontaneous14_Tset=26.h5';
dataset = 'dff';    % specify the full path to the dataset if it doesn't work.
par = 'y';          % 'y' or 'n' : use parallel processing.

% Options :
regions = 106;      % Use only a ZBrainAtlas region, leave empty for all all.
time_steps = [];    % Use only a subset of time steps, leave empty for all.

% --- BSD options
Oalg.adaptive = 1;              % Adaptative, refine given parameters
Oalg.iterations = 5;           	% Number of iterations for estimating parameters
Palg.tauRise = 0.01;            % Fluorescence raise time (s)
Palg.tauDecay = 2.5;            % Fluorescence decay time (s)

% --- Get data 
% [change manually if you don't use FisH5 package]
fprintf('Loading & aligning data...'); tic
H = FisH5(h5fname);
time = H.load('time', 't', time_steps);
neurons_inds = H.findNeuronsInRegion(regions);
signal = H.load(dataset, 'n', neurons_inds, 't', time_steps);
signal = H.timealign(time, signal, 1:H.nneurons);   % Align data on the time vector
fprintf('\t Done (%2.2fs).\n', toc);

% --- Complete BSD options 
% [change manually if H5 file doesn't have correct metadata]
Oalg.nNeurons = size(signal, 1);         % Number of neurons.
Oalg.Time = size(signal, 2);             % Number of time frames.
Oalg.dt = 1/H.framerate;                 % interval duration. (s)

% --- Processing
% It runs in 10min for 10000neurons, 10000 time steps on 8 cores @3.2GHz.
fprintf('Processing...'); tic
switch par
    case 'y'
        [~, ~, ~, Pphys] = pBSD(signal', Oalg, Palg);
    case 'n'
        [~, ~, ~, Pphys] = BSD(signal', Oalg, Palg);
    otherwise
        error('Specify par = ''y'' or ''n''.');
end
fprintf('\t Done (%2.2fs).\n', toc);

tau_rises = Pphys.tauRise;
tau_decays = Pphys.tauDecay;

% --- Stats & Display
R.mean = mean(tau_rises);
R.median = median(tau_rises);
R.std = std(tau_rises);

D.mean = mean(tau_decays);
D.median = median(tau_decays);
D.std = std(tau_decays);

fprintf('Rising time : \n\t Mean = %12.8fs \n\t Median = %12.8fs \n\t Std = %12.8fs \n', ...
    R.mean, R.median, R.std);
fprintf('Decay time : \n\t Mean = %12.8fs \n\t Median = %12.8fs \n\t Std = %12.8fs \n', ...
    D.mean, D.median, D.std);

figure;
subplot(211);
histogram(tau_rises, 100);
xlabel('\tau_{rise} [s]');
subplot(212);
histogram(tau_decays, 100);
xlabel('\tau_{decay} [s]');