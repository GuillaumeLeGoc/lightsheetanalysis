% Deconvolve neurons calcium signal to get spikes (example of utilisation).
% Saves spikes in files with signals in signal_stack folder.

close all
clear
clc

% pause(10*60*60);       % wait for some time

% --- Experiment
study = 'Thermotaxis';

% Manual
% dat = '2018-01-10';
% runs = [3, 4];
% gcline = 'nucslow';

% Automatic from list
% Comment following line to process only specified run
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list.txt';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Spontaneous/Spontaneous_list.txt';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list_all.txt';
experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/RandomPulses_list.txt';

% --- Options
overwrite = 'y';    % overwrite existing files
par = 'y';          % use parallelized function

% GCaMP6f time constants
fast.tauRise = 0.15;
fast.tauDecay = 1.6;

% GCaMP6s time constants 
slow.tauRise = 0.2;
slow.tauDecay = 3.55;

% Read dates (only if experiment list specified)
if exist('experiment_list', 'var')
    list = readtable(experiment_list, 'DatetimeType', 'text');
    n_exp = size(list, 1);
else
    n_exp = 1;
end

fprintf('Infering spikes...\n');

for idx_dates = 1:n_exp
    
    if exist('experiment_list', 'var')
        % Read date and runs list
        dat = char(list.Date(idx_dates));
        if strcmp(dat(1), '#')
            continue
        end
        
        if isa(list.RunNumber, 'double')
        % Handles when only one day in a day and runs list is read as an
        % array directly
            runs = list.RunNumber(idx_dates);
        else
            runs = eval(['[' list.RunNumber{idx_dates} ']']);
        end
        
        gcline = list.Line(idx_dates);
    end
    
    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        
        F = getFocus([study filesep dat], run);
        
        tic
        fprintf('%s ...', F.name);
        
        n_layers = length(F.sets);
        dt = F.dt*1e-3;
        F.select(1)
        reftime = F.set.t/1000;
        
        for layer = 1:n_layers
            
            F.select(layer);
            
            % Check if spikes file exist
            if exist(F.fname('@Spikes'), 'file')
                switch overwrite
                    case 'n'
                        continue;
                    case 'y'
                        copyfile(F.fname('@Spikes'), [F.fname('@Spikes') '.bak']);
                    otherwise
                        error('Specify ''y'' or ''n'' for overwriting existing file.');
                end
            end
            
            % Load signal
            signal = load(F.fname('@DFF'), 'neurons');
            signal = double(signal.neurons');                   % flip signal to match BSD expectations
            signal(abs(signal) > 100*std(signal(:))) = 1e-6;    % remove artifact
            times = F.set.t/1000;
            
            signal = interp1(times, signal, reftime, 'pchip', 'extrap');

            % Inference algorithm parameters
            Oalg = struct;                      % Struct of experimental conditions & decoding options.
            Oalg.Time = size(signal, 1);        % Number of time frames.
            Oalg.dt = mean(diff(times));        % interval duration. (s)
            Oalg.nNeurons = size(signal, 2);    % Number of neurons.
            Oalg.adaptive = 0;                  % Not adaptive = Will use provided values for parameters, and estimate the unknown ones.
            
            Palg = struct;
            if ~Oalg.adaptive
                if contains(gcline, 'slow', 'IgnoreCase', true)
                    Palg.tauRise = slow.tauRise;             % Fluorescence raise time (s)
                    Palg.tauDecay = slow.tauDecay;           % Fluorescence decay time (s)
                elseif contains(gcline, 'fast', 'IgnoreCase', true)
                    Palg.tauRise = fast.tauRise;             % Fluorescence raise time (s)
                    Palg.tauDecay = fast.tauDecay;           % Fluorescence decay time (s)
                else
                    warning('GCaMP fast or slow not recognized, skipping.');
                    continue;
                end
            end
            
            switch par
                case 'y'
                	% paralellized
                    [spikes, spikes_conv, pAlg, pPhys] = pBSD(signal, Oalg, Palg);   % Blind sparse deconvolution from Tubiana
                case 'n'
                    [spikes, spikes_conv, pAlg, pPhys] = BSD(signal, Oalg, Palg);    % Blind sparse deconvolution from Tubiana
            end

            spikes_bin = spikes > pPhys.threshold;

            % Get back to usual order (n_neurons x  n_times)
            spikes = spikes';
            spikes_conv = spikes_conv';
            spikes_bin = spikes_bin';
            
            % Save
            save(F.fname('@Spikes'), 'spikes', 'spikes_bin', 'spikes_conv', 'pAlg', 'pPhys');
            dff_aligned = signal';
            save(F.fname('@DFF_Aligned'), 'dff_aligned');
        end
        
        fprintf(' Done (%2.2fs).\n', toc);
    end
end
fprintf('All done.\n');