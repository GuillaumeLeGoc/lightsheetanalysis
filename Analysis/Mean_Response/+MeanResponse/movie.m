function [] = movie(F, time_before, time_after, motion_thresh, binsize)
% Creates a movie from averaged frames around stimulations. Images are corrected
% for drift, and resized according to binsize. TIME_BEFORE and TIME_AFTER
% defines the averaging window in seconds.

% Complete parameters
dt = F.dt*1e-3;
n_layers = length(F.sets);
dt_brain = dt*n_layers;
time_total = time_before + time_after;

% Load stimulus onsets
stim = getStimTimes(F.Data);

% Convert times in frames
stim_im = floor(stim/dt_brain);
time_after_im = floor(time_after/dt_brain);     % to discard after a motion
time_before_im = floor(time_before/dt_brain);
time_total_im = floor(time_total/dt_brain);

% Define and create directories
% Output
output_path = 'Mean_Movie';     % relative to F.Data

raw_dir = [F.Data output_path filesep 'Raw' filesep];
dif_dir = [F.Data output_path filesep 'Difference' filesep];

if ~exist([F.Data output_path], 'dir')
    mkdir(raw_dir);
    mkdir(dif_dir);
end

mean_raw_name = @(n) [raw_dir 'Image_' sprintf('%04i', n) '.tif'];
mean_dif_name = @(n) [dif_dir 'Image_' sprintf('%04i', n) '.tif'];

fprintf('%s : Creating mean movie\n', F.name);
ML.CW.line;

% Loop over layers
% ---------------------------------------------------------------------

for layer = 1:n_layers
    
    file_count = -n_layers + layer - 1;
    
    tic;
    F.select(layer);
    fprintf('Layer #%i... ', layer);
    
    % Discard stimulus with motions
    % -----------------------------
    drifts = load(F.fname('IP/@Drifts'));       % Load drifts
    drift = [drifts.dx' drifts.dy'];
    motion_times = extractMotionTimes(drift, motion_thresh, time_after_im, 'sym');
    stim_wm = stim_im;      % stim_wm expressed as frames
    stim_wm(ismember(stim_im, motion_times)) = [];  % stim onsets without motion
    
    if numel(stim_wm) < 0.5*numel(stim)
        error('Not enough stimulation remaining.');
    end
    fprintf('%i/%i stimulations remaining.\n', numel(stim_wm), numel(stim));
    fprintf('[');
    % Loop to construct time points around stimulus
    % -----------------------------------------------------------------
    for ti = 0:time_total_im
        
        if mod(ti, 10) == 0
            fprintf('#');
        end
        
        % Define time points
        % ------------------
        time_points = stim_wm - time_before_im + ti;
        
        % Prepare drift correction with the first image
        % ---------------------------------------------
        bkg = load(F.fname('IP/@Background'), 'mean_first');
        img_1 = F.iload(time_points(1));
        img_1.rm_infos('rep', bkg.mean_first);  % remove timestamps
        img_1 = img_1.pix;
        %[yim, xim] = meshgrid(1:size(img_1, 2), 1:size(img_1, 1));
        img_1 = imresize(img_1, 1/binsize);     % bin image
        img_mean = img_1;                       % store first image for averaging
        
        % Loop over all time points
        % -------------------------------------------------------------
        for time_step = time_points'
            
            img_2 = F.iload(time_step);
            img_2.rm_infos('rep', bkg.mean_first);     % remove timestamp
            
            % Correct drift
            %img_2.translate_old(-drifts.dy(time_step), -drifts.dx(time_step));
            img_2.translate(-drifts.dy(time_step), -drifts.dx(time_step));
            img_2 = img_2.pix;
            %img_2 = interp2(yim, xim, img_2, yim + drifts.dy(time_step), xim + drifts.dx(time_step));
            img_2 = imresize(img_2, 1/binsize);  % bin
            img_mean = img_mean + img_2;         % for average
            
        end
        
        img_mean = img_mean/numel(stim_wm);
        
        if ti == 0
            img_to_remove = img_mean;
        end
        
        img_diff = img_mean - img_to_remove;
        
        file_count = file_count + n_layers;
        imwrite(uint16(img_mean), mean_raw_name(file_count));
        imwrite(uint16(img_diff), mean_dif_name(file_count));
    end
    fprintf('] ');
    fprintf('Done for layer %i (%2.2fs).\n', layer, toc);
end

fprintf('Creating black stack... ');
tic;
createBlackStack(F, time_total_im + 1, 'Mean_Movie');
fprintf('Done (%2.2fs).\n', toc);
end