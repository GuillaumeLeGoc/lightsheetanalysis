function [peristim_time, peristim_sig] = periStimMean(time, signal, stim, n_layers, time_before, time_after)
% This function averages the signal around specified events. The signal is
% oversampled on a time basis common to all layers.
%
% INPUTS :
% ------
% time : time vector where signal has been sampled.
% signal : signal to average. n_times x n_pixels - time series are on columns
% for interpolation. Flipped if doesn't match time dimension.
% stim : stimulation onsets in seconds, signal is averaged around the time points in
% this vector. 
% n_layers : number of layers to get oversampling factor.
% time_before : defines the low limit of the averaging window around stimulus, in seconds.
% time_after : defines the high limit of the averaging window around stimulus, in seconds.
%
% OUTPUTS : 
% -------
% peristim_time : time vector corresponding to the average, 0 is the onset
% of the stimuli.
% peristim_sig : averaged signal around stimulus.
%
% 2018-01-22 GLG

% Complete parameters
% -------------------
dt_layer = mean(diff(time));
dt = dt_layer/n_layers;
n_times = numel(time);

% Check input size
% ----------------
if size(signal, 1) ~= n_times
    signal = signal';
    warning('Signal has been flipped.');
end
if size(stim, 2) == 1 && numel(stim) ~= 1
    stim = stim';
end

% Convert window in frames
% ------------------------
time_before_im = round(time_before/dt);
time_after_im = round(time_after/dt);

% Define time vectors
% -------------------
query_time_full = linspace(0, n_times*dt_layer, n_times*n_layers)';

% Loop over stimulus onsets
% -------------------------
peristim_sig = 0;
for stim_onset = stim
    % Find closest available time
    [~, idx_closest_time] = min(abs(query_time_full - stim_onset));
    % Define time points to interpolate the signal
    time_points = (idx_closest_time - time_before_im):(idx_closest_time + time_after_im);
    query_time = query_time_full(time_points);
    % Oversample the signal
    interp_signal = interp1(time, signal, query_time);
    % Add the new values
    peristim_sig = peristim_sig + interp_signal;
end

peristim_sig = peristim_sig/numel(stim);    % compute the mean signal around stimulus
peristim_time = linspace(-time_before, time_after, size(peristim_sig, 1)); % corresponding time vector, 0 is the real onset
end