% 2018-01-18 Create stimulus averaged movie. Works directly with raw images,
% drift-corrected. Saves the movie as tiff images in a directory.

clear;
close all
clc;

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
study = 'Thermotaxis';
dat = '2018-07-04';
runs = [3];

% Options
% -------
% Averaging window
time_before = 1;            % time before stim (s)
time_after = 30;            % time after stim (s)
motion_thresh = 100;          % ignore stimulation if motion occured
binsize = 1;

checkdate = datevec(dat);

% Loop over runs
% -------------------------------------------------------------------------

for run = runs
    
   
    F = getFocus([study filesep dat], run);
    MeanResponse.movie(F, time_before, time_after, motion_thresh, binsize);
    
end