% 2018-02-08 Create response maps from neurons' DF/F.
% The response is defined as the mean DF/F after the stimulus,
% stim-averaged. Response is saved as a matfile in signal_stack.

clear;
close all
clc;

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
study = 'Thermotaxis';
dat = '2018-01-10';
runs = 3;

% Options
% -------
% Averaging window
time_before = 1;        % time before stim (s)
time_after = 8;         % time after stim (s)
integrate_after = 8;    % compute mean response until integrate_after seconds
layer_motion = 5;       % choose this layer for motion
motion_thresh = 3;      % ignore stimulation if motion occured
dff_thresh = 10;        % consider DF/F > dff_thresh*std(dff) as artifacts and remove them
useGpu = 'y';           % use GPU for interpolation (not recommended for dff.mat > 1GB)

% Define grey stack directories relative to F.Data
gre_dir = 'grey_stack';

% Define output
output_path = ['Mean_Response' filesep 'dff_neurons' filesep];
pos_name = 'mean_dff_Pos';
neg_name = 'mean_dff_Neg';
sig_name = 'mean_sig';

checkdate = datevec(dat);

% Loop over runs
% -------------------------------------------------------------------------

for run = runs
    
    % Get focus
    if checkdate(2) < 9 && checkdate(1) < 2018
        F = getFocus([dat ' ' study], run);
    else
        F = getFocus([study filesep dat], run);
    end
    
    % Complete parameters
    dt = F.dt*1e-3;
    n_layers = length(F.sets);
    dt_brain = dt*n_layers;
    
    % Function to load grey_stack image
    mean_img_name = @(n)([F.Data gre_dir filesep 'Image_' sprintf('%02i', n) '.tif']);
    
    % Load stimulus onsets
    stim = getStimTimes(F.Data);
    
    % Convert times in frames
    stim_im = floor(stim/dt_brain);
    time_after_im = floor(time_after/dt_brain);     % to discard after a motion
    
    % Create output structures and directories;
    peristim_dff = struct;  % will contain mean dff around stimulus
    R = struct;             % will contain mean response integrated after stimulus
    
    % Create directories
    mkdir([F.Data output_path]);
    mkdir([F.Data output_path pos_name]);
    mkdir([F.Data output_path neg_name]);
    mkdir([F.Data output_path sig_name]);
    
    timer = 0;      % initialise timer
    
    fprintf('%s : Computing mean response map\n', F.name);
    ML.CW.line;
    
    % Discard stimulus with motions
    % -----------------------------
    fprintf('Discarding stimulus with motions ');
    motion_times = getMotionTimes(F, layer_motion, time_after_im, 'sym');
    stim_wm = stim;
    stim_wm(ismember(stim_im, motion_times)) = [];  % stim onsets without motion
    
    if numel(stim_wm) < 0.5*numel(stim)
        error('Not enough stimulation remaining.');
    end
    fprintf('(%i/%i remaining).\n', numel(stim_wm), numel(stim));
    
    % Loop over layers
    % ---------------------------------------------------------------------
    for layer = 1:n_layers
        
        F.select(layer);
        
        fprintf('Layer #%i...\n', layer);
        
        % Average DF/F around stimulus
        % -----------------------------------------------------------------
        
        % Load DF/F and index
        % -------------------
        tic;
        fprintf('Loading DF/F and indices... ');
       
        dff = F.matfile('@DFF');
        dff = dff.load('neurons');
        dff = dff.neurons;
        
        Brain = F.matfile('IP/@Brain');
        bbox = Brain.load('bbox');              % cropped image around brain
        bbox = bbox.bbox;
        
        Neurons = F.matfile('IP/@Neurons');
        neurons_index = Neurons.load('ind');
        neurons_index = neurons_index.ind;      % Each cell contains the neuron's pixels index
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
        
        % Compute mean DF/F around stimulus
        % ---------------------------------
        tic
        switch useGpu
            case 'y'
                dff = gpuArray(dff);
        end
        fprintf('Computing mean DF/F around stimulus... ');
        [time, mean_dff] = periStimMean(dff', stim_wm, time_before, time_after, F);
        mean_dff = gather(mean_dff');
        peristim_dff.mean_dff = mean_dff;
        peristim_dff.mean_time = time;
        peristim_dff_file = F.matfile('@Peristim_DFF');
        peristim_dff_file.save(peristim_dff, 'peristim_DFF');
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
        
        % Compute mean response after a stimulation
        % -----------------------------------------------------------------
        tic;
        fprintf('Computing mean response after stimulus... ');
        [~, onset] = min(abs(time));      % find closest time to stimuli onset
        offset = onset + fix(integrate_after/(mean(diff(time))));  % time after
        % Mean response ('integral') with first value removed
        dff_mean_response = mean(mean_dff(:, onset:offset) - mean_dff(:, onset), 2);
        R.dff_mean_response = dff_mean_response;
        Response = F.matfile('Response');
        Response.save(R, 'Response');
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
        
        % Save the response as an image
        % -----------------------------------------------------------------
        tic;
        fprintf('Saving response as image... ');
        mean_img = imread(mean_img_name(layer));
        img_pos = zeros(size(mean_img));    % canvas for positive response
        img_neg = zeros(size(mean_img));    % canvas for negative response
        
        % Positive response
        % -----------------
        pre_canvas_pos = zeros(bbox(4) - bbox(3) + 1, bbox(2) - bbox(1) + 1);
        R_pos = R.dff_mean_response;
        R_pos(R_pos<0) = 0;
        R_pos(R_pos > dff_thresh*std(R_pos)) = 0;      % remove artifacts
        
        % Negative response
        % -----------------
        pre_canvas_neg = zeros(bbox(4) - bbox(3) + 1, bbox(2) - bbox(1) + 1);
        R_neg = R.dff_mean_response;
        R_neg(R_neg > 0) = 0;
        R_neg = - R_neg;
        R_neg(R_neg > dff_thresh*std(R_neg)) = 0;      % remove artifacts
        
        % Fill canvases with responses values
        % -----------------------------------
        for idx_neuron = 1:length(neurons_index)
            
            px_idx = neurons_index{idx_neuron};
            
            pre_canvas_pos(px_idx) = R_pos(idx_neuron);   % Positive response
            pre_canvas_neg(px_idx) = R_neg(idx_neuron);   % Negative response
        end
        
        img_pos(bbox(3):bbox(4), bbox(1):bbox(2)) = pre_canvas_pos;
        img_neg(bbox(3):bbox(4), bbox(1):bbox(2)) = pre_canvas_neg;
        
        % Save images
        % -----------
        img_pos = uint16(400*img_pos);
        img_neg = uint16(400*img_neg);
        
        imwrite(img_pos, [F.Data output_path pos_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
        imwrite(img_neg, [F.Data output_path neg_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
        imwrite(mean_img, [F.Data output_path sig_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
        
        fprintf('Done (%2.2fs).\n', toc);
        timer = timer +  toc;
        
    end
end
fprintf('%i maps computed in %2.2fs.\n', numel(runs), timer);