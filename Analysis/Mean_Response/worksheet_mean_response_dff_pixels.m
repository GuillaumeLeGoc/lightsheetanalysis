% Mean response computation to build response
% maps. The DF/F signals is interpolated to have the same time vector for
% all layers, and DF/F is averaged around stimulus without motions. Then
% the integreal is computed from the stim onset to have a number
% caracterising the response. Responses are stored in matfiles and as
% images. In the latter, DF/F > dff_thresh are set to maximum value and removed.

clear;
close all
clc;

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
study = 'Thermotaxis';
% dat = '2018-06-05';
% runs = [2];

list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/RandomPulses_list.txt';

% Options
% -------
time_before = 0;        % time before stim (s)
time_after = 8;         % time after stim (s)
integrate_after = 8;    % compute mean response until integrate_after seconds
layer_motion = 5;       % choose this layer for motion
motion_thresh = 8;      % ignore stimulation if motion occured
dff_thresh = 5;         % consider DF/F > dff_thresh as artifacts and remove them

% Loop over runs
% -------------------------------------------------------------------------
exp_list = readtable(list);
n_exp = numel(exp_list);

for idx_dates = 1:n_exp
    
    % Read date and runs list
    dat = char(exp_list.Date(idx_dates));
    runs = eval(['[' exp_list.RunNumber{idx_dates} ']']);

    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        
        % Get focus
        F = getFocus([study filesep dat], run);
        
        MeanResponse.dffMean(F, time_before, time_after, integrate_after, layer_motion, motion_thresh, dff_thresh);
    end
end