function [dff, baseline, index, noise] = dff_pixels(F, fast, span)
% Extraction.dff_pixels.
% This functions extracts DF/F from raw signals in F.Data/signal_stacks and
% stores it in the same folder along with the baseline and the noise.
% DF/F is defined as (Fluo (- Background) - baseline)/baseline.
% Uses following functions : fast_baseline, progression_info, noise_dff.
%
% INPUT
% F : Focus object, focused on a layer (F.select(layerID))
% fast : boolean, use parfor loop or not.
% span : if specified, baseline is smoothed with a window of size span
% (seconds)
%
% OUTPUT
% dff : DF/F, rows are time series of a single pixel
% baseline : baseline, rows are time series of a single pixel
% index : pixels' index from reference image
% noise : computed noise

% Preparation
% ------------------------------------------------------------------------------
% Fill default values
% -------------------
window = 50;        % moving window in seconds
percen = 8;         % nth percentile

% Complete parameters
% -------------------
dt = F.dt*1e-3;
Nlayer = length(F.sets);
dt_brain = dt*Nlayer;
layer = F.set.id;

% Load files
% ----------
Background = F.matfile('IP/@Background');       % Background
bg = Background.load('mean_first');
bg = bg.mean_first;                             % background properly computed

fluo = load([F.Data 'signal_stacks' filesep num2str(layer) filesep 'sig.mat'], 'signal_stack');
index = load([F.Data 'signal_stacks' filesep num2str(layer) filesep 'sig.mat'], 'index');

% Handle old version where data is a structure
if ~isfield(fluo, 'signal_stack')
    data = load([F.Data 'signal_stacks' filesep num2str(layer)], 'DD');
    fluo = data.DD.signal_stack;
    index = data.DD.index;
else
    fluo = fluo.signal_stack;
    index = index.index;
end

% Compute baseline and DF/F
% ------------------------------------------------------------------------------
% Compute baseline
% ----------------
baseline = computeBaseline(fluo, percen, round(window/dt_brain), fast);

% Smooth baseline with moving mean window
if exist('span', 'var')
    baseline = smoothdata(baseline, 2, 'movmean', span*dt);
end

% Convert to single datatype (smaller files)
baseline = single(baseline);
fluo = single(fluo);

baseline(baseline == 0) = 1;                % remove 0
dff = (fluo - baseline)./(baseline - bg);	% DF/F

% Extract estimated noise
if nargout > 3
    noise = extractNoise(dff);          % compute estimated noise
end
end