function [signal_stack, index, greystack] = fluo_pixels(F, binsize, regist)
% This functions stores raw fluo data from a set of images into a matrix
% where each row is a time serie associated to a pixel within the brain
% mask.
% INPUTS :
% ------
% F: Focus object, focused on a layer.
% binsize: Image will be resized with a factor 1/binsize.
% regist: Wether or not you want to register the image skeleton for further
% correction. Slower and induces weird effect on the mean image.

% OUTPUTs :
% -------
% signal_stack : Raw fluo signal matrix.
% index : Linear indices of pixels within the brain mask.
% greystack : Mean image, averaged over each image of the layer.

% Fill default values
% -------------------------------------------------------------------------
if regist
    sigma = 5*[1 1.5];              % Gaussian filter sigma
end

% Get the pixel list
% -------------------------------------------------------------------------
index = load(F.fname('IP/@Brain'), 'ind');
index = index.ind;
index = convert2BinnedIndex(index, [F.IP.height F.IP.width], binsize);

% Start raw signal extraction
% -------------------------------------------------------------------------
% Correction preparation
% ----------------------
drift = load(F.fname('IP/@Drifts'), 'dx', 'dy');        % Load drift file
bkg = load(F.fname('IP/@Background'), 'mean_first');      % Load background
n_img_layer = numel(F.set.frames);          % Number of frames for this layer
Img1 = F.iload(1);                          % Load fist frame as a reference
Img1.rm_infos('rep', bkg.mean_first);       % Remove timestamp

% If eyes movement, prepare a skeleton of the picture for registration
if regist
    Gfx = imgaussfilt(Img1.pix, sigma);
    Gfy = imgaussfilt(Img1.pix, fliplr(sigma));
    Ref_1 = abs(Gfx - Gfy);
end

Img_bin = imresize(Img1.pix, 1/binsize);    % Bin the picture
signal_stack = zeros(size(index, 1), n_img_layer, 'uint16');  % Initialise signal stacks
Img_mean = Img_bin;                         % Initialise mean image
signal_stack(:, 1) = Img_bin(index);        % Fill signal stacks with first frame

% Process all images from the set
% -------------------------------
for t_image = 2:n_img_layer
    
    statusInfo(t_image - 1, n_img_layer, 25);
    
    Img2 = F.iload(t_image);                    % Load image
    Img2.rm_infos('rep', bkg.mean_first);       % Remove timestamp
    
    % Correct drift
    Img2.translate(-drift.dy(t_image), -drift.dx(t_image), 'fill', bkg.mean_first);
    
    % If eyes movement, perform a registration for further correction
    if regist
        % Image skeleton
        Gfx = imgaussfilt(Img2.pix, sigma);
        Gfy = imgaussfilt(Img2.pix, fliplr(sigma));
        Ref_2 = abs(Gfx - Gfy);
        
        % Image registration
        regist_param = imregdemons(Ref_2, Ref_1, [50 5 1], 'PyramidLevels', 3, ...
            'AccumulatedFieldSmoothing', 2, 'DisplayWaitbar', false);
        Img2 = imwarp(Img2.pix, regist_param);
    else
        Img2 = Img2.pix;
    end
    
    Img_bin = imresize(Img2, 1/binsize);                % Bin the image
    Img_mean = Img_mean + Img_bin;                      % Add the frame to the mean
    signal_stack(:, t_image) = Img_bin(index);          % Store raw fluo signal
end

% Output
% -------------------------------------------------------------------------
greystack = uint16(Img_mean/n_img_layer);   % Mean image
end