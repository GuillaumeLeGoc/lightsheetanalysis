function baseline = computeBaseline(signal, q, window, fast)
% This function computes the baseline of the input signal by computing the
% qth quantile of the signal in a moving window. Uses ordfilt2 to simulate
% the moving window.
%
% INPUTS :
% ------
% signal : signal matrix, time series on rows (size is n_pixel x n_times)
% q : quantile (typically 8 to get the 8th percentile).
% window : size of the moving time window in frames
% fast : choose to use a parallel loop or not.
%
% OUPUTS :
% ------
% baseline : array with the same size of signal, containing baseline value
% through time (columns) for each pixel (rows).
%
% Notes :
% -----
% - signal should be in uint16.
% - domain is defined with more than 3 rows for performance (see ordfilt2
% documentation).

domain = ones(window, 1);
stepsize = ceil(size(signal, 1)/8);

if stepsize > 1
    ranges = 0:stepsize:size(signal, 1);    % Split the signal in several arrays
else
    ranges = 0;                             % for single time serie
end

bl = cell(length(ranges), 1);

if fast
    
    parfor idx = 1:length(ranges)
        n = ranges(idx);
        range = (n + 1):min((n + stepsize), size(signal, 1));	% define pix range
        data = signal(range, :);
        bl_tmp = ordfilt2(data', q, domain, 'symmetric');
        bl{idx} = bl_tmp';
    end
    
    baseline = cat(1, bl{:});
    
else
    
    for idx = 1:length(ranges)
        n = ranges(idx);
        range = (n + 1):min((n + stepsize), size(signal, 1));	% define pix range
        data = signal(range, :);
        bl_tmp = ordfilt2(data', q, domain, 'symmetric');
        bl{idx} = bl_tmp';
    end
    
    baseline = cat(1, bl{:});
    
end