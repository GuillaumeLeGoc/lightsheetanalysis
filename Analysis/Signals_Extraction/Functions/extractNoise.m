function [sigma, threshold] = extractNoise(signal)
% Estimates noise level of signal assuming most of it is gaussian noise.
%
% INPUTS :
% ------
% signal : array, where time series are on rows (npixels x ntimes array).
%
% OUTPUTS :
% -------
% sigma : noise level
% threshold : threshold to consider noise vs signal.

% Check input
% -----------
if ~isa(signal, 'single')
    signal = single(signal);
end
signal = signal';

% Split in several arrays to avoid out of memory error
stepsize = ceil(size(signal, 2)/8);
if stepsize > 1
    ranges = 0:stepsize:size(signal, 2);    % Split the signal in several arrays
else
    ranges = 0;                             % for single time serie
end

th_low = cell(length(ranges), 1);
th_up = cell(length(ranges), 1);
peak_val = cell(length(ranges), 1);

for idx = 1:length(ranges)
    n = ranges(idx);
    range = (n + 1):min((n + stepsize), size(signal, 2));       % define range
    
    % Find lower threshold
    th_low{idx} = quantile(signal(:, range), 0.005);
    
    % Find upper threshold
    [counts, centers] = hist(signal(:, range), 100);
    [~, peak_ind] = max(counts);
    peak_val{idx} = centers(peak_ind)';
    th_up{idx} = 3*peak_val{idx} - 2*th_low{idx};
    
end

th_low = cat(2, th_low{:});
th_up = cat(2, th_up{:});
peak_val = cat(2, peak_val{:});

% Compute std of thresholded signal
sigma = zeros(size(signal, 2), 1);
threshold = zeros(size(signal, 2), 1);

for pix = 1:size(signal, 2)
    subsignal = signal(:, pix);
    signal_thresholded = subsignal(subsignal < th_up(pix) & ...
        subsignal > th_low(pix));
    sigma(pix)= std(signal_thresholded);
    threshold(pix) = 3*sigma(pix) + peak_val(pix);
end

sigma = single(sigma);
threshold = single(threshold);

end