% Worksheet to extract DF/F from signal_stack (raw fluo time per pixels).

% pause(6*60*60); % wait before launching code

clear;
clc;

% Parameters
study = 'Thermotaxis';
dat = '2019-11-26';
runs =  [1];
fast = 0;               % can cause out of memory
extract_noise = 'n';    % estimate noise or not
smooth_span = 100;
overwrite = 'n';        % recompute existing files or not

for run = runs
    
    F = getFocus([study filesep dat], run);
    
    % Define output folders
    make_output_name = @(n) [F.Data 'signal_stacks' filesep sprintf('%01i', n) filesep];
    
    n_layers = numel(F.sets);
    
    fprintf('%s : Computing baseline and DF/F for pixels\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        % Free RAM
        if exist('dff', 'var')
            clear dff
        end
        if exist('baseline', 'var')
            clear baseline
        end
        if exist('noise', 'var')
            clear noise
        end
        
        tic;
        fprintf('Layer %i...', layer);
        
        % Select current layer
        F.select(layer);
        
        % Prepare filenames
        save_dff = [make_output_name(layer) 'dff.mat'];
        save_bsl = [make_output_name(layer) 'baseline.mat'];
        
        switch overwrite
            case 'n'
                if exist(save_dff, 'file')
                    continue
                end
        end
        
        % Compute baseline and DF/F for pixels
        switch extract_noise
            case 'y'
                [dff, baseline, index, noise] = Extraction.dff_pixels(F, fast, smooth_span);
            case 'n'
                [dff, baseline, index] = Extraction.dff_pixels(F, fast, smooth_span);
        end
        
        % Save baseline and dff
        if exist('noise', 'var')
            save(save_dff, 'dff', 'noise', 'index');
        else
            save(save_dff, 'dff', 'index');
        end
        save(save_bsl, 'baseline');
        
        timer = timer + toc;
        fprintf(' Done (%2.2fs).\n', toc);
        
    end
    
    fprintf('All done in %2.2fs.\n', timer);
end