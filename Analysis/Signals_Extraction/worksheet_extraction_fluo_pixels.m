% Worksheet to use the function fluo_pixels that extracts raw fluo data from set
% of images, with drift correction.

clear
close all
clc

% Parameters
study = 'Thermotaxis';      % Focus will look for the run in study/dat
dat = '2019-11-26';
runs = [1];

% Options
override = 'y';     % Recompute exising file
binsize = 1;        % Bin signals
regist = 0;         % Register image for further correction (if eyes free)

for run = runs
    
    F = getFocus([study filesep dat], run);

    n_layers = numel(F.sets);
    
    fprintf('%s : Extracting raw fluo\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        % Prepare filenames
        sig_path = [F.Data 'signal_stacks' filesep sprintf('%i', layer) filesep];
        gre_path = [F.Data 'grey_stack' filesep];
        
        % Check if file already exists
        switch override
            case 'y'
                
            otherwise
                if exist([sig_path 'sig.mat'], 'var')
                    fprintf(' Done (%2.2fs).\n', toc);
                    continue
                end
        end
        
        % Free RAM
        if exist('signal_stack', 'var')
            clear signal_stack
        end
        
        tic
        fprintf('Layer #%i... ', layer);
        
        % Select current layer
        F.select(layer);
        
        % Extrack fluo from pixels
        [signal_stack, index, greystack] = Extraction.fluo_pixels(F, binsize, regist);
        
        if ~exist(sig_path, 'dir')
            mkdir(sig_path);
        end
        if ~exist(gre_path, 'dir')
            mkdir(gre_path);
        end
        
        % Save signal matrix and mean image
        save([sig_path 'sig.mat'], 'signal_stack', 'index');
        imwrite(greystack, [gre_path 'Image_' sprintf('%02i', layer) '.tif']);
        
        timer = timer + toc;
        fprintf(' Done (%2.2fs).\n', toc);
    end
    
    fprintf('All done in %2.2fs.\n', timer);
    
end