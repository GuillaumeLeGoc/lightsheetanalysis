% Worksheet to launched during Routines' signal extraction. Perform the
% analysis pixel wise, extracting drift-corrected fluo and DF/F per pixel.

close all

% --- Parameters
% study = 'Thermotaxis';
% dat = '2018-07-03';
% run = 2;

% Get focus
% F = getFocus([study filesep dat], run);

% pause(8*60*60);

% Specify what to perform after Routines
perform_fluo_extraction = 1;
perform_dff_extraction = 1;
perform_spikes_inference = 1;

% --- Options
gcamp = 'fast';         % choose correct speed of GCaMP
regist = 0;             % perform registration to correct drift
%sigma = 5*[1 1.5];     % Gaussian filter sigma for signal_stacks
binsize = 1;            % binning
para = 0;				% use parallel pool
smooth_span = 100;      % smooth baseline
extract_noise = 'n';

% Complete parameters;
n_layers = length(F.sets);

% GCaMP6f time constants (educated guess)
fast.tauRise = 0.15;
fast.tauDecay = 1.6;

% GCaMP6s time constants
slow.tauRise = 0.2;
slow.tauDecay = 3.55;

% ------------------------------------------------------------------------------

% Extract fluo and store in a matfile
if perform_fluo_extraction
    
    fprintf('%s : Extracting raw fluo\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        % Free RAM
        if exist('signal_stack', 'var')
            clear signal_stack
        end
        
        tic
        fprintf('Layer #%i... : ', layer);
        
        % Select current layer
        F.select(layer);
        
        % Extrack fluo from pixels
        [signal_stack, index, greystack] = Extraction.fluo_pixels(F, binsize, regist);
        
        % Prepare filenames
        sig_path = [F.Data 'signal_stacks' filesep sprintf('%i', layer) filesep];
        gre_path = [F.Data 'grey_stack' filesep];
        
        if ~exist(sig_path, 'dir')
            mkdir(sig_path);
        end
        if ~exist(gre_path, 'dir')
            mkdir(gre_path);
        end
        
        % Save signal matrix and mean image
        save([sig_path 'sig.mat'], 'signal_stack', 'index');
        imwrite(greystack, [gre_path 'Image_' sprintf('%02i', layer) '.tif']);
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
    end
    
    fprintf('All done in %2.2fs.\n', timer);
    
    
end

% ------------------------------------------------------------------------------

% DFF extraction
if perform_dff_extraction
    
    % Define output folders
    make_output_name = @(n) [F.Data 'signal_stacks' filesep sprintf('%01i', n) filesep];
    
    fprintf('%s : Computing baseline and DF/F for pixels\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        % Free RAM
        if exist('dff', 'var')
            clear dff
        end
        if exist('baseline', 'var')
            clear baseline
        end
        if exist('noise', 'var')
            clear noise
        end
        
        tic;
        fprintf('Layer %i... ', layer);
        
        % Select current layer
        F.select(layer);
        
        % Compute baseline and DF/F of pixels
        switch extract_noise
            case 'y'
                [dff, baseline, index, noise] = Extraction.dff_pixels(F, para, smooth_span);
            case 'n'
                [dff, baseline, index] = Extraction.dff_pixels(F, para, smooth_span);
        end
        
        % Prepare filenames
        save_dff = [make_output_name(layer) 'dff.mat'];
        save_bsl = [make_output_name(layer) 'baseline.mat'];
        
        % Save baseline and dff
        if exist('noise', 'var')
            save(save_dff, 'dff', 'noise', 'index');
        else
            save(save_dff, 'dff', 'index');
        end
        save(save_bsl, 'baseline');
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
    end
    
    fprintf('All done in %2.2fs.\n', timer);
end

% ------------------------------------------------------------------------------

% Spikes inference using BSD
if perform_spikes_inference
    
    n_layers = length(F.sets);
    dt = F.dt*1e-3;
    
    fprintf('%s : Infering spikes on neurons\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        tic
        fprintf('Layer %i... ', layer);
        
        F.select(layer);
        
        % Load signal
        signal = load(F.fname('@DFF'), 'neurons');
        signal = double(signal.neurons');                   % flip signal to match BSD expectations
        signal(abs(signal) > 100*std(signal(:))) = 1e-6;    % remove artifact
        
        % Inference algorithm parameters
        Oalg = struct;                      % Struct of experimental conditions & decoding options.
        Oalg.Time = size(signal, 1);        % Number of time frames.
        Oalg.dt = n_layers*dt;              % interval duration. (s)
        Oalg.nNeurons = size(signal, 2);    % Number of neurons.
        Oalg.adaptive = 0;                  % Not adaptive = Will use provided values for parameters, and estimate the unknown ones.
        
        Palg = struct;
        if ~Oalg.adaptive
            if strcmp(gcamp, 'slow')
                Palg.tauRise = slow.tauRise;             % Fluorescence raise time (s)
                Palg.tauDecay = slow.tauDecay;           % Fluorescence decay time (s)
            elseif strcmp(gcamp, 'fast')
                Palg.tauRise = fast.tauRise;             % Fluorescence raise time (s)
                Palg.tauDecay = fast.tauDecay;           % Fluorescence decay time (s)
            else
                error('GCaMP fast or slow not recognized.');
            end
        end
        
        if para
            % paralellized
            [spikes, ~, ~, Pphys] = pBSD(signal, Oalg, Palg);   % Blind sparse deconvolution from Tubiana
        else
            [spikes, ~, ~, Pphys] = BSD(signal, Oalg, Palg);    % Blind sparse deconvolution from Tubiana
        end
        
        spikes_bin = spikes > Pphys.threshold;
        
        % Get back to usual order (n_neurons x  n_times)
        spikes = spikes';
        spikes_bin = spikes_bin';
        
        % Save
        save(F.fname('@Spikes'), 'spikes', 'spikes_bin', 'Pphys');
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
    end
    fprintf('All done in %2.2fs.\n', timer);
end