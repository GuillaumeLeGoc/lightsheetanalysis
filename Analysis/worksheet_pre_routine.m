% Prepare for Routines: rename parameter file and reeq. number of zeros in
% images names.

base = '/home/ljp/Science/Projects/Neurofish/Data/';
study = 'Thermotaxis';
dat = '2019-11-26';
runs = 3;

% Choose what to do
% rename_param = 1;
reeq_numzero = 1;

% if rename_param
%     renameParameters(base, study, dat, runs);
% end

if reeq_numzero
    reformatImagesName(base, study, dat, runs);
end