% 2018-06-11 - Averages several nrrd file (with the CMTK command), typically
% to create mean maps from several fish registered on a ref brain.

close all;
clear;
clc;

root = '/home/ljp/Science/Projects/Neurofish/Data/';
cmtk_dir = '/usr/local/lib/cmtk/bin/';      % where are CMTK binaries

% Directories
% -----------
study = 'Thermotaxis';
reg_dir = 'Registration';   % relative to study
out_prefix = 'hot_average';
in_path = [root study filesep reg_dir filesep];
dates_list_path = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Hot_list.txt';

% Names of stacks to average
stacks_list = {'response'};
n_stacks = numel(stacks_list);

folders_list = dir(in_path);
folders_list(1:2) = [];     % remove '.' and '..'
folders_list = {folders_list(:).name}';
dates_list = readtable(dates_list_path);
n_exp = size(dates_list, 1);

% Build folders names
c = 0;
for id_dat = 1:n_exp
    
    dat = char(dates_list.Date(id_dat));
%         runs = eval(['[' dates_list.RunNumber{id_dat} ']']);
    runs = dates_list.RunNumber(id_dat);
    
    for id_run = 1:numel(runs)
        c = c + 1;
        run = runs(id_run);
        exp_list{c, 1} = [dat '_' sprintf('%02i', run)];
    end
end

exp_to_process = folders_list(ismember(folders_list, exp_list));
n_experiments = numel(exp_to_process);

fprintf('Averaging ');
for stack = 1:n_stacks
    
    
    stack_name = stacks_list{stack};
    make_filename = @(n) [stack_name '_' num2str(n) '_LJP_mean_refbrain_01_ras.nrrd'];
    
    fprintf('%s...', stack_name);
    tic
    
    % Average all stacks
    % -------------------------------------------------------------------------
    
    out_filename = [out_prefix '_' stack_name '_ljp_ref_brain.nrrd'];
    mean_path = [in_path out_filename];
    
    stack_list_reformated = [];
    
    for exp = 1:n_experiments
        reformated_stack_path_1 = [in_path exp_to_process{exp} filesep make_filename(1)];
        reformated_stack_path_2 = [in_path exp_to_process{exp} filesep make_filename(2)];
        stack_list_reformated = [stack_list_reformated ' ' reformated_stack_path_1 ' ' reformated_stack_path_2];
    end
    
    command = [cmtk_dir 'average_images -o ' mean_path stack_list_reformated];
    unix(command, '-echo');
    
    fprintf(' Done (%2.2fs).\n', toc);
end