% Create NRRD and perform registration for specified sets in specified folder.
% Made for interleaved stacks, where the outbound and the return has been split
% (see cmtk_interleaved_preparation).

close all;
clear;
clc;

root = '/home/ljp/Science/Projects/Neurofish/Data/';
cmtk_dir = '/usr/lib/cmtk/bin/';      % where are CMTK binaries
overwrite = 'y';                            % overwrite existing data or skip

% Directories
% -----------
study = 'Thermotaxis';
reg_dir = 'Registration';   % relative to study
% dat = '2017-11-29';
% runs = [1];
% fishline = 'nuc';
% Comment following line to process only specified run
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list.txt';
experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/RandomPulses_list.txt';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list_all.txt';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Spontaneous/Spontaneous_list.txt';

% Name of folders containing tif images to register.
% Following names should be under root/study/reg_dir/date_run/ :
% stacks_list = {'grey_stack', 'Stimulus_P', 'Stimulus_N', 'Positive_Derivative_P', 'Positive_Derivative_N', ...
%     'Negative_Derivative_P', 'Negative_Derivative_N'};
stacks_list = {'grey_stack'};
n_stacks = numel(stacks_list);

% Stack to use to compute transformation
registered_stack = 'grey_stack';

% CMTK parameters
% ---------------
% (see command for CMTK settings)
rotation = -90;
warp = 'n';
create_nrrd = 'y';    % create NRRD from tiff stack
register = 'y';       % register grey stack on the reference brain
apply = 'y';          % apply CMTK transformation

% Read dates
if exist('experiment_list', 'var')
    list = readtable(experiment_list, 'DateTimeType', 'text');
    n_exp = size(list, 1);
else
    n_exp = 1;
end

for idx_dates = 1:n_exp
    
    if exist('experiment_list', 'var')
        % Read date and runs list
        dat = char(list.Date(idx_dates));
        if strcmp(dat(1), '#')
            continue
        else
            runs = eval(['[' (list.RunNumber{idx_dates}) ']']);
            fishline = list.Line{idx_dates};
        end
    end
    
    % Reference brain
    if contains(fishline, 'cyt', 'IgnoreCase', true)
        ref_brain = 'LJP_Cytoplasmic';
    elseif contains(fishline, 'nuc', 'IgnoreCase', true)
        ref_brain = 'LJP_Nuclear';
    end
    
    ref_brain_path = [root 'Reference_Brains' filesep ref_brain '.nrrd'];
    
    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        
        cwd = [root study filesep reg_dir filesep dat '_' sprintf('%02i', run) filesep];
        
        floatings_dir = ['Floatings' filesep];
        
        if exist([cwd floatings_dir], 'dir')
            switch overwrite
                case 'n'
                    continue;
                case 'y'
                    warning('Overwriting data.');
                otherwise
                    continue;
            end
        end
        % -----------------------------------------------------------------
        % Create NRRD
        % -----------------------------------------------------------------
        switch create_nrrd
            case 'y'
                tic
                fprintf('% s - Run %i, building NRRD...', dat, run)
                % Get NRRD spec
                F = getFocus([study filesep dat], run);
                n_layers = length(F.sets);
                dx = F.dx;
                dy = F.dy;
                z = [F.sets.z];
                param.exp_type = '';
                param.binning = [1 1];
                param.rotate = rotation;                 % clockwise rotation : '-', anticlockwise : '+'
                
                for stack = 1:n_stacks
                    
                    stack_name = stacks_list{stack};
                    
                    % Make two distinct nrrd file
                    for c = 1:2
                        
                        if c == 1
                            dz = mean(diff(z(1:n_layers/2)));
                            param.space_type = 'RPS';
                        else
                            dz = mean(diff(z(n_layers/2 + 1:end)));
                            param.space_type = 'RPA';
                        end
                        
                        param.pix_size = [dx dy dz];
                        
                        in_folder = [cwd stack_name '_' num2str(c) filesep];
                        out_folder = [cwd floatings_dir];
                        out_name = [stack_name '_' num2str(c)];
                        makeNrrdStack(in_folder, out_folder, out_name, param);
                    end
                    
                end
                
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % -----------------------------------------------------------------
        % Compute CMTK transformations
        % -----------------------------------------------------------------
        switch register
            case 'y'
                
                fprintf('Registering stack on reference brain...');
                tic
                
                % Directory where to save transformations
                transfo_path = [cwd 'Transformations' filesep];
                if ~exist(transfo_path, 'dir')
                    mkdir(transfo_path);
                end
                
                for c = 1:2
                    
                    % Build path names
                    affine_path = [transfo_path 'affine_'  registered_stack '_' num2str(c) '_' ref_brain filesep];
                    warp_path = [transfo_path 'warp_'  registered_stack '_' num2str(c) '_' ref_brain filesep];
                    % Create directories if necessary
                    if ~exist(affine_path, 'dir')
                        mkdir(affine_path);
                    end
                    switch warp
                        case 'y'
                            if ~exist(warp_path, 'dir')
                                mkdir(warp_path);
                            end
                    end
                    
                    % Define CMTK inputs
                    float_name = [cwd floatings_dir registered_stack '_' num2str(c) '.nrrd'];

                    % === Rigid registration ===
                    % Define CMTK command
                    stacks = ['-o ' '"' affine_path '"'  ' ' '"' ref_brain_path '"' ' ' '"' float_name '"' ];
                    
                    % Guillaume
                    options = 'registrationx --omit-original-data --max-stepsize 25.6 --min-stepsize 3.2 --coarsest 25.6 --sampling 3.2 --dofs 6,9,12 --symmetric ';
                    % Bertoni
%                     options = 'registration -i --coarsest 25.6 --sampling 3.2 --omit-original-data --exploration 25.6 --dofs 6 --dofs 9 --accuracy 3.2 ';
                    % CMTK user guide
%                     options = 'registration --initxlate --dofs 6,9 --auto-multi-levels 4 ';
                    % Hugo
%                     options = 'registration --initxlate --dofs 6,9,12 --sampling 3 --coarsest 25 --omit-original-data --accuracy 3 --exploration 25.6 ';
                    
                    command = [cmtk_dir options stacks];
                    
                    % Launch CMTK
                    unix(command, '-echo');
                    
                    % === Non-rigid registration ===
                    switch warp
                        case 'y'
                            % Define CMTK command
                            stacks = ['-o ' '"' warp_path '"' ' ' '"' affine_path '"'];
                            % Options
                            options = 'warpx --fast --omit-original-data --grid-spacing 40 --grid-refine 2 --max-stepsize 25.6 --min-stepsize 3.2 --coarsest 25.6 --sampling 3.2 --smoothness-constraint-weight 0.1 --jacobian-constraint-weight 0.001 --adaptive-fix-thresh 0.25 ';

                            command = [cmtk_dir options stacks];
                            
                            % Launch CMTK
                            unix(command, '-echo');
                    end
                end
                
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % -----------------------------------------------------------------
        % Apply transformations
        % -----------------------------------------------------------------
        switch apply
            case 'y'
                
                fprintf('Applying transformations...');
                tic
                % Following folders should already exists and contain
                % transformations
                transfo_path = [cwd 'Transformations' filesep];
                
                for stack = 1:n_stacks
                    
                    stack_name = stacks_list(stack);
                    
                    for c = 1:2
                        
                        affine_save = [transfo_path 'affine_' registered_stack '_' num2str(c) '_' ref_brain];
                        warp_save = [transfo_path 'warp_' registered_stack '_' num2str(c) '_' ref_brain];
                        
                        % Define CMTK inputs
                        float_name = [cwd floatings_dir stack_name '_' num2str(c) '.nrrd'];
                        
                        % Define CMTK outputs
                        reformated_stack_path = [cwd stack_name '_' num2str(c) '_' ref_brain '.nrrd'];
                        
                        % Define CMTK command
                        switch warp
                            case 'y'
                                command = ['"' cmtk_dir 'reformatx" --pad-out 0 -o ' '"' reformated_stack_path '"' ' --floating ' '"' float_name '"' ' ' '"' ref_brain_path '"' ' ' '"' warp_save '"'];
                            otherwise
                                command = ['"' cmtk_dir 'reformatx" --pad-out 0 -o ' '"' reformated_stack_path '"' ' --floating ' '"' float_name '"' ' ' '"' ref_brain_path '"' ' ' '"' affine_save '"'];
                        end
                        
                        % Launch CMTK
                        command = char(join(command, ''));
                        unix(command, '-echo');
                    end
                end
                fprintf(' Done (%2.2fs).\n', toc);
        end
    end
end