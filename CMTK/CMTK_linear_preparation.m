% Preparation of stacks to be registered with CMTK : gather grey stacks and
% put them in the registration folder.

clear
close all
clc

% Parameters
% -------------------------------------------------------------------------
base_dir = '/home/ljp/Science/Projects/Neurofish/Data/';

% Experiment
% ----------
study = 'Thermotaxis';

% Manual
% dat = '2019-10-30';
% runs = 2;

% Automatic from list
% Comment following line to process only specified run.
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list.txt';
experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/RandomPulses_list.txt';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Spontaneous/Spontaneous_list.txt';

% Stacks to process, relative to experiment path
resp_path = '';             % subfolder relative to experiment path
list_name = {'grey_stack'}; % name of stack within above folder
% list_name = {'Stimulus_P', 'Stimulus_N', 'Positive_Derivative_P', 'Positive_Derivative_N', ...
%     'Negative_Derivative_P', 'Negative_Derivative_N'};

% Options
% -------
overwrite = 'y';
mirror = 'y';

% Define output, relative to study directory
registration_name = 'Registration';

if exist('experiment_list', 'var')
    list = readtable(experiment_list, 'DateTimeType', 'text');
    n_exp = size(list, 1);
else
    n_exp = 1;
end

tic;
c = 0;
for idx_dates = 1:n_exp
    
    if exist('experiment_list', 'var')
        % Read date and runs list
        dat = char(list.Date(idx_dates));
        runs = eval(['[' (list.RunNumber{idx_dates}) ']']);
        
        if strcmp(dat(1), '#')
            continue
        end
        
    end
    
    for idx_run = 1:numel(runs)
        
        c = c + 1;
        
        run = runs(idx_run);
        
        % Make full paths
        % ---------------
        str_run = sprintf('%02i', run);
        path_to_run = [base_dir study filesep dat filesep 'Run ' str_run filesep];
        path_to_regi = [base_dir study filesep registration_name filesep dat '_' str_run filesep];
        
        if exist(path_to_regi, 'dir')
            switch overwrite
                case 'y'
                    warning('Overwriting data.')
                case 'n'
                    continue;
                otherwise
                    continue;
            end
        end
        
        for idx_stack = 1:numel(list_name)
            
            stack_name = list_name{idx_stack};
            
            path_to_stack = [path_to_run resp_path filesep stack_name filesep];
            
            path_to_processed = [path_to_regi stack_name filesep];
            
            if ~exist(path_to_regi, 'dir')
                mkdir(path_to_regi);
            end
            if ~exist(path_to_processed, 'dir')
                mkdir(path_to_processed);
            end
            
            % Responses stack
            % ---------------------------------------------------------------------
            list_img = dir([path_to_stack '*.tif']);
            n_img = length(list_img);
            input_name = @(n) [path_to_stack list_img(n).name];
            
            for idx = 1:n_img
                
                img = imread(input_name(idx));
                
                switch mirror
                    case 'y'
                        img = flip(img, 1);
                end
                
                imwrite(img, [path_to_processed 'Image_' sprintf('%02i', idx) '.tif']);
            end
        end
    end
end

fprintf('\nDone in %2.2fs\n', toc);