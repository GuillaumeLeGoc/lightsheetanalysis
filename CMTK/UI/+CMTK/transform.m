function [] = transform(option)

% Fill default values
% -------------------
cmtk_dir = 'cmtk ';      % where is CMTK binaries
if ~exist('option', 'var')
    option = '';
end

% Get folders
% -----------
% Brain to transform
[flo_brain_name, flo_brain_path] = uigetfile([pwd filesep '*.nrrd'], 'Choose stack to transform');
flo_full_path = [flo_brain_path flo_brain_name];

% Choose CMTK transformations to apply
transfo_dir = uigetdir(flo_brain_path,  'Choose CMTK transformations to apply');

% Corresponding reference brain
[ref_brain_name, ref_brain_path] = uigetfile([pwd filesep '*.nrrd'], 'What was the reference brain');
ref_brain = [ref_brain_path ref_brain_name];

% Where to save it
[aligned_name, aligned_path] = uiputfile([flo_brain_path '*.nrrd'], 'Choose where to save transformed stack');
aligned_full_path = [aligned_path aligned_name];

% Create the reformatted stack
% ----------------------------
if isempty(option)
    %     command = ['"' cmtk_dir 'reformatx" -o ' '"' aligned_full_path '"' ' --floating ' '"' flo_full_path '"' ' ' '"' ref_brain '"' ' ' '"' transfo_dir '"'];
    options = 'reformatx -o ';
    stacks = ['"' aligned_full_path '"' ' --floating ' '"' flo_full_path '"' ' ' '"' ref_brain '"' ' ' '"' transfo_dir '"'];
    command = [cmtk_dir options stacks];
elseif strcmp(option, 'inverse')
    %     command = ['"' cmtk_dir 'reformatx" -o ' '"' aligned_full_path '"' ' --floating ' '"' flo_full_path '"' ' ' '"' ref_brain '"' ' ' '--inverse "' transfo_dir '"'];
    options = 'reformatx -o ';
    stacks = ['"' aligned_full_path '"' ' --floating ' '"' flo_full_path '"' ' ' '"' ref_brain '"' ' ' '--inverse' '"' transfo_dir '"'];
    command = [cmtk_dir options stacks];
end
unix(command, '-echo');

%'reformatx --pad-out 0 \-o new_line-f01-02_warp.nrrd --floating new_line-f01-01.nrrd refbrain.nrrd new_line-f01_nonrigid.xform'