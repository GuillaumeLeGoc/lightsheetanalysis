dx = 0.4;
dy = 0.4;
dz = 0.8;
param.exp_type = '';
param.binning = [1 1];
param.pix_size = [dx dy dz];
param.origin = [0 0 0];
param.rotate = 0;                   % clockwise rotation : '-', anticlockwise : '+'
param.space_type = 'RPS';           % cmtk space is right-anterior-superior
param.mirror = 0;

CMTK.createNrrd(param);
CMTK.register;
CMTK.transform;