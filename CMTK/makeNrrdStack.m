
function [] = makeNrrdStack(inFolder, outFolder, outName, param)
% This function convert a series of tiff image in a nrrd file suitable for
% CMTK.
% Update Guillaume: now rotates the images with an angle param.rotate
% Update Guillaume: now mirror the image with flip.

%% Read parameters

binning = param.binning;
pixel_size(1) = param.pix_size(1);  % pixel dimension in micron during acquisition
pixel_size(2) = param.pix_size(2);  % pixel dimension in micron during acquisition
pixel_size(3) = param.pix_size(3);  % distance between each plane in micron
space_type = param.space_type;

if isfield(param, 'mirror')
    mirror = param.mirror;
else
    mirror = 0;
end
if ~isfield(param, 'rotate')
    param.rotate = 0;
end
if ~isfield(param, 'origin')
    param.origin = [0 0 0];
end

%% Read images
dirInFolder = dir(fullfile([ inFolder '/*.tif']));
if isempty(dirInFolder)
    error('No tiff files found in specified directory.');
end
ImageNames = {dirInFolder.name}';
numFrames = numel(ImageNames);

I = imread([inFolder,filesep,ImageNames{1}]);
if param.rotate
    I = imrotate(I, param.rotate);
end

%% Preallocate the array
sequence = zeros([fix(size(I)/binning(1)) fix(numFrames/binning(2))]);

%% Create image sequence array
if binning(2) == 1
    for p = 1:numFrames
        I = double(imread([inFolder filesep ImageNames{p}]));
        J = imrotate(I, param.rotate);
        K = imresize(J, 1/binning(1));
        sequence(:, :, p) = K;
    end
else
    substack = zeros([size(I)/binning(1) binning(2)]);
    for p = 1:binning(2):binning(2)*fix(numFrames/binning(2))
        for n = 1:binning(2)
            I = double(imread([inFolder filesep ImageNames{p+n-1}]));
            
            if mirror == 1
                I = flip(I, 1);
            elseif mirror == 2
                I = flip(I, 2);
            end
            
            J = imrotate(I, param.rotate);
            K = imresize(J, 1/binning(1));
            substack(:,:,n) = K;
        end
        sequence(:, :, fix(1+p/binning(2))) = mean(substack, 3);
    end
end

%% Scale signal stack
seqsort = sort(sequence(:));

Nelements = size(seqsort(:),1);
min_intensity = seqsort(fix(1+0.25*Nelements));
max_intensity = seqsort(fix(1*Nelements));
sequence2 = uint16(65535*(sequence-min_intensity)/(max_intensity-min_intensity));

%% save
pixelspacing = [pixel_size(1) pixel_size(2) pixel_size(3)].*[binning(1) binning(1) binning(2)];
origin = param.origin;
encoding = 'raw';

if ~exist(outFolder, 'dir')
    mkdir(outFolder);
end

nrrdWriter([outFolder filesep outName '.nrrd'], sequence2, pixelspacing, origin, encoding, space_type);
end