% Quickstart with CMTK registration with warp or not.
% Caution : it overwrites if something already exists.

close all
clear
clc

% --- Input
reference_stack = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/LJP_Nuclear.nrrd';
floating_stack = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/ZBrainAtlas_Nuclear.nrrd';

% --- Output
xform_prefix = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Registration/2018-12-06_01/Transfo_LJP_Nuc_on_ZBrain_';
reformatted_stack = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/Reformatted_ZBrainAtlas_Nuc_on_LJP.nrrd';

% --- Options
warp = 'y';

xform_affine = [xform_prefix 'affine.xform'];
xform_warp = [xform_prefix 'warp.xform'];

% --- Affine options
% Bertoni
%affine_options = 'registration -i --coarsest 25.6 --sampling 3.2 --omit-original-data --exploration 25.6 --dofs 6 --dofs 9 --accuracy 3.2 ';
% Burgess lab
%affine_options = 'registrationx --dofs 12 --min-stepsize 1 ';
% CMTK user guide (automatic)
%affine_options = 'registration --initxlate --dofs 6,9 --auto-multi-levels 4 ';
% Slow Guillaume
affine_options = 'registrationx --verbose-level 2 --omit-original-data --max-stepsize 25.6 --min-stepsize 3.2 --coarsest 25.6 --sampling 3.2 --dofs 6,9,12 --symmetric ';
affine_folders = ['--output ' '"' xform_affine '"'  ' ' '"' reference_stack '"' ' ' '"' floating_stack '"'];
affine_command = ['cmtk ' affine_options affine_folders];

% --- Warp options
switch warp
    case 'y'
        % Options from Burgess lab
        %warp_options = 'warpx --fast --grid-spacing 100 --smoothness-constraint-weight 1e-1 --grid-refine 2 --min-stepsize 0.25 --adaptive-fix-thresh 0.25 ';
        % Options from Volker
        %warp_options = 'warp -v --fast --grid-spacing 40 --refine 2 --jacobian-weight 0.001 --coarsest 6.4 --sampling 3.2 --accuracy 3.2 --omit-original-data ';
        % Slow Guillaume
        warp_options = 'warpx --verbose-level 2 --fast --omit-original-data --grid-spacing 40 --grid-refine 2 --adaptive-fix-thresh 0.25 --jacobian-constraint-weight 0.001 --max-stepsize 25.6 --min-stepsize 3.2 --coarsest 25.6 --sampling 3.2 ';
        warp_folders = ['--outlist ' '"' xform_warp '"' ' ' '"' reference_stack '"' ' ' '"' floating_stack '"' ' ' '"' xform_affine '"'];
        warp_command = ['cmtk ' warp_options warp_folders];
end

% --- Reformat options
refmt_options = 'reformatx --verbose-level 2 --ushort ';
switch warp
    case 'y'
        refmt_folders = ['--outfile ' '"' reformatted_stack '"' ' --floating ' '"' floating_stack '"' ' ' '"' reference_stack '"' ' ' '"' xform_warp '"'];
    case 'n'
        refmt_folders = ['--outfile ' '"' reformatted_stack '"' ' --floating ' '"' floating_stack '"' ' ' '"' reference_stack '"' ' ' '"' xform_affine '"'];
end
refmt_command = ['cmtk ' refmt_options refmt_folders];

% --- Processing
fprintf('Affine registration...\n'); tic
unix(affine_command);
fprintf('\t Done (%2.2fs).\n', toc);

switch warp
    case 'y'
        fprintf('Non-rigid registration...\n'); tic
        unix(warp_command);
        fprintf('\t Done (%2.2fs).\n', toc);
end

fprintf('Creating reformatted stack...\n'); tic
unix(refmt_command);
fprintf('\t Done (%2.2fs).\n', toc);