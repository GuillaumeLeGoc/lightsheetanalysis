function out = gload(this, tag, varargin)
%FOCUS.GLOAD Load figure
%   FOCUS.GLOAD(N) Load figure number N
%
%*  See also: Focus.

% === Input variables =====================================================

in = inputParser;
in.addRequired('tag', @ischar);

in.parse(tag, varargin{:});
in = in.Results;

% =========================================================================

% --- Get file name
fname = [this.gname(in.tag) '.fig'];

% --- Load Graph (figure)
if exist(fname, 'file')
    h = openfig(fname, 'reuse');
else
    h = NaN;
end

if nargout
    out = h;
end
