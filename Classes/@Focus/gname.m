function out = gname(this, tag, varargin)
%FOCUS.GNAME returns the filename corresponding to a graph.
%*  OUT = FOCUS.GNAME() returns the filename corresponding to a graph.
%
%*  OUT = FOCUS.GNAME(TAG) where TAG is a string, returns the corresponding
%   filename. If TAG starts with an '@', the path is adaptated to the 
%   current Stack. If TAG starts with two '@@', the path is automatically 
%   adaptated to the current Stack and layer.
%
%[  Note: A TAG composed of tokens separated by the filesystem separator
%   will save the file in subfolders.   ]
%
%*  See also: Focus, Focus.fname, Focus.iname, Focus.gsave, Focus.gload.

% === Input variables =====================================================

in = inputParser;
in.addRequired('tag', @ischar);

in.parse(tag, varargin{:});
in = in.Results;

% =========================================================================

% --- Run shortcut
if ~isnan(this.layer)
    in.tag = strrep(in.tag, '@', [num2str(this.layer) filesep]);
end

out = [this.Figures in.tag];


