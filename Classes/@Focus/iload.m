function Img = iload(this, n, varargin)
%[Focus].iload Image load
%   IMG = [Focus].ILOAD(N) Returns image number N.
%
%   [Focus].ILOAD(..., MODE) specifies the MODE, which can be 'abs'
%   (absolute) or 'rel' (relative, default). In absolute mode, N is assumed
%   to be the image number while in relative mode N is the frame number in 
%   the selected set.
%
%   See also: Focus.

% === Input variables =====================================================

in = inputParser;
in.addRequired('n', @isnumeric);
in.addOptional('mode', 'rel', @ischar);

in.parse(n, varargin{:});
in = in.Results;

% =========================================================================

name = this.iname(in.n, in.mode);

if isa(name, 'string') || isa(name, 'char')
    Img = Image(name);
elseif isa(name, 'struct')
    img = imread(name.name, 'Index', name.n);
    Img = Image(double(img));
end

Img.camera = this.IP.camera;
Img.range = this.IP.range;

switch this.IP.camera
    case {'Andor_iXon', 'PCO.edge'}
        Img.tstamp = this.set.t(in.n);
end