function M = load(this, tag, varargin)
%FOCUS.LOAD load a file in the Files directory.
%*  M = FOCUS.LOAD(TAG) loads M from a file TAG in the Focus.Files
%   directory.
%
%[  Note: A tag composed of tokens separated by the filesystem separator
%   will load the file from subfolders.   ]
%
%*  See also: Focus, Focus.save.

% === Input variables =====================================================

in = inputParser;
in.addRequired('tag', @ischar);
in.addOptional('type', 'mat', @ischar);

in.parse(tag, varargin{:});
in = in.Results;

% =========================================================================

% --- Get the file name
fname = this.fname(in.tag, in.type);

% --- Load the file

switch in.type

    case 'mat'
        try
            tmp = load(fname);
            f = fieldnames(tmp);
            if numel(f)==1
                try
                    M = tmp.(f{1});
                catch
                    warning(['Error while analyzing ' fname '.']);
                    M = NaN;
                end
            else
                warning(['Wrong number of fields (' num2str(numel(f)) ') in file ' fname '.']);
                M = NaN;
            end
            
        catch
            
            warning(['Could not load the file ' fname '.']);
            M = NaN;
            
        end
        
    case 'png'
        M = Image(fname);
        
end