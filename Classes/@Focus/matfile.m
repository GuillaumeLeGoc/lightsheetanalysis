function out = matfile(this, tag, varargin)
%[Focus].matfile returns the matfile corresponding to a tag.
%   OUT = [Focus].MATFILE(TAG) returns the ML.matfile object corresponding 
%   to a tag. TAG should be a string, optionnaly comprising '/' to define 
%   subfolders. If TAG starts with an '@', the path is automatically 
%   adaptated to the current set.
%
%*  See also: Focus, Focus.fname.

% === Input variables =====================================================

in = inputParser;
in.addRequired('tag', @ischar);
in.addOptional('ext', 'mat', @ischar);
in.parse(tag, varargin{:});
in = in.Results;

% =========================================================================

% --- Output
out = ML.matfile(this.fname(in.tag, in.ext));