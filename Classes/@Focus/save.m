function save(this, M, tag)
%FOCUS.SAVE save file in the Focus directory.
%*  FOCUS.SAVE(M, TAG) saves M in a file TAG in the Focus.files directory.
%
%[  Note: A tag composed of tokens separated by the filesystem separator
%   will save the file in subfolders.   ]
%
%*  See also: Focus, Focus.load.

% === Input variables =====================================================

in = inputParser;
in.addRequired('M', @(x) true);
in.addRequired('tag', @ischar);
in.addParamValue('ext', 'mat', @ischar);
in.parse(M, tag);
in = in.Results;

% =========================================================================

% --- Get the file name
fname = this.fname(in.tag, in.ext);

% --- Create the directory (if needed)
dir = fileparts(fname);
if ~exist(dir,'dir')
     fprintf('→ Creating folder : %s\n',dir);
     mkdir(dir);
end

% --- Save the file
save(fname, 'M');
