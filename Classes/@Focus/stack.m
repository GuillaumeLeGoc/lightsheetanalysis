function S = stack(this, varargin)
%Focus.stack Stack corresponding to the current set
%*  S = FOCUS.STACK() returns the stack corresponding to the current set.
%   If the stack does not exists, it is created.
%
%*  S = FOCUS.STACK(SET) returns the stack corresponding to set SET.
%
%*  See also: Focus.

% === Input variables =====================================================

in = inputParser;
in.addOptional('set', NaN, @isnumeric);

in.parse(varargin{:});
in = in.Results;

% === Parameters ==========================================================

tag = '@Stack';

% =========================================================================

% --- Default values
if isnan(in.set)
    if isa(this.set, 'struct')
        in.set = this.set.id;
    else
        warning('FOCUS:STACK', 'No set selected, aborting.')
        S = NaN;
        return
    end
end

% --- Stack creation
if ~exist(this.fname(tag), 'file')
     
    I = this.sets(in.set).idx;
    S = zeros(this.IP.height, this.IP.width, numel(I), this.IP.class);
    
    for i = 1:numel(I);
        S(:,:,i) = imread(this.iname(i));
        if ~mod(i,10) || i==numel(I)
            Time.status(['Creating ' this.name ' stack']); 
        end
    end
    
    % Save stack
    Time.start('Saving stack');
    this.save(S, tag);
    Time.stop
    
else
    Time.start('Loading stack');
    S = this.load(tag);
    Time.stop
end