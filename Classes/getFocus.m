function F = getFocus(varargin)
%getFocus Get a Focus interactively.
%   F = GETFOCUS(DATE, RUN) Returns the Focus corresponding to RUN at DATE.
%
%   F = GETFOCUS(DATE) Asks for RUN and return the corresponding Focus.
%
%   F = GETFOCUS() Asks for both DATE and RUN.
%
%   See also Focus.

% === Input variables =====================================================

in = inputParser;
in.addOptional('date', '', @ischar);
in.addOptional('run', [], @isnumeric);

in.parse(varargin{:});
in = in.Results;

% =========================================================================

% --- Select the project
Proj = ML.Projects.select;
if isempty(Proj.name)
    Proj = ML.projects('force');
end

% --- Select date
if isempty(in.date)
    fprintf('Please enter the run date:\n');
    in.date = input('?> ', 's');
end

% --- Select run
if isempty(in.run)
    runs = dir([Proj.path 'Data' filesep in.date filesep 'Run *']);
    if numel(runs)>1
        fprintf('\nPlease select a run:\n');
        for i = 1:numel(runs)
            tmp = regexp(runs(i).name, 'Run (\d+)', 'tokens');
            fprintf('\t%i - %s\n', str2double(tmp{1}{1}), runs(i).name);
        end
        tmp = input('?> ', 's');
        in.run = str2double(tmp);
    else
        runs
        tmp = regexp(runs(1).name, 'Run (\d+)', 'tokens');
        in.run = str2double(tmp{1}{1});
    end
end

% --- Return the Focus
F = Focus(in.date, in.run);