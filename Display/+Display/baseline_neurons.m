% Display fluo with baseline. A Focus object and layer should be existing in the
% workspace (see display_baseline_neurons).

clear p1 p2

% Prepare data
% ------------
% Select layer
F.select(layer);

% Get stimulation onsets
[stim, durations] = getStimTimes(F.Data);

% Time
t = F.sets.t;
t = t.*1e-3;

% Prepare file
Neurons = matfile(F.fname('IP/@Neurons'));

% Mean image
mean_image = imread(F.fname('IP/@Mean', 'png'));

% Neurons' index
neurons_index = Neurons.ind;
n_neurons = size(neurons_index, 2);            % number of neurons

% Neurons' fluo
fluo = Neurons.fluo;

% Neurons' baseline
baseline = Neurons.baseline;

% Make a canvas containing neurons' id
canvas = zeros(size(mean_image));
for idx = 1:n_neurons
    neuron_pix = neurons_index{idx};
    canvas(neuron_pix) = idx;
end

% Display mean image
figure;
subplot(2,1,1);
imshow(rescale(mean_image))

% Begin UI crosshair
% ------------------
while true
    
    subplot(2,1,1);
    h = drawpoint;
    pos = h.Position;
    i = ceil(pos(2)); j = ceil(pos(1));
    ind = sub2ind(size(mean_image), i, j);      % convert to linear index
    neuron_id = canvas(ind);
    
    fprintf('Neuron #%i selected\n', neuron_id);
    
    if neuron_id > 0
        
        neuron_pix = neurons_index{neuron_id};
        mean_image(neuron_pix) = 0;             % put the neuron in black
        subplot(2,1,1)
        imshow(rescale(mean_image));
        
        % Plot fluo and baseline below mean image
        if ~exist('p1', 'var')
            subplot(2,1,2)
            p1 = plot(t, baseline(neuron_id, :));
            hold on
            p2 = plot(t, fluo(neuron_id, :));
            ylabel('Fluo');
            xlabel('Time [s]');
            legend('Baseline', 'Fluo', 'AutoUpdate', 'off');
            title(['Neuron #' num2str(neuron_id)]);
        else
            p1.YData = baseline(neuron_id, :);
            p2.YData = fluo(neuron_id, :);
            axis auto
        end
        
        % Stimulation representation
%         supe = max(fluo(neuron_id, :));
%         infe = min(fluo(neuron_id, :));
%         subplot(212);
%         if ~exist('p', 'var')
%             p = cell(numel(stim), 1);
%         end
%         for n = 1:length(stim)
%             if ~isempty(p{n})
%                 p{n}.delete;
%             end
%             p{n} = patch([stim(n) stim(n) stim(n)+durations(n) stim(n)+durations(n)], ...
%                 [infe supe supe infe], [0 0 1]);
%             p{n}.FaceAlpha = 0.2;
%             p{n}.EdgeColor = 'none';
%         end
    elseif neuron_id == 0
        title('No neuron here...');
    end
end