% Display baseline computed for pixels.

clear p1

% Select layer
F.select(layer);

% Get stimulation onsets
[stim, durations] = getStimTimes(F.Data);

% Prepare filenames
fluo_file = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'sig.mat']);
base_file = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'baseline.mat']);
avg_fname = [F.Data 'grey_stack' filesep 'Image_' sprintf('%02i', layer) '.tif'];

% Load pixel indices
index = fluo_file.index;

% Load mean image
mean_image = imread(avg_fname);

% Time
t = F.sets.t;
t = t.*1e-3;

% Display mean image
figure;
subplot(2,1,1);
imshow(rescale(mean_image));

% Begin UI crosshair
while true
    
    [x, y] = ginput(1);                  % select pixel
    x = ceil(x); y = ceil(y);
    
    idx_px = sub2ind(size(mean_image), y, x);
    pix = find(index == idx_px);
    
    fprintf('Pixel #%i selected\n', pix);
    
    if pix > 0
        
        mean_image(y, x) = 0;           % put the pixel in black
        subplot(2,1,1);
        imshow(rescale(mean_image));
        
        selected_fluo = fluo_file.signal_stack(pix, :);
        selected_base = base_file.baseline(pix, :);
        
        % Plot DF/F trace below mean image
        if ~exist('p1', 'var')
            subplot(2,1,2)
            hold on
            p1 = plot(t, selected_fluo);
            p2 = plot(t, selected_base);
            ylabel('Fluo');
            xlabel('Time [s]');
            title(['Pixel #' num2str(pix)]);
        else
            p1.YData = selected_fluo;
            p2.YData = selected_base;
            axis auto
        end
        
        % Stimulation representation
        supe = max(selected_fluo);
        infe = min(selected_fluo);
        subplot(2,1,2);
        if ~exist('p', 'var')
            p = cell(numel(stim), 1);
        end
        for n = 1:length(stim)
            if ~isempty(p{n})
                p{n}.delete;
            end
            p{n} = patch([stim(n) stim(n) stim(n)+durations(n) stim(n)+durations(n)], ...
                [infe supe supe infe], [0 0 1]);
            p{n}.FaceAlpha = 0.2;
            p{n}.EdgeColor = 'none';
        end
    elseif isempty(pix)
        title('No pixel here...');
    end
end