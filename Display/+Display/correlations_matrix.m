% Display correlations of the selected neuron. A Focus object and a correlation
% matrix should be existing in the workspace (see 'display_correlations_matrix'
% script).

% Additional parameters
correl_neg = 0;   % show (anti)correlation > thresh
correl_pos = 0;

% Neurons index
neurons_index = Neurons.ind;
n_neurons = length(neurons_index);            % number of neurons

% Mean image
mean_image = imread(F.fname('IP/@Mean', 'png'));
mean_image = rescale(mean_image, 0, .5);

% Make a canvas containing neurons' id
canvas = zeros(size(mean_image));
for idx = 1:n_neurons
    neuron_pix = neurons_index{idx};
    canvas(neuron_pix) = idx;
end

fig = figure;
imshow(mean_image, [0, 1])

% Begin UI crosshair
while true
    
    h = drawpoint;                     % select neuron
    pos = h.Position;
    i = ceil(pos(2)); j = ceil(pos(1));
    ind = sub2ind(size(mean_image), i, j);	% convert to linear index
    neuron_id = canvas(ind);
    
    fprintf('Neuron #%i selected\n', neuron_id);
    
    if neuron_id > 0
        
        neuron_pix = neurons_index{neuron_id};
        
        % Prepare canvas for correlations
        corr_img = zeros(size(mean_image));
        
        % Fill neurons with correlations
        for h = 1:size(correlation_matrix, 1)
            
            ind2 = neurons_index{h};
            
            if correlation_matrix(neuron_id, h) < correl_neg
                corr_img(ind2) = correlation_matrix(neuron_id, h);
            elseif correlation_matrix(neuron_id, h) > correl_pos
                corr_img(ind2) = correlation_matrix(neuron_id, h);
            end
        end
        
        % Make a RGB image with mean image
        rgb = zeros([size(mean_image) 3]);
        poscorr = corr_img;
        poscorr(corr_img < 0) = 0;
        negcorr = corr_img;
        negcorr(corr_img > 0) = 0; negcorr = abs(negcorr);
        rgb(:, :, 1) = mean_image + poscorr;
        rgb(:, :, 2) = mean_image;
        rgb(:, :, 3) = mean_image + negcorr;
        
        % Show what max/min correlation look like
%         rgb(1:10, 1:30, :) = cat(3, 2*ones(10, 30), zeros(10, 30), zeros(10, 30));
%         rgb(11:20, 1:30, :) = cat(3, zeros(10, 30), zeros(10, 30), 2*ones(10, 30));
        
        imshow(rgb);
    end
end