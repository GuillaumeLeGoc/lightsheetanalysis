% Display DF/F with real stim trace and stim average. A Focus object and layer,
% and information regarding calibration should be existing in the workspace (see
% 'display_baseline_neurons' script).

clear p1 p2 p3 p4

% Prepare data
% ------------
% Select layer
F.select(layer);
n_layers = numel(F.sets);

% Get stimulation onsets
[stim, durations] = getStimTimes(F.Data);

% Get stimulation trace
calib_filename = [F.Data(1:end-7) 'Calibration' filesep calib_name '_Tset=' num2str(Tset) '.mat'];
stim_calib = load(calib_filename);

% Time
t = F.sets.t;
t = t.*1e-3;

% Prepare files
Neurons = matfile(F.fname('IP/@Neurons'));
DFF = matfile(F.fname('@DFF'));

% Mean image
mean_image = imread(F.fname('IP/@Mean', 'png'));

% Neurons' index
neurons_index = Neurons.ind;
n_neurons = size(neurons_index, 2);            % number of neurons

% Neurons' DF/F
dff = DFF.neurons;

% Make a canvas containing neurons' id
canvas = zeros(size(mean_image));
for idx = 1:n_neurons
    neuron_pix = neurons_index{idx};
    canvas(neuron_pix) = idx;
end

% Display mean image
figure;
subplot(2, 2, [1 2]);
imshow(rescale(mean_image))

% Begin UI crosshair
% ------------------
while true
    
    [x, y] = ginput(1);                         % select neuron
    i = ceil(y); j = ceil(x);
    ind = sub2ind(size(mean_image), i, j);      % convert to linear index
    neuron_id = canvas(ind);
    
    fprintf('Neuron #%i selected\n', neuron_id);
    
    if neuron_id > 0
        
        neuron_pix = neurons_index{neuron_id};
        mean_image(neuron_pix) = 0;             % put the neuron in black
        subplot(2, 2, [1 2])
        imshow(rescale(mean_image));
        
        % Plot DF/F and stimulation below mean image
        if ~exist('p1', 'var')
            subplot(2, 2, 3)
            title(['Neuron #' num2str(neuron_id)]);
            hold on
            xlabel('Time [s]');
            
            yyaxis left
            p1 = plot(t, dff(neuron_id, :));
            ylabel('\Delta{F}/F');
            
            yyaxis right
            p2 = plot(stim_calib.trace.time, stim_calib.trace.temperature);
            ylabel('Temperature [°C]');
            p2.Color(4) = 0.4;
            ylabel('Temperature [°C]');
        else
            p1.YData = dff(neuron_id, :);
            axis auto
        end
        
        % Stim average DF/F
        [peristim_time, peristim_sig] = periStimMean(t, dff(neuron_id, :)', stim, ...
            n_layers, t_before, t_after);
        
        subplot(2, 2, 4);
        hold on
        if ~exist('p3', 'var')
            
            xlabel('Time [s]');
            
            yyaxis left
            p3 = plot(peristim_time, peristim_sig);
            ylabel('<\Delta{F}/F>_{stim}');
            
            yyaxis right
            p4 = plot(stim_calib.mean_time, stim_calib.mean_temp);
            ylabel('Temperature [°C]');
        else
            p3.XData = peristim_time;
            p3.YData = peristim_sig;
        end
        
    elseif neuron_id == 0
        title('No neuron here...');
    end
end