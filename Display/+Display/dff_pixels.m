% Display DF/F per pixel. A Focus object and layer should be existing in the
% workspace (see display_baseline_neurons).

clear p1

% Select layer
F.select(layer);

% Get stimulation onsets
[stim, durations] = getStimTimes(F.Data);

% Prepare filenames
dff_file = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'dff.mat']);
avg_fname = [F.Data 'grey_stack' filesep 'Image_' sprintf('%02i', layer) '.tif'];

% Load pixel indices
index = dff_file.index;

% Load mean image
mean_image = imread(avg_fname);

% Time
t = F.sets.t;
t = t.*1e-3;

% Display mean image
figure;
subplot(2,1,1);
imshow(rescale(mean_image));

% Begin UI crosshair
while true
    
    [x, y] = ginput(1);                  % select pixel
    x = ceil(x); y = ceil(y);
    
    idx_px = sub2ind(size(mean_image), y, x);
    pix = find(index == idx_px);
    
    fprintf('Pixel #%i selected\n', pix);
    
    if pix > 0
        
        mean_image(y, x) = 0;           % put the pixel in black
        subplot(2,1,1);
        imshow(rescale(mean_image));
        
        selected_dff = dff_file.dff(pix, :);
        
        % Plot DF/F trace below mean image
        if ~exist('p1', 'var')
            subplot(2,1,2)
            p1 = plot(t, selected_dff);
            hold on
            ylabel('\Delta{F}/F');
            xlabel('Time [s]');
            title(['Pixel #' num2str(pix)]);
        else
            p1.YData = selected_dff;
            axis auto
        end
        
        % Stimulation representation
        supe = max(selected_dff);
        infe = min(selected_dff);
        subplot(2,1,2);
        if ~exist('p', 'var')
            p = cell(numel(stim), 1);
        end
        for n = 1:length(stim)
            if ~isempty(p{n})
                p{n}.delete;
            end
            p{n} = patch([stim(n) stim(n) stim(n)+durations(n) stim(n)+durations(n)], ...
                [infe supe supe infe], [0 0 1]);
            p{n}.FaceAlpha = 0.2;
            p{n}.EdgeColor = 'none';
        end
    elseif isempty(pix)
        title('No pixel here...');
    end
end