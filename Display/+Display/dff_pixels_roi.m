% Display DF/F per pixel. A Focus object and layer should be existing in the
% workspace (see display_baseline_neurons).

clear p1

% Select layer
F.select(layer);

% Get stimulation onsets
[stim, durations] = getStimTimes(F.Data);

% Prepare filenames
dff_file = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'dff.mat']);
avg_fname = [F.Data 'grey_stack' filesep 'Image_' sprintf('%02i', layer) '.tif'];

% Load pixel indices
index = dff_file.index;

% Load DF/F
if ~exist('dff', 'var')
    fprintf('Loading DF/F...');
    dff = dff_file.dff;
    fprintf(' Done.\n');
end

% Load mean image
mean_image = imread(avg_fname);

% Time
t = F.sets.t;
t = t.*1e-3;

% Display mean image
figure;
subplot(2,1,1);
imshow(rescale(mean_image));
ax = gca;
fprintf('Draw a region (click & drag to draw)\n');

while true
    
    img = mean_image;
    
    % Ask for a ROI
    % h = imfreehand(ax);   % for MATLAB version < R2018b
    h = drawfreehand(ax);
    mask = h.createMask;
    h.delete;
    
    % Find pixels in ROI
    roi_pix = find(mask);
    idx_pix = ismember(index, roi_pix);
    idx_pix = find(idx_pix);
    dff_roi = mean(dff(idx_pix, :), 1);
    
    img(roi_pix) = 0;  % put the ROI in black
    subplot(2,1,1);
    imshow(rescale(mean_image));
    
    % Plot DF/F trace below mean image
    if ~exist('p1', 'var')
        subplot(2,1,2)
        p1 = plot(t, dff_roi);
        hold on
        ylabel('\Delta{F}/F');
        xlabel('Time [s]');
    else
        p1.YData = dff_roi;
        axis auto
    end
    
    % Stimulation representation
    supe = max(dff_roi);
    infe = min(dff_roi);
    subplot(212);
    if ~exist('p', 'var')
        p = cell(numel(stim), 1);
    end
    for n = 1:length(stim)
        if ~isempty(p{n})
            p{n}.delete;
        end
        p{n} = patch([stim(n) stim(n) stim(n)+durations(n) stim(n)+durations(n)], ...
            [infe supe supe infe], [0 0 1]);
        p{n}.FaceAlpha = 0.2;
        p{n}.EdgeColor = 'none';
    end 
end