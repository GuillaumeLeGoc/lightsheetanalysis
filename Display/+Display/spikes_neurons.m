% Display spikes. A Focus object and layer should be existing.

clear p1

% Prepare data
% ------------
% Select layer
F.select(layer);

% Get stimulation onsets
[stim, durations] = getStimTimes(F.Data);

% Time
t = F.sets.t;
t = t.*1e-3;

% Prepare files
Neurons = matfile(F.fname('IP/@Neurons'));
spikes = matfile(F.fname('@Spikes'));
dff = matfile(F.fname('@DFF'));

% Mean image
mean_image = imread(F.fname('IP/@Mean', 'png'));

% Neurons' index
neurons_index = Neurons.ind;
n_neurons = size(neurons_index, 2);            % number of neurons

% Neurons' spikes & DF/F
spi = spikes.spikes;
dff = dff.neurons;

% Make a canvas containing neurons' id
canvas = zeros(size(mean_image));
for idx = 1:n_neurons
    neuron_pix = neurons_index{idx};
    canvas(neuron_pix) = idx;
end

% Display mean image
figure;
subplot(2,1,1);
imshow(rescale(mean_image))

% Begin UI crosshair
% ------------------
while true
    
    [x, y] = ginput(1);                         % select neuron
    i = ceil(y); j = ceil(x);
    ind = sub2ind(size(mean_image), i, j);      % convert to linear index
    neuron_id = canvas(ind);
    
    fprintf('Neuron #%i selected\n', neuron_id);
    
    if neuron_id > 0
        
        neuron_pix = neurons_index{neuron_id};
        mean_image(neuron_pix) = 0;             % put the neuron in black
        subplot(2,1,1)
        imshow(rescale(mean_image));
        
        % Plot DF/F below mean image
        if ~exist('p1', 'var')
            subplot(2,1,2)
            yyaxis left
            p1 = plot(t, spi(neuron_id, :));
            ylim([-0.05 0.2]);
            ylabel('Spikes');
            hold on
            yyaxis right
            p2 = plot(t, dff(neuron_id, :));
            p2.Color(4) = 0.25;
            ylim([-0.05 0.4]);
            ylabel('\Delta{F}/F');
            xlabel('Time [s]');
            title(['Neuron #' num2str(neuron_id)]);
        else
            p1.YData = spi(neuron_id, :);
            p2.YData = dff(neuron_id, :);
            axis auto
        end
        
        % Stimulation representation
        supe = max(spi(neuron_id, :));
        infe = min(spi(neuron_id, :));
        subplot(212);
        if ~exist('p', 'var')
            p = cell(numel(stim), 1);
        end
        for n = 1:length(stim)
            if ~isempty(p{n})
                p{n}.delete;
            end
            p{n} = patch([stim(n) stim(n) stim(n)+durations(n) stim(n)+durations(n)], ...
                [infe supe supe infe], [0 0 1]);
            p{n}.FaceAlpha = 0.2;
            p{n}.EdgeColor = 'none';
        end
    elseif neuron_id == 0
        title('No neuron here...');
    end
end