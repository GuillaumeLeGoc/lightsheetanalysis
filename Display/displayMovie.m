function [] = displayMovie(F, varargin)

% --- Check input
p = inputParser;
p.addRequired('F', @(x) isa(x, 'Focus'));
p.addOptional('cmap', inferno, @isnumeric);
p.parse(F, varargin{:});
F = p.Results.F;
cmap = p.Results.cmap;

% --- Get data
layer = F.set.id;
dff = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'dff.mat']);
meanimage = imread([F.Data 'grey_stack' filesep 'Image_' sprintf('%02i', layer) '.tif']);
index = dff.index;
sig = dff.dff;
ntimes = size(sig, 2);

crange = [0 1];

t = 1;

% --- Get data
[nrows, ncols, ~] = size(meanimage);

% --- Create first stack
img = zeros(nrows, ncols);
img(index) = sig(:, t);

% --- Create figure
fig = figure('Visible', 'off');
h = imshow(img);
ax = gca;
ax.Colormap = cmap;
caxis(crange);

% --- Create t slider
ht = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', ntimes, ...
    'SliderStep', [1/(ntimes-1) 1/(ntimes-1)], ...
    'Value', t, ...
    'Position', [260 40 260 20]);

% --- Create play/pause button
hp = uicontrol('Style', 'togglebutton', ...
    'Position', [530 40 20 20], ...
    'String', '|>');

% --- Create FPS box
hb = uicontrol('Style', 'edit', ...
    'Position', [560 40 50 20]);

% --- Link buttons
ht.Callback = @updateT;
ht.ButtonDownFcn = {@stopSlide, hp};
hp.Callback = {@playPause, ht, hb};

hlt1 = addlistener(ht, 'ContinuousValueChange', @updateT);
setappdata(ht, 'sliderListener', hlt1);

fig.Visible = 'on';

% -------------------------------------------------------------------------

    function updateT(slider_handle, ~)
        % Updates the t image to be displayed.
        t = floor(slider_handle.Value);
        img = zeros(nrows, ncols);
        img(index) = sig(:, t);
        set(h, 'Cdata', img);
        caxis(crange);
        drawnow;
    end

    function playPause(toggle_button, ~, sliderhandle, edithandle)
        sliderhandle.Enable = 'inactive';
        while get(toggle_button, 'Value')
            current_value = sliderhandle.Value;
            if current_value == ntimes
                current_value = 0;
            end
            sliderhandle.Value = current_value + 1;
            notify(sliderhandle, 'ContinuousValueChange');
            pause(1/str2double(edithandle.String));
        end
        sliderhandle.Enable = 'on';
    end

    function stopSlide(~, ~, toggle_button)
        toggle_button.Value = 0;
    end
end