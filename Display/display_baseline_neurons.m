% Display selected neuron fluo with baseline.

clear;
close all

% Experiment
study = 'Thermotaxis';
dat = '2019-06-06';
run = 3;
layer = 1;

% Get Focus
F = getFocus([study filesep dat], run);

% Display
Display.baseline_neurons;