% Compute and display with an user interface the Pearson's correlations
% matrix, corrected by correlated noise (first component from PCA).

clear;
close all

% Experiment
study = 'Thermotaxis';
dat = '2018-01-10';
run = 9;
layer = 4;

% Parameters
% ----------
motion_thresh = Inf;    % discard images with motion speed > motion_thresh*std
time_after_im = 0;      % discard X frames after a motion
start_time = 500;         % first time to consider in the analysis (SECONDS)
end_time = 'end';

% Get Focus
F = getFocus([study filesep dat], run);

fprintf('%s', F.name);
ML.CW.line;

% Prepare data
% ------------
% Complete parameters
n_layers = length(F.sets);
dt_scan = n_layers*(F.dt*1e-3);
start_frame = round(start_time/dt_scan);

% Select layer
F.select(layer);

% Prepare filenames
DFF = matfile(F.fname('@DFF'));
Neurons = matfile(F.fname('IP/@Neurons'));

% Load files
fprintf('Loading DF/F...');
dff = DFF.neurons;
fprintf(' Done.\n');

% Time
t = (F.set.t).*1e-3;        % in seconds
n_times = numel(F.set.t);    % number of frames per layer

% Remove stimulation with motion
fprintf('Discarding frames with motions...');
motion_times = getMotionTimes(F, layer, motion_thresh, time_after_im, 'sym');
dff_wm = dff;
dff_wm(:, motion_times) = 0;    % DFF without motions
fprintf(' Done, %i/%i frames discarded.\n', numel(motion_times), n_times);

% Define start and end time points
if ischar(end_time)
    if strcmp(end_time, 'end')
        dff_wm = dff_wm(:, start_frame:end);
    end
else
    end_frame = round(end_time/dt_scan);
    dff_wm = dff_wm(:, start_frame:end_frame);
end

% Compute Pearson's correlations matrix
% -------------------------------------
tic;
fprintf('Computing correlation matrix...');
correlation_matrix = computeCorrelationMatrix(dff_wm');  % correlation matrix
fprintf(' Done (%2.2fs).\n', toc);

% Display
% -------
Display.correlations_matrix;