% Display DF/F of selected neuron.

clear;
close all

% Experiment
study = 'Thermotaxis';
dat = '2019-11-19';
run = 1;
layer = 3;

% Get Focus
F = getFocus([study filesep dat], run);

% Display
Display.dff_neurons;