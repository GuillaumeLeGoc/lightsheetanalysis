% Display DF/F of selected neurons, along with stimulatio trace for
% corresponding calibration, and mean DF/F around the stim.

clear;
close all

% Experiment & calibration
study = 'Thermotaxis_Old';
dat = '2017-08-01';
run = 2;
layer = 13;

Tset = '0_33';
t_before = -0.5;        % time before stim for average
t_after = 60;           % time after stim for average
calib_name = 'calib';

% Get Focus
F = getFocus([study filesep dat], run);

Display.dff_neurons_stim;