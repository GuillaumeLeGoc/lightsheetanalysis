% Display DF/F of selected pixel.

clear;
close all

% Experiment
study = 'Thermotaxis';
dat = '2018-11-16';
run = 1;
layer = 16;

% Get Focus
F = getFocus([study filesep dat], run);

% Display
Display.dff_pixels;