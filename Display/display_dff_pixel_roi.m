% Display DF/F per pixels in the selected ROI.

clear;
close all

% Experiment
study = 'Thermotaxis';
dat = '2018-07-06';
run = 6;
layer = 13;

% Get Focus
F = getFocus([study filesep dat], run);

% Display
Display.dff_pixels_roi;