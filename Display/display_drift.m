% Play the raw movie and trace computed drift.

close all
clear
clc

% Experiment
% ----------
study = 'Thermotaxis';
dat = '2018-07-06';
run = 6;
layer = 5;

% Settings
% --------
live_correct = 'y';     % correct images

% Load data
% ---------
F = getFocus([study filesep dat], run);
F.select(layer);
n_times = numel(F.set.frames);
t = F.set.t/1e3;

% --- Drift
Drift = matfile(F.fname('IP/@Drifts'));
dx = Drift.dx;
dy = Drift.dy;

% --- Background
Bkg = matfile(F.fname('IP/@Background'));
bkg = Bkg.mean_first;

% --- Brain countour
Brain = matfile(F.fname('IP/@Brain'));
contours = Brain.contours;

% Prepare figure
% --------------
figure;
subplot(2,1,2);
hold on

warning('off', 'images:imshow:magnificationMustBeFitForDockedFigure');
tic
usr_msg = ['0/' num2str(n_times) '   [0 FPS]'];
fprintf(usr_msg);

% Play movie
% ----------
for ti = 1:n_times
    
    if mod(ti, 1) == 0
        fprintf(repmat(sprintf('\b'), 1, numel(usr_msg)));  % erase previous message
        usr_msg = [num2str(ti) '/' num2str(n_times) '   [' num2str(ti/toc) ' FPS]'];
        fprintf(usr_msg);
    end
    
    subplot(2,2,1);
    cla;
    img = F.iload(ti);
    img.rm_infos('rep', bkg);
    imshow(img.pix, F.IP.range);
    hold on
    plot(contours(:, 1), contours(:, 2), '-.y', 'LineWidth', .1);
    drawnow limitrate
    hold off
    switch live_correct
        case 'y'
            img.translate(-dy(ti), -dx(ti), 'fill', bkg);
            
            subplot(2,2,2);
            imshow(img.pix, F.IP.range);
            hold on
            plot(contours(:, 1), contours(:, 2), '-.y', 'LineWidth', .1);
            drawnow limitrate
            hold off
    end
    
    subplot(2,2,[3 4]);
    sta = max(1, ti - fix(60/F.dt*numel(F.sets)));
    plot(t(sta:ti), dx(sta:ti), 'b');
    hold on
    plot(t(sta:ti), dy(sta:ti), 'r');
    hold off
    drawnow limitrate
end