% Plays movie with pixels data (fluo, baseline, DF/F or spikes).

close all
clear
clc

% Experiment
% ----------
study = 'Thermotaxis';
dat = '2018-12-06';
run = 1;
layer = 1;

% Parameters
% ----------
what_to_show = 'spikes';  % 'baseline', 'fluo', 'spikes' or 'dff'

% Load data
% ---------
F = getFocus([study filesep dat], run);

% --- Data to show
switch what_to_show
    
    case 'fluo'
        fluo = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'sig.mat']);
        sig = fluo.signal_stack;
        index = fluo.index;
    case 'baseline'
        fluo = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'sig.mat']);
        index = fluo.index;
        baseline = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'baseline.mat']);
        sig = baseline.baseline;
    case 'dff'
        dff = matfile([F.Data 'signal_stacks' filesep num2str(layer) filesep 'dff.mat']);
        index = dff.index;
        sig = dff.dff;
    case 'spikes'
        spikes = matfile(F.fname('@Spikes'));
        neurons = matfile(F.fname('IP/@Neurons'));
        brain = matfile(F.fname('IP/@Brain'));
        sig = spikes.spikes;
        index = neurons.ind;
        bbox = brain.bbox;
end
n_times = size(sig, 2);

% Prepare figure
% --------------
figure;
warning('off', 'images:imshow:magnificationMustBeFitForDockedFigure');
tic
usr_msg = ['0/' num2str(n_times) '   [0 FPS]'];
fprintf(usr_msg);

% Play movie
% ----------
for ti = 1:n_times
    
    if mod(ti, 1) == 0
        fprintf(repmat(sprintf('\b'), 1, numel(usr_msg)));  % erase previous message
        usr_msg = [num2str(ti) '/' num2str(n_times) '   [' num2str(ti/toc) ' FPS]'];
        fprintf(usr_msg);
    end
    
    clf;
    
    switch what_to_show
        case 'spikes'
            img = zeros(bbox(4) - bbox(3) + 1, bbox(2) - bbox(1) + 1);
            
            for n = 1:size(sig, 1)
                img(index{n}) = sig(n, ti);
            end
            range = [];
        case 'fluo'
            range = F.IP.range;
            img = zeros(F.IP.height, F.IP.width);
            img(index) = sig(:, ti);
        otherwise
            img = zeros(F.IP.height, F.IP.width);
            img(index) = sig(:, ti);
            range = [];
    end
    
    imshow(img, range);
%     colormap('jet');
    drawnow limitrate
    
end

fprintf('\n');