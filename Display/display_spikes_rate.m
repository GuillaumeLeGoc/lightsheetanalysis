% Display inferred spiking rate.

% --- Experiment
study = 'Thermotaxis';
dat = '2019-01-30';
run = 1;

% --- Get data
F = getFocus([study '/' dat], run);
[~, spikes] = Store.getNeuronsSpikes(F);
n_times = F.set.t(end)/1000;
rate = sum(spikes, 2)/n_times;

% --- Display
figure; hold on
plot(rate, 'o');
xlabel('Neuron');
ylabel('<Spike rate> [Hz]');