LightSheetAnalysis
------------------
Set of tools created to analyse & use zebrafish calcium imaging data from lightsheet microscope, using a PCO.edge camera.

Documentation usually available at the beginning of each file.
MLab folder comes from Raphael Candelier (see http://candelier.fr/MLab/ ).
+Routines and +PCO were initially developed by Rapahel Candelier.
Also contains some functions from MATLAB FileExchange. All credits are given as downloaded on FileEx website.
Some codes use functions from the separate Tools repository.