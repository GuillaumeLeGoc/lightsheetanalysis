function list_binned = convert2BinnedIndex(list, size_original, binsize)
% This function convert a list of pixel indices in a ROI inside a picture
% of size sizeOriginal to a list of pixel indices in the same picture
% resized with a factor 1/binsize.
% INPUT:
% -----
% LIST : list of pixel indices
% SIZE_ORIGINAL : size of the non-resized image
% BINSIZE : inverse of scale factor of resized image
% 
% OUTPUT:
% ------
% LIST_BINNED : list of pixel indices of the ROI in the resized image.

[r, c] = ind2sub(size_original, list);  % convert indices to subscripts

r_b = floor(r/binsize);   % resize indices row- and column-wise
c_b = floor(c/binsize);

sub = horzcat(r_b, c_b);
sub_u = unique(sub, 'rows'); % remove duplicate pairs

list_binned = sub2ind(ceil(size_original/binsize), sub_u(:, 1), sub_u(:, 2)); % revert to indices