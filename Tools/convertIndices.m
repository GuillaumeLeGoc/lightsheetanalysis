function new_indices = convertIndices(F, old_indices, layer)
% Convert a set of neuron indices in a given a layer to absolute indices (in H5
% files).
%
% INPUTS :
% ------
% F : Focus object.
% old_indices : indices in a single layer to convert.
%
% OUTPUT :
% ------
% new_indices : list of indices corresponding to each input indices in the full
% brain.

new_indices = old_indices;

for l = 1:layer - 1
    
    F.select(l);
    
    % Get number of neurons on preceeding layers
    N = load(F.fname('IP/@Neurons'), 'N');
    new_indices = new_indices + N.N;

end