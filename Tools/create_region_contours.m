% Create coordinates of brain regions contours from ZBrainAtlas. Stores
% coordinates of contours in 3D, z projection (xy), x projection (yz) and z
% projection (in RPS space (x > right, y > posterior (tail), z > superior).
% Coordinates are expressed in microns. Also returns the brain contour.

clear
clc

% --- File definitions
dbpath = [pwd '/Data/Reference_Brains/Atlas_Masks/MaskDatabase.mat'];
outpath = [pwd '/Data/Reference_Brains/Atlas_Masks/ContoursZBrain.mat'];

% --- Load mask
D = load(dbpath);

% --- Image parameters
H = D.height;
W = D.width;
Z = D.Zs;
pixsize = 0.798;    % pixel size (µm)
zincrem = 2;        % voxel height (µm)
nregions = numel(D.MaskDatabaseNames);

% --- Processing
RegionContours3D = cell(nregions, 1);
RegionContoursZProj = cell(nregions, 1);
RegionContoursXProj = cell(nregions, 1);
RegionContoursYProj = cell(nregions, 1);

% Regions defining whole brain contour
regbraincontour = {'Diencephalon -' 'Rhombencephalon -', ...
    'Mesencephalon -', ...
    'Spinal Cord' 'Telencephalon -', ...
    'Mesencephalon - Torus Longitudinalis', ...
    'Mesencephalon - NucMLF (nucleus of the medial longitudinal fascicle)', ...
    'Mesencephalon - Oculomotor Nucleus nIII', ...
    'Mesencephalon - Tegmentum', ...
    'Mesencephalon - Tectum Stratum Periventriculare', ...
    'Diencephalon - Habenula', ...
    'Rhombencephalon - Cerebellum', ...
    'Rhombencephalon - Inferior Olive', ...
    'Rhombencephalon - Oculomotor Nucleus nIV', ...
    'Rhombencephalon - Spinal Backfill Vestibular Population', ...
    'Rhombencephalon - Tangential Vestibular Nucleus', ...
    'Ganglia - Statoacoustic Ganglion', ...
    'Rhombencephalon - Rhombomere 3', ...
    'Rhombencephalon - Rhombomere 4', ...
    'Rhombencephalon - Rhombomere 5', ...
    'Rhombencephalon - Rhombomere 6', ...
    'Rhombencephalon - Rhombomere 7'};
            
allimg = zeros(H, W, Z);        % to get brain contour

fprintf('Getting contours of %d regions... ', nregions); tic;
fprintf('%4d/%4d', 0, nregions);

for r = 1:nregions
    
    fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d', r, nregions);
    
    % Prepare image
    img = zeros(H, W, Z);
    
    % Get pixels belonging to this region
	pixinreg = find(D.MaskDatabaseOutlines(:, r));
    
    % Prepare brain contour
    if any(ismember(regbraincontour, D.MaskDatabaseNames{r}))
        allimg(pixinreg) = 1;
    end
    
    % Fill image
    img(pixinreg) = 1;
    
    % - Get xy coordinates of contours for each z slice
    xyz = cell(Z, 1);
    for layer = 1:Z
        
        zimg = img(:, :, layer);
        
        % Contour coordinates
        [y, x] = find(bwperim(zimg));
        z = repmat(layer, size(x, 1), 1);
        
        % Convert to microns
        x = (x - 1).*pixsize;   % start at (0, 0, 0)
        y = (y - 1).*pixsize;
        z = (z - 1).*zincrem;
        
        xyz{layer} = [x, y, z];
        
    end
    
    % Cleanup empty layers
    xyz(cellfun(@isempty, xyz)) = [];
    
    % Pool all
    RegionContours3D{r} = cat(1, xyz{:});
    
    % - Get coordinates of outline of Z projection (x,y plane)
    zproj = max(img, [], 3);
    zprojcont = bwboundaries(zproj);
    zprojcont = cellfun(@(x) x(:, [2, 1]), zprojcont, 'UniformOutput', false);  % set x as first
    
    % Convert in microns
    zprojcont = cellfun(@(x) (x - 1).*pixsize, zprojcont, 'UniformOutput', false);
    RegionContoursZProj{r} = zprojcont;
    
    % - Get coordinates of outline of X projection (y,z plane)
    xproj = squeeze(max(img, [], 2));
    xprojcont = bwboundaries(xproj);
    
    % Convert in microns
    y = cellfun(@(x) (x(:, 1) - 1).*pixsize, xprojcont, 'UniformOutput', false);
    z = cellfun(@(x) (x(:, 2) - 1).*zincrem, xprojcont, 'UniformOutput', false);
    RegionContoursXProj{r} = cellfun(@(x, y) [x, y], y, z, 'UniformOutput', false);
    
    % - Get coordinates of outline of Y projection (x,z plane)
    yproj = squeeze(max(img, [], 1));
    yprojcont = bwboundaries(yproj);
    
    % Convert in microns
    x = cellfun(@(x) (x(:, 1) - 1).*pixsize, yprojcont, 'UniformOutput', false);
    z = cellfun(@(x) (x(:, 2) - 1).*zincrem, yprojcont, 'UniformOutput', false);
    RegionContoursYProj{r} = cellfun(@(x, y) [x, y], x, z, 'UniformOutput', false);
end

fprintf('\b\b\b\b\b\b\b\b\bDone (%2.2fs).\n', toc)

fprintf('Getting brain contours...'); tic;
% - Get brain contour in Z, X and Y projection
brainzproj = bwboundaries(max(allimg, [], 3));
brainzproj = (brainzproj{1}(:, [2, 1]) - 1).*pixsize;
BrainContourZProj = brainzproj;

brainxproj = bwboundaries(squeeze(max(allimg, [], 2)));
y = (brainxproj{1}(:, 1) - 1).*pixsize;
z = (brainxproj{1}(:, 2) - 1).*zincrem;
brainxproj = [y, z];
BrainContourXProj = brainxproj;

brainyproj = bwboundaries(squeeze(max(allimg, [], 1)));
x = (brainyproj{1}(:, 1) - 1).*pixsize;
z = (brainyproj{1}(:, 2) - 1).*zincrem;
brainyproj = [x, z];
BrainContourYProj = brainyproj;

fprintf('\tDone(%2.2fs).\n', toc);

% --- Save file
fprintf('Saving file...'); tic;
METADATA = 'Coordinates (x,y,z), (x,y for ZProj, y,z for XProj, x,z for YProj) of brain regions contours, in microns, in RPS space';
RegionNames = D.MaskDatabaseNames';
save(outpath, 'METADATA', 'RegionNames', 'RegionContours3D', 'RegionContoursXProj', 'RegionContoursYProj', 'RegionContoursZProj', 'BrainContourXProj', 'BrainContourYProj', 'BrainContourZProj');
fprintf('\tDone (%2.2fs).\n', toc);