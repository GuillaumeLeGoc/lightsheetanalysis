function motion_times = extractMotionTimes(drift, thresh, window, symetric)
% This function finds frames numbers where a motion occured. A motion is
% defined when the drift speed is higher than thresh*std(speed). The frame
% before and the next few (window) are discarded as well. Adapted from
% extract_motion_times.
% 
% INPUTS : 
% ------
% drift : x-y two-columns drift matrix
% thresh : tresh*std(speed) is the threshold
% window : number of frames to consider as motion after a motion
% symetric : 'sym' or 'uni', discard window image after a motion ('uni') or
% before and after ('sym').
% 
% OUPUTS :
% ------
% motion_times : index of frames where a motion occured.

Ntimes = size(drift,1);
dx = diff(drift(:,1));
dy = diff(drift(:,2));
v = sqrt(dx.^2 + dy.^2);
th = thresh.*std(v);

motion_times_init = v > th;

s = zeros(Ntimes, 1);
s(motion_times_init) = 1;

switch symetric
    case 'sym'
        filt = ones((2*window + 1), 1);
    case 'uni'
        filt = ones((2*window + 1), 1);
        filt(1:window-1) = 0;
    otherwise 
        error("Choose 'sym' (discard frames before and after motion) or 'uni' (discard frame after motion only) as last argument.");
end

s2 = imdilate(s, filt);
motion_times = find(s2>0);
end