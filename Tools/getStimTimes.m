function [stim_onsets, stim_durations, channels] = getStimTimes(chemin)
% This function reads the parameters file to get digital stimuli (DS)
% onsets and duration. Try to use the NT.Parameters class.
% The file should be name 'Parameters.txt' or 'ParametersN' where N is the run
% number.
%
% INPUTS :
% ------
% chemin : path to the folder containing the parameters file.
%
% OUTPUTS :
% -------
% stim_onset : vector with stimulation onsets.
% stim_duration : vector with stimulation durations.
% channel : vector giving the channel of each stim.

run = num2str(str2double(chemin(end-2:end-1))); % convert run number to %01i

% Try both names
fparam1 = [chemin 'Parameters.txt'];  % create filename
fparam2 = [chemin 'Parameters' run '.txt'];  % create filename

if exist(fparam1, 'file')
    fparam = fparam1;
elseif exist(fparam2, 'file')
    fparam = fparam2;
else
    warning('No parameters file found.');
    stim_onsets = [];
    stim_durations = [];
    channels = [];
    return
end

% Try to parse Parameters with Parameters object
P = NT.Parameters;
P.load(fparam);

n_chan = numel(P.Signals.DS);
n_stim = numel([P.Signals.DS(:).tstart]);
stim_onsets = NaN(n_stim, 1);
stim_durations = NaN(n_stim, 1);
channels = cell(n_stim, 1);

if ~isempty(P.Signals)

    for c = 1:n_chan
        
        if c == 1
            range = 1:numel(P.Signals.DS(c).tstart);
        else
            start = numel(P.Signals.DS(c-1).tstart) + 1;
            stop = numel(P.Signals.DS(c-1).tstart) + numel(P.Signals.DS(c).tstart);
            range = start:stop;
        end
        stim_onsets(range) = P.Signals.DS(c).tstart;
        stim_durations(range) = P.Signals.DS(c).tstop - P.Signals.DS(c).tstart;
    end
else
    
    warning('NT.Parameters loading didn''t work.');
    % Following doesn't work when several stim channel, to be fixed.
    %     fid = fopen(fparam, 'r');   % open files
    %
    %     c = 0;  % initialise line counter
    %     read_string = '';
    %
    %     while ~strcmp(read_string, '# Digital Signals')
    %         read_string = fgetl(fid); % read line by line
    %         c = c + 1;
    %     end
    %
    %     % c + 3 is the beginning of stim onsets
    %
    %     data = readtable(fparam, 'HeaderLines', c + 3, 'Delimiter', '\t', ...
    %         'MultipleDelimsAsOne', true, 'ReadVariableNames', false);
    %
    %     if size(data, 2) == 1
    %         data = readtable(fparam, 'HeaderLines', c + 3, 'Delimiter', '   ', ...
    %             'MultipleDelimsAsOne', true, 'ReadVariableNames', false);
    %     end
    %
    %     stim_chan = data(:, 1);
    %     stim_onset = data(:, 2);
    %     stim_duration = data(:, 3);
    %     stim_onset = table2array(stim_onset);
    %     stim_duration = table2array(stim_duration);
    %
    %     fclose(fid);
    
end
end