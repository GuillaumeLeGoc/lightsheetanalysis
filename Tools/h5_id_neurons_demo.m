% How to identify a given neuron on the temporal mean from H5 file.

file = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Datasets/20181206_Run01_spontaneous_Tset=30.2.h5';

% T = readtable('/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/HBO/2019-01-09_Run06/Index/hbo_indices.txt');
% neuron_id = T.Var1;
% neuron_id(isnan(neuron_id)) = [];

neuron_id = [10315
       10316
       10335
       10340
       10369
       10383
       10389
       10403
       10412
       10446
       10467
       10474
       10475
       10483
       10493
       10495
       10514
       10515
       10520
       10521
       10525
       10529
       10546
       10547
       10552
       10553
       10558
       10559
       10583
       10594
       10598
       10626
       10652
       10676
       10677
       10705
       10710
       10729];
   

[pixlist, layer] = getPixelList(file, neuron_id);

mean_stack = h5read(file, '/Data/Brain/Pixels/TemporalMean');
mean_image = mean_stack(:, :, layer{2});

mean_image(cat(1, pixlist{:})) = 1;

imshow(mean_image); caxis auto

function [pixlist, layer, id_on_layer] = getPixelList(file, neuron_id)
% Reads h5 file specified by FILE and find the pixel list (linearly indexed
% within the temporal mean image) composing the neuron with number NEURON_ID.
%
% INPUTS :
% ------
% file : full path to H5 file.
% neuron_id : vector containing neurons' id.
%
% OUTPUTS :
% -------
% pixlist : cell, contains for each NEURON_ID element the pixels list composing
% the neuron.
% layer : cell, gives the number of layer where each NEURON_ID element is.
% id_on_layer : cell, gives the ID on the given layer.

% Load data
coordinates = h5read(file, '/Data/Brain/Coordinates');
segm_mask = logical(h5read(file, '/Data/Brain/Pixels/Segmentation'));
pix_size = h5readatt(file, '/Data/Brain/Pixels/TemporalMean', 'pixel size (µm)');

% Get layers
layers = unique(coordinates(:, 3), 'stable');

% Get neuron's coordinates
coord_neuron = coordinates(neuron_id, :);

layer = cell(numel(neuron_id), 1);
id_on_layer = cell(numel(neuron_id), 1);
pixlist = cell(numel(neuron_id), 1);

for id = 1:numel(neuron_id)
    
    % Get neuron's layer
    layer{id} = find(ismember(layers, coord_neuron(id, 3)), 1);
    
    % Get neurons pixels
    R = regionprops(segm_mask(:, :, layer{id}), 'PixelIdxList', 'Centroid');
    pos = reshape([R(:).Centroid], [2 numel(R)])';
    
    id_on_layer{id} = find(ismembertol(pos, coord_neuron(id, [1 2])*1000/pix_size), 1);
    pixlist{id} = R(id_on_layer{id}).PixelIdxList;
end
end