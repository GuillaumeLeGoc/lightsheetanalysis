% Update an attribute in h5 files. Not tested, use with caution.

% Path parameters
% ---------------
origin = pwd;
atlas_xform = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/Warp_Atlas_On_LJP';
% experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/RandomPulses_list.txt';
experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Spontaneous_list.txt';
study = 'Thermotaxis';
root = [pwd filesep 'Data' filesep study filesep];
tag = 'spontaneous';      % add tag to output filename

% Attribute to change
attrloc = '/Metadata';
attrname = 'File version';
newvalue = '1.0';

list = readtable(experiment_list, 'DatetimeType', 'text');
n_exp = size(list, 1);

fprintf('Replacing attribute '); tic;
for idx_dates = 1:n_exp
    
    % Read date and runs list
    dat = list.Date{idx_dates};
    if strcmp(dat(1), '#')
        continue;
    end
    
    if isa(list.RunNumber, 'double')
        % Handles when only one day in a day and runs list is read as an
        % array directly
        runs = list.RunNumber(idx_dates);
        
        if strcmp(study, 'Thermotaxis')
            Tsets = list.Tset(idx_dates);
        else
            Tsets = [];
        end
        
    else
        runs = eval(['[' list.RunNumber{idx_dates} ']']);
        if strcmp(study, 'Thermotaxis')
            Tsets = eval(['[' list.Tset{idx_dates} ']']);
        else
            Tsets = [];
        end
    end
    
    for idx_run = 1:numel(runs)
        
        fprintf('.');
        
        run = runs(idx_run);
        if ~isempty(Tsets)
            Tset = Tsets(idx_run);  % temperature setpoint in the experiment
        else
            Tset = [];
        end
        
        % Build output HDF5 file
        d = dat;
        d(strfind(d, '-')) = [];    % remove hyphen
        output_dir = [root 'Datasets' filesep];
        output_file = [output_dir d '_Run' sprintf('%02i', run) '_' tag '_Tset=' num2str(Tset) '.h5'];
        
        % Delete attribute
        fileID = H5F.open(output_file,'H5F_ACC_RDWR','H5P_DEFAULT');
        fileIDCleanUp = onCleanup(@()H5F.close(fileID));
        % Open the dataset/group
        locID  = H5O.open(fileID, attrloc,'H5P_DEFAULT');
        locIDCleanUp = onCleanup(@()H5O.close(locID));
        
        attID = H5A.open(locID, attrname, 'H5P_DEFAULT');
        H5A.close(attID);
        H5A.delete(locID, attrname);
        
        h5writeatt(output_file, attrloc, attrname, newvalue);
        
    end
end
fprintf(' Done (%2.2fs).\n', toc);