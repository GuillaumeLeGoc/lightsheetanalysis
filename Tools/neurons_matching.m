% Match brain mask and neurons coordinates from one experiment to another 
% with CMTK.

close all
clear
clc

% Definitions
cmtk_dir = '/usr/local/lib/cmtk/bin/';      % where are CMTK binaries
study = 'Thermotaxis';

% Define pairs list
pairs_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Matching_list.txt';

pairs = readtable(pairs_list, 'DatetimeType', 'text', 'Delimiter', 'comma');

for idx_pair = 1:size(pairs, 1)
    
    % Input experiment
    in.dat = pairs.DateIn{idx_pair};
    in.run = pairs.RunIn(idx_pair);
    
    % Output experiment
    out.dat = pairs.DateOut{idx_pair};
    out.run = pairs.RunOut(idx_pair);
    
    % CMTK settings
    param.exp_type = '';
    param.binning = [1 1];
    
    % Get focii
    in.F = getFocus([study '/' in.dat], in.run);
    out.F = getFocus([study '/' out.dat], out.run);
    
    fprintf('Matching %s --> %s', out.F.name, in.F.name); tic
    
    % Get parameters
    n_layers = numel(in.F.sets);
    
    % Prepare output folders
    in.dir = fullfile(in.F.Data, 'Registration');
    out.dir = fullfile(out.F.Data, 'Registration');
    out.affine1 = fullfile(out.dir, 'affine_1.xform');
    out.affine2 = fullfile(out.dir, 'affine_2.xform');
    if ~exist(in.dir, 'dir')
        mkdir(in.dir);
    end
    if ~exist(out.dir, 'dir')
        mkdir(out.dir);
    end
    
    % Create NRRD, register OUT on IN and save transformations
    % --------------------------------------------------------
    focii = {in.F, out.F};
    [part1, part2] = create2Nrrd(focii, 'grey_stack', 'Registration', param);
    registerNrrd(part1{1}, part1{2}, out.affine1);
    registerNrrd(part2{1}, part2{2}, out.affine2);
    
    % Convert neurons indices
    % -------------------------------------------------------------------------
    for l = 1:n_layers
        
        fprintf('.');
        
        in.F.select(l);
        out.F.select(l);
        
        % Get xformlist
        if l <= n_layers/2
            xform = out.affine1;
        else
            xform = out.affine2;
        end
        
        % In data
        in.neurons = load(in.F.fname('IP/@Neurons'), 'N', 'ind', 'pos', 'sub');
        in.brain = load(in.F.fname('IP/@Brain'));
        in.param = load(in.F.fname('IP/@Parameters'));
        
        % Out data
        out.Nmat = out.F.matfile('IP/@Neurons');
        out.Pmat = out.F.matfile('IP/@Parameters');
        out.Bmat = out.F.matfile('IP/@Brain');
        
        % --- Backup
        if exist(out.F.fname('IP/@Neurons'), 'file')
            movefile(out.F.fname('IP/@Neurons'), [out.F.fname('IP/@Neurons') '.bak']);
        end
        if exist(out.F.fname('IP/@Brain'), 'file')
            movefile(out.F.fname('IP/@Brain'), [out.F.fname('IP/@Brain') '.bak']);
        end
        if exist(out.F.fname('IP/@Mean', 'png'), 'file')
            movefile(out.F.fname('IP/@Mean', 'png'), [out.F.fname('IP/@Mean', 'png') '.bak']);
        end
        if exist(out.F.fname('IP/@mean8', 'png'), 'file')
            movefile(out.F.fname('IP/@mean8', 'png'), [out.F.fname('IP/@mean8', 'png') '.bak']);
        end
        if exist(out.F.fname('@DFF'), 'file')
            movefile(out.F.fname('@DFF'), [out.F.fname('@DFF') '.bak']);
        end
        % ---
        
        % --- Brain mask
        % --------------
        sub = in.brain.sub;
        pos = in.brain.contours;
        
        % Transform coordinates of brain mask
        pos = [pos.*in.F.dx, in.F.set.z.*ones(size(pos, 1), 1)];
        pos = Store.convertCoordinates(pos, xform);
        pos = round(pos(:, 1:2)./in.F.dx);
        sub = [sub(:, 2).*in.F.dx, sub(:, 1).*in.F.dx, in.F.set.z.*ones(size(sub, 1), 1)];
        sub = Store.convertCoordinates(sub, xform);
        sub = round([sub(:, 2), sub(:, 1)]./in.F.dx);
        sub = min(sub, [in.F.IP.height, in.F.IP.width]);
        sub = max(sub, [1, 1]);
        ind = NaN(size(sub, 1), 1);
        for j = 1:size(sub, 1)
            r = sub(j, 1);
            c = sub(j, 2);
            ind(j) = sub2ind([in.F.IP.height, in.F.IP.width], r, c);
        end
        out.Bmat.save(ind, 'Linear indices of the brain mask ([i])');
        out.Bmat.save(sub, 'Subscripts indices of the brain mask ([i j])');
        out.Bmat.save('contours', pos, 'Contour of the brain ([x y])');
        bbox = [max(ceil(min(pos(:,1))),1) min(floor(max(pos(:,1))),out.F.IP.width) ...
            max(ceil(min(pos(:,2))),1) min(floor(max(pos(:,2))),out.F.IP.height)];
        out.Bmat.save(bbox, 'Bounding box of the brain contour ([x1 x2 y1 y2])');
        out.brain = load(out.F.fname('IP/@Brain'));
        out.meansize = [out.brain.bbox(4) - out.brain.bbox(3) + 1, out.brain.bbox(2) - out.brain.bbox(1) + 1];
        % ---
        
        % --- Neurons
        % -----------
        % --- Convert neurons' coordinates in full frame
        pos = in.neurons.pos;
        pos = pos + [in.brain.bbox(1), in.brain.bbox(3)];
        sub = in.neurons.sub;
        sub = cellfun(@(x) x + [in.brain.bbox(1), in.brain.bbox(3)], sub, 'UniformOutput', false);
        % ---
        
        % --- Transform neurons' coordinates with CMTK
        pos = [pos.*in.F.dx, in.F.set.z.*ones(size(pos, 1), 1)];
        pos = Store.convertCoordinates(pos, xform);
        pos = pos(:, 1:2)./in.F.dx;
        sub = cellfun(@(x) [x.*in.F.dx, in.F.set.z.*ones(size(x, 1), 1)], sub, 'UniformOutput', false);
        sub = cellfun(@(x) Store.convertCoordinates(x, xform), sub, 'UniformOutput', false);
        sub = cellfun(@(x) round(x(:, 1:2)./in.F.dx), sub, 'UniformOutput', false);
        % ---
        
        % --- Convert neurons' coordinates back in the brain box
        pos = pos - [out.brain.bbox(1), out.brain.bbox(3)];
        sub = cellfun(@(x) x - [out.brain.bbox(1), out.brain.bbox(3)], sub, 'UniformOutput', false);
        sub = cellfun(@(x) min(x, flip(out.meansize)), sub, 'UniformOutput', false);
        sub = cellfun(@(x) max(x, [1, 1]), sub, 'UniformOutput', false);
        % Get linear indices, can't use cellfun
        ind = cell(size(sub));
        for i = 1:numel(sub)
            subs = sub{i};
            linind = NaN(size(subs, 1), 1);
            for j = 1:size(subs, 1)
                r = subs(j, 2);
                c = subs(j, 1);
                linind(j) = sub2ind(out.meansize, r, c);
            end
            ind{i} = linind;
        end
        % ---
        
        % --- Save new neurons
        out.Nmat.save('N', in.neurons.N, 'Number of neurons');
        out.Nmat.save('pos', pos, 'Positions of the neurons'' centroids [x, y]');
        out.Nmat.save('ind', ind, 'Linear indices of the pixels belonging to each neuron in the mean image {[i]}.');
        out.Nmat.save('sub', sub, 'Subscript indices of the pixels belonging to each neuron in the mean image {[x y]}.');
        % ---
        
        % --- Copy segmentation parameters
        area_lth = in.param.neurons_lower_area;
        area_uth = in.param.neurons_upper_area;
        ecc_lth = in.param.neurons_eccentricity;
        out.Pmat.save('neurons_lower_area', area_lth, 'Area lower threshold');
        out.Pmat.save('neurons_upper_area', area_uth, 'Area upper threshold');
        out.Pmat.save('neurons_eccentricity', ecc_lth, 'Eccentricity threshold');
        % ---
    end
    fprintf('\tDone (%2.2fs)\n', toc);
end
% -------------------------------------------------------------------------


% -------------------------------------------------------------------------

function [nrrd1, nrrd2] = create2Nrrd(focii, tiffdirname, regdirname, param)

if ~iscell(focii)
    focii{1} = focii;
end

% Handles
grey_source = @(F, n) fullfile(F.Data, tiffdirname, ['Image_' sprintf('%02i', n) '.tif']);
grey_target = @(F, m, n) fullfile(F.Data, regdirname, [tiffdirname '_' num2str(m)], ['Image_' sprintf('%02i', n) '.tif']);

% Init
nrrd1 = cell(numel(focii), 1);
nrrd2 = cell(numel(focii), 1);
for idxf = 1:numel(focii)
    
    n_layers = numel(focii{idxf}.sets);
    
    % --- Outbound
    if ~exist(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1']), 'dir')
        mkdir(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1']));
    end
    % Create substack
    layers = 1:n_layers/2;
    for l = layers
        cmd = ['cp "' grey_source(focii{idxf}, l) '" "' grey_target(focii{idxf}, 1, l) '"'];
        unix(cmd);
    end
    % Convert to NRRD
    dz = mean(abs(diff([focii{idxf}.sets(layers).z])));
    param.pix_size = [focii{idxf}.dx focii{idxf}.dy dz];
    param.origin = [0 0 focii{idxf}.sets(layers(1)).z];
    param.space_type = 'pls';
    makeNrrdStack(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1']), ...
        fullfile(focii{idxf}.Data, regdirname), [tiffdirname '_1'], param);
    nrrd1{idxf} = fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_1.nrrd']);
    
    % --- Return
    if ~exist(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2']), 'dir')
        mkdir(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2']));
    end
    % Create substack
    layers = (n_layers/2 + 1):n_layers;
    for l = layers
        cmd = ['cp "' grey_source(focii{idxf}, l) '" "' grey_target(focii{idxf}, 2, l) '"'];
        unix(cmd);
    end
    % Convert to NRRD
    dz = mean(diff([focii{idxf}.sets(layers).z]));
    param.pix_size = [focii{idxf}.dx focii{idxf}.dy dz];
    param.origin = [0 0 focii{idxf}.sets(layers(1)).z];
    param.space_type = 'pla';
    makeNrrdStack(fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2']), ...
        fullfile(focii{idxf}.Data, regdirname), [tiffdirname '_2'], param);
    nrrd2{idxf} = fullfile(focii{idxf}.Data, regdirname, [tiffdirname '_2.nrrd']);
end
end

% -------------------------------------------------------------------------

function [] = registerNrrd(refnrrd, floatnrrd, output)

cmtk_dir = '/usr/local/lib/cmtk/bin/';      % where are CMTK binaries
stacks = ['-o ' '"' output '"'  ' ' '"' refnrrd '"' ' ' '"' floatnrrd '"' ];
% Options from Bertoni (leave a space at the end of the string)
options = 'registration -i --coarsest 25.6 --sampling 3.2 --omit-original-data --exploration 25.6 --dofs 6 --dofs 9 --accuracy 3.2 ';
% options = 'registrationx --dofs 12 --min-stepsize 1 '; % Burgess
% options = 'registration --initxlate --dofs 6,9 --auto-multi-levels 4 '; %
% CMTK User guide
command = [cmtk_dir options stacks];
unix(command);
end