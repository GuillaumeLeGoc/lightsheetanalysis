% Take a RGB stack in ZBrain space (138 layers, 1406*621*3), compute
% Z-projection and X-projection, display them and overlay brain contours
% and specified regions.
% You'll need the 'ContoursZBrain.mat' file created by the
% 'create_region_contours' script.

clear
clc

% --- Parameters

% Input stack
stackpath = '/home/guillaume/Science/Projects/ForNatalia/Y249_new/';

% Brain regions contours to display
regtodot = {'Rhombencephalon - Rhombomere 4', ...
    'Rhombencephalon - Rhombomere 5', ...
    'Rhombencephalon - Rhombomere 6', ...
    'Telencephalon - Subpallium', ...
    'Telencephalon - Pallium', ...
    'Diencephalon - Pretectum', ...
    'Mesencephalon - Tectum Stratum Periventriculare', ...
    'Rhombencephalon - Cerebellum', ...
    'Diencephalon - Habenula', ...
    'Ganglia - Trigeminal Ganglion', ...
    'Rhombencephalon - Neuropil Region 4', ...
    'Rhombencephalon - Neuropil Region 2'};

% Brain regions contours colors
regcolors = rainbow(numel(regtodot));

% Brain regions line thickness
csz = 1.25;

% Contours file
contpath = [pwd filesep 'Data' filesep 'Reference_Brains' filesep 'Atlas_Masks' filesep 'ContoursZBrain.mat'];

% ZBrain space specs.
sx = 0.798; % pixel size (microns)
sy = 0.798;
sz = 2;     % z increment (microns)

% --- Preparing

% Read files
files = dir([stackpath '*.tif']);
readimg = @(n) imread([stackpath, files(n).name]);
img = readimg(1);

% Get properties
nlayers = numel(files);
nrows = size(img, 1);
ncols = size(img, 2);

% Get brain contours
C = load(contpath);

idsplt = ismember(C.RegionNames, regtodot); % find regions to plot

bxy = C.BrainContourZProj;  % z-proj
byz = C.BrainContourXProj;  % x-proj

Rxy = C.RegionContoursZProj(idsplt);
Ryz = C.RegionContoursXProj(idsplt);

% Init. 4D stack (3D + RGB)
rgb_stack = NaN(nrows, ncols, 3, nlayers);

% Create the stack
for idz = 1:nlayers

    img = readimg(idz);

    rgb_stack(:, :, :, idz) = img;

end

% Projections
max_z = uint8(max(rgb_stack, [], 4));
stack_perm = permute(rgb_stack, [1, 4, 3, 2]);
max_x = uint8(max(stack_perm, [], 4));

% --- Display
figure;

% - Display Z-projection
subplot(1, 2, 1);
% 'XData' and 'YData' set the scale in microns
I = imshow(max_z, 'XData', [0, (ncols - 1)*sx], 'YData', [0, (nrows - 1)*sy]);

hold on

% Plot brain contour
plot(bxy(:, 1), bxy(:, 2), 'Color', [0.8, 0.8, 0.8], 'LineWidth', csz);

% Plot specified regions
for ir = 1:size(Rxy)
        
        rxy = Rxy{ir};
        
        cellfun(@(x) plot(x(:, 1), x(:, 2), ...
            'Color', regcolors(ir, :), ...
            'Linewidth', csz), ...
            rxy);
        
end

% Scale bar
plot([30, 130], [1095, 1095], 'Color', [0.8, 0.8, 0.8]);    % 100microns scale bar

% - Display X-projection
subplot(1, 2, 2);
% 'XData' and 'YData' set the scale in microns
J = imshow(max_x, 'XData', [0, nlayers*sz], 'YData', [0, (nrows - 1)*sy]);

hold on

% Plot brain contour
plot(byz(:, 2), byz(:, 1), 'Color', [0.8, 0.8, 0.8], 'LineWidth', csz);

% Plot specified regions
for ir = 1:size(Ryz)
        
        ryz = Ryz{ir};
        
        cellfun(@(x) plot(x(:, 2), x(:, 1), ...
            'Color', regcolors(ir, :), ...
            'Linewidth', csz), ...
            ryz);
        
end

% Scale bar
plot([15, 115], [1095, 1095], 'Color', [0.8, 0.8, 0.8]);    % 100microns scale bar
