function [] = reformatImagesName(base, study, dat, runs)
% This function renames the images found in study/dat/runs to have the same
% number of zeros for all images. Uses natsortfiles from FileExchange.

% Format path
checkdate = datevec(dat);
if checkdate(1) < 2018 && checkdate(2) < 9
    path_to_day = [base dat ' ' filesep];
else
    path_to_day = [base study filesep dat filesep];
end

% Rename images
for run = runs
    path_to_images = [path_to_day 'Run ' sprintf('%02i', run) filesep 'Images' filesep];
    
    % List image in directory
    image_list = dir([path_to_images '*.tif']);
    image_names = struct2cell(image_list);
    image_names = image_names(1, :);
    image_names = natsortfiles(image_names);
    Nimages = numel(image_list);
    
    if Nimages == 0
        error('No images found.');
    elseif Nimages < 100000
        ndig = 5;   % number of digits
        form = '%05i';
        last = 9999;
    elseif Nimages >= 100000 && Nimages < 1000000
        ndig = 6;
        form = '%06i';
        last = 99999;
    else
        ndig = 7;
        form = '%07i';
        last = 999999;
    end
    
    % Loop over all images
    fprintf('%s, %s, Run %02i, renaming images...\n', study, dat, run);
    tic;
    idx_file = -1;
    for idx_img = 1:last + 1
        
        % Verbose
        statusInfo(idx_img, numel(1:last+1), 20);
        
        % Filenames begin with 0
        idx_file = idx_file + 1;
        
        % Make filename to replace
        oldname = [path_to_images image_names{idx_img}];
        
        % Find underscore position
        underscore_loc = strfind(image_names{idx_img}, '_');
        
        % Find prefix and suffix
        prefix = image_names{idx_img}(1:underscore_loc);
        suffix = image_names{idx_img}(end - 3 : end);
        
        % Make the new file name
        newname = [path_to_images prefix sprintf(form, idx_file) suffix];
        
        % Rename the file if it is not in the good format
        if numel(image_names{idx_img}) ~= (numel(prefix) + numel(suffix)  + ndig)
            %movefile(oldname, newname);
            java.io.File(oldname).renameTo(java.io.File(newname));
        end

    end
    fprintf('\nDone in %2.2f s.\n', toc);
end